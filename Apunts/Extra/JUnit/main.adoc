:copyleft:

:author: Manel Orós
:email: manel.oros@xtec.cat, manel.oros@copernic.cat
:revdate: 01/09/2024
:revnumber: 1.0
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font
:imagesdir: ./images

:sectnums:
= Proves Unitàries amb Java

== Introducció

Una de les parts fonamentals del desenvolupament de programari és l'etapa de codificació.

Com a programadors/es, quan codifiquem estem traspassant la fase de disseny, en forma de requeriments funcionals, no funcionals, especificacions, esquemes UML, etc... en instruccions de codi font. Aquestes instruccions s'executen en un entorn que normalment és un sistema operatiu i que en el cas de Java no coneixem a priori, i sobre un maquinari, que tampoc coneixem, operat per personal del que òbviament no en podem anticipar la forma en què usen l'aplicació.

Això implica que el ventall de situacions no previstes a priori que poden succeir i que amenacen l'estabilitat del progranari, les accions realitzades per aquest o la fiabilitat de les dades proporcionades és immens.

Col.loquialment una situació d'aquest tipus s'anomena "error", però hi ha de molts tipus.


Segons la RAE, un error és:
====
* Concepto equivocado o juicio falso.
* Acción desacertada o equivocada.
* Cosa hecha erradamente.
* Fís. y Mat. Diferencia entre el valor medido o calculado y el real.
====

Obviant els errors d'anàlisi i/o disseny d’un determinat programari, ens centrarem en els errors durant la fase de desenvolupament. 

Segons la Viquipèdia, un error de programari és:
====
Un error o defecte que causa un resultat incorrecte o inesperat en un programa o sistema, o que es comporti de forma no prevista. 
====

Al llarg de la història recent tenim diversos  https://www.fyccorp.com/articulo-10-grandes-errores-de-software[exemples] d'errors fatals de programari.

=== Tipus d'errors

Podem classificar els errors de programari segons la fase del desenvolupament en la que es produeixen:

==== Errors de compilació 

Es produeixen quan la sintaxi del codi és incorrecte i el compilador o l’intèrpret no pot traduir-lo a codi executable. Normalment el procés queda interromput i no es genera el codi executable fins que la codificació és totalment correcta. Actualment la majoria dels IDE ja prevenen d’aquests errors abans que es produeixin.

Aquest tipus d'errors no s'inclouen en els mecanismes de prova atès que és el compilador, intèrpret, IDE, etc... que ja els detecten automàticament.

==== Errors d’execució
Es produeixen quan el flux del programa queda interromput per una situació no prevista. Aquesta situació pot estar provocada principalment per dos tipus factors: 

*Factors intrínsecs:* Una combinació d’instruccions que porten a un estat no controlat  del sistema. P.ex una divisió per zero, una crida al mètode d’un objecte null, un accés fora dels límits d’un array, etc...

*Factors extrínsecs:* La impossibilitat de portar a terme una acció que depèn d’un sistema extern camviant. P.ex. un recurs de xarxa no disponible, no tenir permisos per desar al disc, accedir a un directori que ja no existeix, etc...

=== Planificació de proves

=== Tipus de proves

Els diferents tipus de proves de programari depenen del nivell o capa del programari que es vulgui provar i del seu cost en temps i recursos:

* *Proves unitaries*: Es realitzen individualment sobre els mètodes, classes, components o mòduls. Son de baix cost i fàcilment automatitzables.
* Proves d'integració: Verifiquen que els diferents mòduls que s'integren en una aplicació funcionen correctament entre ells. Pot incloure microserveix, bases de dades, drivers, etc...
* *Proves funcionals*: Es centren en verificar els requsits funcionals. Aquestes proves es basen en la correcta especificació d'aquests.
* *Proves integrals*: Repliquen el comportament de l'usuari en un entorn complet. 
* *Proves d'acceptació*: S'acostumen a fer amb la participació del client i busquen l'acceptació d'aquelles parts rellevants per part d'aquest.
* *Proves de rendiment*: Ajuden a mesurar la fiabilitat, escalabilitat i capacitat de resposta d'una aplicació en condicions normals i en condicions extremes. També s'anomenen proves d'estrés.
* *Proves de fum (smoke test)*: Proves bàsiques de baix cost que serveixen per assegurar el funcionament bàsic de l'aplicació i prevenir despeses en proves més costoses.
* *Proves de regressió*: L'objectiu d'aquest tipus de prova és garantir que tot allò del programari que NO s'ha tocat en la nova versió continuï funcionant sense afectació.

== Disseny de proves unitàries

L'ideal és tenir per a cada mètode una prova unitària que el validi. Cada prova unitària ha de ser efectiva, precisa i rellevant. 

Alguns dels principals criteris a tenir en compte són:

. *Independència*: Cada prova unitària ha de ser independent d'altres proves. 
. *Especificitat*: Cada prova unitària ha de provar una funció o mètode en particular amb una entrada i sortida controlada.
. *Determinisme*: Les proves han de produir el mateix resultat cada vegada que s'executen sota les mateixes condicions.
. *Rapidesa*: S'han d'executar de forma ràpida, atès que són el pas previ al desplegament. Per exemple, si les proves triguen hores o dies, no és possible un cicle ràpid de desenvolupament. També han de ser automatitzables per ser integrades en entorns CI/CD.
. *Mantenibilitat*: Han de ser igual de mantenibles que el codi que suporten.
. *Coverage*: Han de cobrir el màxim del codi possible. L'ideal és un 100%, tot i que de vegades no és possible aconseguir-ho d'aquesta forma.

Posem l'exemple bàsic d'un mètode en Java que suma dos nombres:

[source, java]
----
public class Calculator {

    public Integer add(Integer a, Integer b) 
    {
        return a+b;
    }
}
----

La prova unitària d'aquest mètode hauría de tenir en compte tots els possibles cassos:

* Opera correctament amb nombres positius de poca grandaria?
* Opera correctament amb nombres negatius de poca grandaria?
* Opera correctament amb nombres positius i negatius de poca grandaria?
* Opera correctament incloent zeros?
* Opera correctament amb valors extrems?
* Compleix la propietat commutativa?


[source, java]
----
    @Test
    public void testNombresNegatius() {
        assertEquals(-5, Calculadora.add(-2, -3));
    }

    @Test
    public void testUnDecada() {
        assertEquals(1, Calculadora.add(4, -3));
    }

    @Test
    public void testAmbZeros() {
        assertEquals(4, Calculadora.add(4, 0));
        assertEquals(4, Calculadora.add(0, 4));
    }

    @Test
    public void testNombresExtrems() {
        assertEquals(Integer.MAX_VALUE+Integer.MAX_VALUE, Calculadora.add(Integer.MAX_VALUE, Integer.MAX_VALUE));
    }
    
    @Test
    public void testNombresExtrems2() {
        assertEquals(0, Calculadora.add(Integer.MAX_VALUE, -Integer.MAX_VALUE));
    }

    @Test
    public void testPropietatCommutativa() {
        assertEquals(Calculadora.add(3, 5), Calculadora.add(5, 3));
    }
----

[IMPORTANT]
====
A l'anterior exemple hi ha un cas de prova que no s'ha tingut en compte i que pot fer fallar el mètode. Sabríes dir quin és?
====



[NOTE]
====
Exercici a l'aula
====

----
El teu project manager et passa la tasca d'integrar el següent mètode. Tens a la teva disposició a un programador extern ubicat a Bangladesh que realitzarà la implementació. 

El mètode forma part d'una aplicació molt més gran encarregada de la gestió dels TPV (terminals punt de venda) d'una gran xarxa de botigues. Concretament, el mètode ajudarà al personal de caixa a prevenir errors al retornar el canvi als clients. La teva tasca és garantir que el mètode realitza a la perfecció el que s'ha definit, sense cap tipus d'error.
Atès que formes part d'un entorn que treballa amb CD/CI (integració contínua), et demanen implementar el test que validarà aquest mètode. Tens la responsabilitat del bon funcionament del métode. Si el teu test dona l'ok, el mètode s'integra a l'aplicació.
----

----
1 - Analitza detingudament les especificacions que et passa el teu project manager (que sempre va amb presses) i revisa que estiguin ben definides. Les acceptes?
----

[source, java]
----
    /***
     * Mètode que descompon una determinada quantitat en € en les seves monedes fraccionaries.
     * 
     * @param n Import en euros, entre 0.00 i 1000.00€.
     * @return Una llista fixa de 14 posicions començant per la posició zero,
     * on cada posició és el nombre de monedes d'aquell valor que composen
     * la quantitat final.
     * 
     * 0.01 ===> posició 0
     * 0.05
     * 0.10
     * 0.20
     * 0.50
     * 1
     * 2
     * 5
     * 10
     * 20
     * 50
     * 100
     * 200
     * 500 ====> posició 13
     * 
     * Exemple: 
     * 
     * Entrada <== 10.04
     * Sortida ==> 4,0,0,0,0,0,0,1,0,0,0,0,0
     * Entrada <== 500.01
     * Sortida ==> 1,0,0,0,0,0,0,1,0,0,0,0,1
     * Entrada <== 80.00
     * Sortida ==> 0,0,0,0,0,0,0,1,1,1,0,0,0
     */
    public static List<Integer> retornaCanvi(Float n)
----

----
2 - Un cop acceptada la tasca del teu PM, doissenya el test que ha de validar el mètode anabs de ser integrat.
----


== Proves unitàries amb JUnit

Java disposa del seu propi conjunt de llibreries anomenat https://junit.org/junit5/[*JUnit*] per a aquest tipus de proves. La darrera versió és la 5.

=== JUnit 5 Configuració

Per a configurar JUnit al nostre Netbeans mitjançant Maven, hem de seguir el següents passos:

Sobre nostre projecte, botó dret i seleccionar "Java Class..."

image::JunitConfig1.png[align="center"]

a continuacio seleccionar "JUnit Test"

image::JunitConfig2.png[align="center"]

i omplim el quadre 

image::JunitConfig3.png[align="center"]

Ens ha d'apareixer una nova classe a una nova ubicacio anomenada "Test Packages".

image::JunitConfig4.png[align="center"]

A la classe, implementem el següent:

[source, java]
----

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestCalculadora {
    
    public TestCalculadora() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        
        System.out.println("El que posem aquí s'executarà un sol cop quan es carrega la classe");
    }
    
    @AfterAll
    public static void tearDownClass() {
        
        System.out.println("El que posem aquí s'executarà quan es descarrega la classe");
    }
    
    @BeforeEach
    public void setUp() {
        System.out.println("El que posem aquí s'executarà abans d'executar cada mètode de test");
    }
    
    @AfterEach
    public void tearDown() {
        System.out.println("El que posem aquí s'executarà un sol cop a l'acabar cada mètode de test");
    }

    
    @Test
    public void test1() {
    
        System.out.println("Hola, sóc el test 1. Tot va perfecte!!");
        assertTrue(1==1);
    
    }
    
    @Test
    public void test2() {
    
        System.out.println("Hola, sóc el test 2. Tot va perfecte!!");
        assertTrue(1==1);
    
    }
    
    @Test
    public void test3() {
    
        System.out.println("Hola, sóc el test 3 i alguna cosa anirà malament...");
        assertTrue(1==0);
    
    }
    
    @Test
    public void test4() {
        
        System.out.println("Hola, sóc el test 4 i no m'executaré perquè no es compleix una determinada condició");
        Assumptions.assumeTrue(1==0);
    
    }
}

----

Botó dret sobre la classe test i seleccionar "Test File"

image::JunitConfig5.png[align="center"]

Als resultats ha d'apareixer quelcom similar a això, amb sortides per consola per a cada test:

image::JunitConfig6.png[align="center"]

=== JUnit 5 Comandes

La validació o no validació d'una condició de test depen exclusivament del criteri del programador, tot i que hi ha eines que faciliten el disseny dels  casos de prova.

La classe principal de JUnit és *org.junit.jupiter.api.Assertions* i disposa de diversos mètodes que fonamentalment retornen *true* o *false*. En funció dels resultats dun o més asserts, la prova serà vàlida o fallarà.

[TIP]
====
Exemples
====

Si en un mateix test tenim varies proves i alguna d'elles falla, tot i que no sigui la darrera, tot el test falla.

----
    @Test
    public void test5() {
        
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
        assertTrue(false);
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
    }
----

Si per contrari, cap prova falla, el test és correcte. 

[source, java]
----
    @Test
    public void test5() {
        
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
        assertTrue(true);
    }
----

Si el test no té cap prova, tampoc no falla.

[source, java]
----
    @Test
    public void test5() {
        
    }
----

Si el test provoca una excepció no controlada, no serà tingut en compte:

[source, java]
----
    @Test
    public void test5() {
        String s = null;
        s.toString();
    }
----

Alguns exemples de mètodes de la classe Assert:

*assertEquals(expected, actual)*: Comprova que dos valors són iguals. Si no ho són, llança un error amb un missatge de fallida.
*assertTrue(condition)*:Verifica que una condició és true. Si és false, llança un error.
*assertFalse(condition)*: Verifica que una condició és false. Si és true, llança un error.
*assertNull(object)*:Comprova que un objecte és null. Si no ho és, llança un error.
*assertNotNull(object)*:Comprova que un objecte no és null. Si és null, llança un error.
*assertThrows(expectedType, executable)*:Comprova que un bloc de codi llenci una excepció del tipus especificat. Si no llencen l’excepció esperada, llança un error.
*assertArrayEquals(expectedArray, actualArray)*:Comprova que dos arrays són iguals, és a dir, contenen els mateixos elements en la mateixa ordre. Si no ho són, llança un error.

Tambe tenim la classe *org.junit.jupiter.api.Assumptions*. Aquesta classe funciona similar a la classe Assertions, per en lloc de fer fallar un test l'ignora en funció del resultat. 

Per exemple el següent test sí serà tingut en compte a la llista però no s'executarà:

[source, java]
----
    @Test
    public void test5() {
        Assumptions.assumeTrue(1==0);
    }
----

=== Mockito Configuració

https://site.mockito.org/[*Mockito*] és un framework opensource programat en Java que ens permet simular el comportament d'objectes mitjançant la creació de "dobles" objectes de prova.

[TIP]
====
Mock: not authentic or real, but without the intention to deceive.
====

Per a incloure Mockito al nostre projecte únicament necessitem afegir la següent dependència al nostre _pom.xml_:

[source, xml]
----
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>5.13.0</version>
    <scope>test</scope>
</dependency>
----

Anem a veure un exemple de la seva utilització més bàsica en la suplantació d'una classe i dels seus mètodes, interceptant el seu comportament i usant-lo a la nostra conveniencia:

Suposem la senzilla classe *ClasseSimple* que diposa del mètode _saluda_:

[source, java]
----
public class ClasseSimple {
    
    public String saluda(String thingName) {

        return String.format("Hola %s !!", thingName);
    }
}
----

Per exemple:

[source, java]
----
//Creem una instància de la classe ClasseSimple, però "mockeada" 
ClasseSimple miClaseMock = Mockito.mock(ClasseSimple.class);

// Aquesta instància NO funciona directament com una instancia normal
// atès que el seu mètode ha sigut "interceptat"
System.out.println(miClaseMock.saluda("Manel",52)); //no retorna res


// Primer li assignem el seu comportament "normal"
when(miClaseMock.saluda(anyString(),anyInt())).thenCallRealMethod();

//Ara, per exemple, li assigmen el comportament de que quan rebi el paràmetre "Manel" 
// i edat > 50 ha de saludar de forma especial
// eq = equal, gt = més gran que, lt = més petit qué, etc...
when(miClaseMock.saluda(eq("Manel"),anyInt())).thenReturn("Que tal Manel ets molt jove!!!");

//verifiquem
System.out.println(miClaseMock.saluda("Maria", 70));
System.out.println(miClaseMock.saluda("Manel", 52));
System.out.println(miClaseMock.saluda("Pepe", 52));

//finalment podem eliminar els comportaments
Mockito.reset(miClaseMock);
----

La sortia seria:
----
null
Hola Maria , tens 70 anys!!
Que tal Manel ets molt jove!!!
Hola Pepe , tens 52 anys!!
----

En exemple REAL alhora d'utiltzar Mockito és el fet de fer un test sobre el processament de les dades de sortida d'una consulta SQL. El que ens facilita Mochito en aquest cas és:

* Independència d'una infraestructura de connexió a BBDD
* Assegurar que les dades obtingudes sempre son les mateixes
* Garantir que les classes utilitzades són les mateixes que farem servir en producció

Aquí un exemple:

[source, java]
----
 try {
    // Creem un mock de Connection
    Connection connectionMock = mock(Connection.class);

    // Creem un mock de PreparedStatement
    PreparedStatement preparedStatementMock = mock(PreparedStatement.class);

    // Creem un mock de ResultSet
    ResultSet resultSetMock = mock(ResultSet.class);

    // Configurar el mock de connection perquè retorni el PreparedStatement mockeat
    when(connectionMock.prepareStatement(anyString())).thenReturn(preparedStatementMock);

    // El mateix perqué el Prepared Statement retorni el Resultset
    when(preparedStatementMock.executeQuery()).thenReturn(resultSetMock);

    // Configurem el resultset perquè simuli 3 resultats diferents
    when(resultSetMock.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
    when(resultSetMock.getString("name")).thenReturn("Juan").thenReturn("Maria").thenReturn("Ana");
    when(resultSetMock.getString("id")).thenReturn("1").thenReturn("2").thenReturn("3");
    when(resultSetMock.getString("age")).thenReturn("21").thenReturn("20").thenReturn("19");

    // Executem la consulta
    PreparedStatement ps = connectionMock.prepareStatement("SELECT name FROM usuarios");
    resultSetMock = ps.executeQuery();
    
    //un cop tenim les dades, ja le spodríem enviar al métode que hem de verificar
    while (resultSetMock.next()) {
        System.out.print("Id: " + resultSetMock.getString("id")); 
        System.out.print(" Nom: " + resultSetMock.getString("name")); 
        System.out.println(" Edat: " + resultSetMock.getString("age")); 
    }
    
    
} catch (SQLException ex) {
    System.out.println("ERROR: "  );
}
----

i la sortida seria:

----
Id: 1 Nom: Juan Edat: 21
Id: 2 Nom: Maria Edat: 20
Id: 3 Nom: Ana Edat: 19
----

== ANNEX

=== Recursos

ifdef::copyleft[]
include::cc_license.adoc[]
endif::copyleft[]
ifndef::copyleft[]
(C) Manel Orós. Tots els drets reservats
endif::copyleft[]
