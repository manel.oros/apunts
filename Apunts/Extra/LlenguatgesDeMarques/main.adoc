:copyleft:

:author: Manel Orós
:email: manel.oros@xtec.cat, manel.oros@copernic.cat
:revdate: 16/03/2025
:revnumber: 1.1
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font
:imagesdir: ./images

:sectnums:
= Markdown & Asciidoc

== Introducció

*Markdown* i *Asciidoc* són llenguatges de marques (markup languages) de tipus lleuger. 
Tal i com s'ha vist al mòdul professional 4 “*Llenguatge de marques i sistemes de gestió de la informació*” i segons la definició de https://www.termcat.cat/ca/cercaterm/llenguatge%2Bde%2Bmarques%2Blleuger?type=basic[termcat.cat] , un llenguatge de marques de tipus lleuger és...

----
... un llenguatge de marques de sintaxi senzilla dissenyat perquè sigui fàcil d'escriure mitjançant qualsevol editor de text genèric i fàcil de llegir sense format.
----

A més, un llenguatge de marques ha de permetre les operacions següents:

* Definir quines marques es necessiten,
* la seva posició dintre del text,
* distingir entre marques i text,
* donar significat a cada marca.

Dintre de l’univers dels llenguatges de marques, els de tipus lleuger s’utilitzen fonamentalment per a generar i publicar text en un format comprensible per humans, com per exemple HTML, PDF, EBook, PPT, etc… 

image::img1.png[align="center"]

Un dels inconvenients dels llenguatges de marques lleugers és que per la gran quantitat de variants (forks) existents hi ha una gran dispersió d’opcions, eines, extensions i formats. Això fa  que l’usuari tingui dificultats per a decidir quina eina s’adapta millor a les seves necessitats.

== Markdown

Va ser creat l’any 2004 per  John Gruber i Aaron Swartz i es distribueix sota llicència https://es.wikipedia.org/wiki/Berkeley_Software_Distribution[BSD] (més permissiva que © però menys que Copyleft).

Els seus punts forts són la senzillesa, permetent que es pugui escriure utilitzant un format de text fàcil de llegir i d’escriure i també la gran acceptació. Per exemple Git, Reddit, Slack, etc… l’utilitzen.

Els seus inconvenients deriven dels seus avantatges. Al ser senzill d’aprendre i d’usar, disposa d’un ventall reduït de funcionalitats cosa que pot esdevenir un limitador en segons per a quines necessitats.

[NOTE]
Per a més informació consultar la https://www.markdownguide.org/getting-started/[documentació oficial] de Markdown

== AsciiDoc

https://docs.asciidoctor.org/asciidoc/latest/[AsciiDoc] és una especificació de llenguatge de marques lleuger  creat l’any 2002 per *Matthew Peveler, Dan Allen, Michel Krämer* i es distribueix sota una llicència de tipus *Copyleft GPL v2*.
Basat en el llenguatge *DocBook*, es va millorar canviant l'antic format XML per marques de text, facilitant la comprensió però conservant la riquesa de formats i capacitats.

Un dels seus avantatges són les seves múltiples extensions, com per exemple:

* Conversors a HTML, PDF, EPUB, EPUB3, DocBook,...
* PlantUML, per a diagrames UML.
* Mermaid Extension, per a diagrames de flux.
* Ditaa, per a digrames de propòsit general.
* MathJax, per a fòrmulació matemàtica

Després de convertir un text AsciiDoc a HTML, esdevé de la següent forma:

image::img2.png[align="center"]

=== Asciidoctor
https://docs.asciidoctor.org/asciidoctor/latest/[Asciidoctor] és la implementació de referència per a interpretar AsciiDoc escrit en Ruby i executable en diverses plataformes com Linux o Windows així com també integrar-ho en projectes https://docs.asciidoctor.org/asciidoctorj/latest/[Java]. 

Es pot https://docs.asciidoctor.org/asciidoctor/latest/cli/[instalar] a Ubuntu via _apt_:

[source,shell]
----
$ sudo apt install asciidoctor
$ asciidoctor --version

Asciidoctor 2.0.16 [https://asciidoctor.org]
Runtime Environment (ruby 3.0.2p107 (2021-07-07 revision 0db68f0233) [x86_64-linux-gnu]) (lc:UTF-8 fs:UTF-8 in:UTF-8 ex:UTF-8)
----

Per exemple, per a convertir un fitxer index.adoc a index.html:

[,shell]
----
$ asciidoctor index.adoc -o index.html
----

=== AsciiDocFX

Implementació de Asciidoc implementada amb JavaFX que permet una interfície gràfica "amigable" i diverses funcionalitats extra com:

* Previsualització en temps real tipus WYSIWYG (**W**hat **Y**ou **S**ee **I**s **W**hat **Y**ou **G**et) 
* Multiplataforma
* Formats de sortida PDF, HTML, EPUB, MOBI, DocBook
* Diverses extensions com MathJax, PlantUML, Mermaid, ditaa, Filesystem Tree, JavaFX Charts que permeten realitzar diagrames del tipus:

.UML Diagram Example
[plantuml,target="uml-example"]
--
abstract class AbstractList
abstract AbstractCollection
interface List
interface Collection

Collection <|- List
AbstractCollection <|- AbstractList
AbstractList <|-- ArrayList

class ArrayList {
  Object[] elementData
  size()
}

enum TimeUnit {
  DAYS
  HOURS
  MINUTES
}

annotation SuppressWarnings
--

Amb el següent codi raw:

----

.UML Diagram Example
[plantuml,target="uml-example"]
--
abstract class AbstractList
abstract AbstractCollection
interface List
interface Collection

List <|-- AbstractList
Collection <|-- AbstractCollection

Collection <|- List
AbstractCollection <|- AbstractList
AbstractList <|-- ArrayList

class ArrayList {
  Object[] elementData
  size()
}

enum TimeUnit {
  DAYS
  HOURS
  MINUTES
}

annotation SuppressWarnings
--

----

=== Principals caracteristiques

A continuació s'enumeren les principals característiques d'Asciidoc:

Formateig de text:: Negreta, cursiva, subindex, subratllat, ...
Llistes:: Numerades, amb pics, checklists, ...
Taules:: De diverses dimensions, formats, amb capçaleres, ...
Index de contingut autmàtic:: Es genera automàticament a partir de les seccions.
Enllaços:: Referències a URL's externes
Imatges:: Diversos formats. Diverses posicions de centrat i ajustos de grandària. Amb títol, nota al peu, ...
Icones:: Per inserir entre els caràcters. 
Condicionals:: En funció de certs valors o variables, es pot ignorar el processament d'un determinat bloc de text.
Includes:: Permet incloure blocs a partir de fitxers adoc externs.


[NOTE]
Consulteu la https://docs.asciidoctor.org/asciidoc/latest/[documentació oficial] per a obtenir una llista detallada de la sintaxi i de les funcionalitats disponibles.

== Gitlab & Markdown

A Gitlab (i també a Guthub) podem confeccionar una benvinguda a la pàgina principal del nostre projecte amb Markdown de forma molt senzilla tot seguint aquests passos:

Crearem un nou projecte anomenat _mytestproject1_ a Gitlab:

image::img4.png[align="center"]

El clonarem com a repositori local:

image::img3.png[align="center"]

Ens desplaçarem a l'arrel del directori recentment clonat:

[,shell]
----
$ cd mytestproject1
----

Canviarem a la branca main:

[,shell]
----
$ git switch --create main
----

En aquest punt tenim al hostre host un directori totament buit. A l'arrel del directori crearem un fitxer anomenat *readme.md* amb el següent contingut:

----
# TestProject1 2DAMM

Exemple de cóm es veu el fitxer ***readme.md*** en format ***Markdown*** un cop pujat a GitLab, al directori arrel del nostre projecte anomenat ***mytestproject1***.
----

Un cop desat, l'afegirem al projecte amb:

----
$ git add readme.md
----

I ho pujarem tot plegat al repositori remot de Gitlab:

----
$ git commit -m "afegint readme.md"
$ git push --set-upstream origin main
----

Un cop pujat, fent un refresh a la pàgina principal del nostre projecte a Gitlab hauriem d'obtenir quelcom similar a:

image::img6.png[align="center"]

== Gitlab & AsciiDoc

També a Gitlab (i també a Guthub) és possible confeccionar una benvinguda a la pàgina principal del nostre projecte en format AsciiDoc afegint un fitxer anomenat *index.adoc*. 

Com a exemple, aprofitant el projecte _mytestproject1_ creat anteriorment, afegirem el fitxer *index.adoc* amb el següent contingut, al directori arrel:

----
:author: el teu nom
:email: la teva adreça de correu
:revdate: la data de la darrera revisió p.ex 01/01/2024
:revnumber: la versió p.ex. 1.0
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font

# TestProject1 2DAMM

Exemple de cóm es veu el fitxer ***index.adoc*** en format ***AsciiDoc*** un cop pujat a GitLab, al directori arrel del nostre projecte anomenat ***mytestproject1***.
----

[INFO]
====
Has de substituïr aguns dels camps amb les teves dades.
====

Aquest fixter és convertit de forma automàtica a HTML i mostrat com a pàgina de benvinguida al nostre projecte de GitLab.

== Gitlab Pages & HTML

Amb https://docs.gitlab.com/ee/user/project/pages/index.html[GitLab Pages] és possible també publicar llocs web estàtics directament des del nostre repositori. GitLab Pages és un servei gratuït d'integració contínua o https://docs.gitlab.com/ee/ci/[CD/CI]. Solament cal activar-lo i configurar-lo convenientment. 

Com a exemple i aprofitant el projecte _mytestproject1_ creat anteriorment, afegim els següents fitxers:

----
mytestproject1
├── .gitlab-ci.yml
├── images
│   └── planeta.png
├── index.adoc
└── index.html
----

* *planeta.png*: És una imatge en format png que sigui "divertida" i que ens apareixerà a la pàgina de benvinguda.

* *index.adoc*: Similar al fitxer creat anteriorment a l'apartat _Gitlab & AsciiDoc_ però mostrant una imatge:

----
:author: el teu nom
:email: la teva adreça de correu
:revdate: la data de la darrera revisió p.ex 01/01/2024
:revnumber: la versió p.ex. 1.0
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font
:imagesdir: ./images

= HOLA MÓN !!!

image::planeta.png[align="center"]
----

* *index.html*: Aquest fitxer és el resultat de la conversió del fitxer anterior a format html (veure capítol _Asciidoctor_). A GitLab Pages, el contingut no pot estar directament en Asciidoc.

* *.gitlab-ci.yml*: Un cop pujat el projecte a Gitlab, es cerca un fitxer de tipus  https://hsf-training.github.io/hsf-training-cicd/04-understanding-yaml-and-ci/index.html[YAML] amb aquest mateix nom. Aquest fitxer en YAML s'encarrega de desplegar el contingut _html_ al contenidor Docker que el publicarà. Eliminem el contingut anterior del fitxer *.gitlab-ci.yml* i afegim el següent contingut:

----
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r ./* .public
  - mv .public public
  artifacts:
    paths: 
    - public
  only:
  - main
----
Fonamentalment, el que realitza aquest script és un desplegament on copia tot el contingut del directori arrel del nostre projecte a una ubicació temporal (_.public_) per desprès finalment moure-la a la seva ubicació definitiva anomenada _public_.

[WARNING]
====
La posició d'inici de cada línia és part fonamental de la sintaxi de YAML. Has de tenir molta cura de no introduïr o treure espais, ni abans ni després de cada línia, respecte del fitxer anterior.
====

Un cop ho tenim tot, realitzem un push al repositori amb els següents passos:

----
$ git add .
$ git commit -m "publicació a Gitlab Pages"
$ git push origin main
----

Un cop realitzada la pujada, hem de revisar que l'execució del job ha sigut correcte:

image::img12.png[align="center"]

En cas afirmatiu, ja podem obrir el navegador i sol.licitar la nostra pàgina web a la següent adreça:

image::myProject1_pages.png[align="center"]

----
https://manel.oros.gitlab.io/mytestproject1/index.html
----

image::img13.png[align="center"]

[TIP]
====
Has de substituïr manel.oros pel teu nom d'usuari de Gitlab
====

== ANNEX

=== Recursos

* https://jorge-aguilera.gitlab.io/tutoasciidoc/[Tutorial bàsic Asciidoc]

** https://asciidoctor.org/docs/asciidoc-writers-guide/[Documentació Ascidoctor]

*** https://asciidocfx.com/[Documentació AsciiDocFX]

**** https://www.youtube.com/watch?v=KbmpBZO4EoY[Vídeo-tutorial ASciiDoc]

* https://www.markdownguide.org/[MarkDown guide]

* https://quejox.gitlab.io/portafolis/posts/breu_manual_plantilla_hugo/[Breu manual de confecció de portafolis]


ifdef::copyleft[]
include::cc_license.adoc[]
endif::copyleft[]
ifndef::copyleft[]
(C) Manel Orós. Tots els drets reservats
endif::copyleft[]
