== Elements d’una xarxa 

////
==== Mòdem analògic

[IMPORTANT]
====
Un mòdem és un aparell que s’encarrega de convertir el flux de dades digitals que entra o surt de l’ordinador en una ona analògica capaç de ser transmesa per una línia telefònica convencional.
====
 
* El seu nom ve de la contracció de les expressions **MOD**ulador  i **DEM**odulador, i són les dues funcions que realitza.
* Evidentment en una comunicació d’aquest tipus sempre hi ha d’haver dos mòdems, un a cada costat de la línia telefònica.

[cols=".>,.>"]
|===
a|
.Mòdem telefònic extern
image::modem1.jpg[500,500]
a|
.Connexió d'un mòdem intern a la xarxa
image::modem2.jpg[500,500]
|===

* Els mòdems poden ser interns o externs, si és extern s’acostuma a connectar al port sèrie o a un port USB.
* El terme mòdem s’ha ampliat actualment de manera que es considera que aquest dispositiu s’utilitza per a connectar un equip a una WAN (el terme correcte seria el d’adaptador de xarxa), en aquest cas el mòdem no modula ni demodula el senyal i tècnicament no hauríem de parlar de mòdem. (pe. Mòdem ADSL, RDSI o cable mòdem)
////

=== Tarja de xarxa

[IMPORTANT]
====
La tarja de xarxa o NIC (Network Interface Card) realitza la funció d’intermediari entre l’ordinador i la xarxa de comunicacions.
Encapsula els protocols de les capes física, enllaç de dades i xarxa gravats en un ROM.
====

*  La comunicació amb l’ordinador es realitza habitualment pels “slots” ISA, PCI o PCMCIA o directament amb la placa mare.

* Passos que segueix la tarja de xarxa a l’hora de transmetre la informació

1. Determinar la velocitat de transmissió, la longitud de la trama, etc..., en funció de la configuració establerta des del sistema operatiu.
2. Convertir el flux de dades en paral·lel (lo habitual en els busos de l’ordinador) a un flux en sèrie.
3. Codificar la seqüència de bits en una senyal elèctrica. 

////
.Indicadors d’estat
[TIP]
====
Les tages de xarxa i altres dispositius utilitzen llums led per a donar informació sobre l'estat dels seus enllaços físics.

Alguna de la informació que proporcionen pot ser:

* LNK, PWR
** Hi ha connexió amb la xarxa.
* ACT, TX/RX
** La tarja esta enviant o rebent dades.
* COL, FUDUP
** Hi ha més d’una estació emetent a la vegada i s’ha produït una col·lisió.
* Indicador de velocitat
** Taronja 10Mbps i verd 100Mbps

En qualsevol cas els colors i el significat del led depenen del fabricant de la tarja i cal mirar-ne les especificacions per coneixer-ne el significat.
====
////

==== Exemples de targes de xarxa

.Alguns exemples de targetes de xarxa
[cols=".>,.>"]
|===
a| .Tarja híbrida 10base-2/10base-T
image::nic1.jpg[500,500] a| 
.Tarja Ethernet base-T
image::nic2.png[500,500]
|===

[cols=".>,.>"]
|===
a| .Tarja wi-fi
image::nic3.png[500,500] a| 
.1000base-SX
image::nic4.png[500,500]
|===

[cols=".>,.>"]
|===
a| .Tarja wi-fi
image::nic5.png[500,500] a| 
.100base-FX
image::nic6.jpg[500,500]
|===

////
==== Repetidor i Amplificador
 
[IMPORTANT]
====
Si les distàncies entre estacions són grans els efectes d’atenuació són importants i el senyal es degrada.

Els **repetidors** són dispositius que restauren el senyal al seu estat original i permeten que els receptors la rebin en condicions.

Els **amplificadors** són dispositius que amplifiquen el senyal i permeten allargar la distància de transmissió.
====

.Diferència entre amplificador i repetidor
image::amplivsrep.png[]

Característiques:

* Els dos operen a la capa 1
* Els amplificadors s’utilitzen amb senyals analògics i els repetidors amb senyals digitals.
* Els amplificadors amplifiquen el senyal, incloent-hi els errors i per tant cal que el senyal estigui en bon estat i existeix un límit en el nombre d’amplificadors en sèrie que es poden posar.
* Els repetidors reemeten el senyal per tant són menys susceptibles als errors que els anteriors. També tenen un límit a l’hora de posar-los en sèrie però és superior al dels amplificadors.

[cols=".>,.>"]
|===
a| .Ethernet extender
image::extender.png[500,500] a| 
.Diagrama muntatge de l'Ethernet extender
image::extenderdiagram.jpg[500,500]
|===

==== Concentrador (Hubs)

[IMPORTANT]
====
Un concentrador fa la funció de lladre elèctric concentrant tots els cables de la xarxa en un sol punt, i per tant compartint el medi de transmissió, és a dir formant un únic **segment de xarxa**

Consta de múltiples ports d'entrada/sortida de manera que un senyal entrant per un dels ports es propaga per tots els ports excepte el port original.

Actualment és un dispositiu obsolet i ha estat substituït per commutadors excepte en instal·lacions especialitzades.
====

[cols=".>,.>"]
|===
a| .Ethernet hub
image::hub.jpg[500,500] a| 
.Ethernet hub
image::hub2.jpg[500,500]
|===

Característiques:

* Els concentradors Ethernet apareixen com a evolució de les xarxes 10base5 i 10base2 amb **topologia física de bus**, per tant mantenen la **topologia lògica de bus** i adopten la **topologia física d'estrella**.
* La funció dels concentradors és centralitzar les connexions en un sol punt per a facilitar la feina de l'administrador de la xarxa detectant errors de connexió.
* Com a contrapartida si un concentrador falla tota la xarxa es veurà compromesa.
* Degut a la manera de funcionar dels concentradors, aquests només poden treballar a **half-dúplex** ja que si un ordinador esta enviant dades al segment de xarxa qualsevol altra enviament produiria una **col·lisió**.
* Operen a capa 1.
* Existeixen dos tipus de concentradors:
** Passius
**** Actuen només com a concentrador.
** Actius
**** A part d’actuar com a concentrador actuen també com a repetidor, a vegades s'anomenen **repetidors multiport**.
////

////
==== Media access unit (MAU)

[IMPORTANT]
====
Una MAU és un dispositiu de capa 1, actua com un concentrador, capaç d'ajuntar múltiples estacions en una **topologia física  d'estrella** però en una **topologia lògica d'anell**.

Va ser un producte actiu de IBM per a les seves xarxes **Token Ring** fins a finals dels anys noranta.

Actualment és un dispositiu obsolet i mai ha format part de les xarxes Ethernet.
====

[cols=".>,.>"]
|===
a| .MAU Token Ring
image::mau.jpg[500,500] a| 
.Topologia física i lògica d'una MAU
image::mautopology.gif[500,500]
|===

Els MAUS tenen dos tipus de ports:

* Ports “normals”
** Serveixen per connectar estacions de treball.
* Ports RI i RO, normalment només n’hi ha un de cada
** Són els ports Ring Input i Ring Output que permeten ampliar l’anell afegint-hi un altre MAU.

.Connexions d'una MAU
image::mauconnections.jpg[1200,1200]
////

////
==== Pont (Bridge)

[IMPORTANT]
====
* Les xarxes Ethernet muntades amb concentradors eren molt poc eficients i **escalaven** molt malament. El problema era que es formaven grans dominis de col·lisió que degradaven el rendiment de les xarxes.

* Els **ponts** van aparèixer com a primera solució per a fragmentar un gran domini de col·lisió en dos.

* Un **pont** és un dispositiu de capa 2 que permet dividir un segment de xarxa en dos.
====

Característiques:

* Permet unificar segments de xarxa de diferents topologies i diferents protocols MAC.
** Per exemple una xarxa 10baseT i una xarxa 10base2
* A nivell lògic és com si només hi hagués una sola xarxa.
* Un pont està format per dos connectors diferents (no necessàriament, poden ser iguals o poden haver-n’hi més de dos), cada un “enganxat” a una xarxa diferent.
* Operen a capa 2.
* Un pont, a diferencia d’un concentrador, no reenvia les trames enviades des de i dirigides al mateix segment de xarxa.
* Es poden muntar per software.
* Adapten les trames i els protocols de les dues xarxes.
* No examinen la carga útil dels paquets, no actuen com a firewalls.
* Un punt sense fils que disposa d'un port RJ45 es pot considerar un pont. 

.Bridge 10/100base-TX -> 100base-FX
image::bridge.jpg[500,500]
////

=== Commutador (switch)

[IMPORTANT]
====
Un **commutador** és un dispositiu de xarxes que connecta diferents nodes entre ells i utilitza una **commutació de paquets** per rebre, processar i enviar les trames del node origen al node destí.
====

[cols=".>,.>"]
|===
a| .Commutador Ethernet
image::switch1.jpg[500,500] a| 
.Commutador Ethernet
image::switch2.jpg[500,500]
|===

Característiques:

* Un commutador és un dispositiu que permet la interconnexió dintre de la mateixa xarxa a capa d’enllaç (alguns models poden operar a capa 3); per tant les capes 1 i 2 de tots els ordinadors que s’hi connecten han d’utilitzar els mateixos protocols.
* Un commutadors ve a ser un **pont multiport** de manera que segmenta la xarxa en tants segments de xarxa com ports té el commutador. I per tant a diferència dels concentradors:
** Un commutador es capaç d’enviar una trama únicament al port  on està connectada la màquina a qui va dirigida i per tant en certa manera la xarxa passa a ser de broadcast a ser commutada.
** Com que un commutador genera un segment de xarxa per port no produeix col·lisions entre els diferents dispositius connectats a cadascun dels ports del switch.

L'objectiu principal des commutadors és **segmentar la xarxa** on és troben.

==== Classificació segons la forma de segmentació de la xarxa

En funció de les capacitats dels commutadors podem distingir:

* Commutadors no gestionables (de capa 2)
** Funcionen com a ponts multiport.
** La seva finalitat principal és dividir la LAN en múltiples **dominis de col·lisió**
** No són capaços de filtrar broadcasts o multicasts.
* Commutadors gestionables (de capa 2)
** Tenen les mateixes caracterísiriques que els commutadors no gestionables i a més a més:
** Tenen possibilitat de configuració, possiblement:
*** Admeten la gestió de LANs virtuals (VLANs)
*** Permeten la configuració de llistes de control d'accés (ACL)
*** Permeten gestionar la seguretat dels ports.
* Commutadors de capa 3
** A més de les funcions dels commutadors de capa 2 poden admetre:
*** Algunes funcions d'encaminament.
*** Encaminament entre vLAns sense necessitat d'un encaminador extern.
*** Permeten la unió de segments de xarxa en diferents **dominis de difusió**

////
===== Factors de forma dels commutadors

[cols=".>,.>"]
|===
a| .Factors de forma dels commutadors
image::formfactors.png[600,600] a| 
.Transceptor SFP (small form-factor pluggable)
image::sfp.png[500,500]
|===

===== Classificació segons els mètodes de reenviament de les trames en els commutadors
 
Mètodes de reenviament de les trames en els commutadors

* Store and Forward
** Rep la trama completa, comprova que no té error i la reenvia pel port adequat.
* Cut through
** Reenvia el paquet immediatament després d'haver llegit l'adreça destí.
* Fragment free
** Emmagatzema els primers 64bytes de la trama abans de reenviar. 

===== Ports "normals" i ports uplink

Els commutadors poden tenir dos tipus de ports:

* Ports “normals”
** Serveixen per connectar estacions de treball utilitzant cables **sense creuar**.
* Ports “uplink”, normalment només n’hi ha un o dos (downlink)
** ÉS possible que admetin major velocitat que els ports "normals"
** Permet la connexió en cascada o en estrella d’un altre concentrador:
*** del port “uplink” d’un al un port “normal” de l’altre utilitzant cables **sense creuar**
*** del port “uplink” d’un al un port “uplink” de l’altre utilitzant cables **creuats**, pot ser útil si es vol aprofitar la major velocitat d'aquests ports.
** Alguns models permeten transformar un port “normal”, normalment el primer o l’últim,  en un port “uplink” prement el botó “crossover”.

.Detall d'un port uplink
image::uplink.jpg[500,500]
////

=== wireless access point

[IMPORTANT]
====
Un punt d'accés sense fils realitza la mateixa funció que un concentrador però sense cables. Tots els dispositius que estan dins el seu radi d’acció estan “connectats” a ell.

Tècnicament és un bridge, per una banda té una capa 2 wi-fi i per l'altra una capa 2 ethernet
====

.Punt d'accés sense fils
image::ap.jpg[500,500]

.Infraestructura d'una xarxa Wi-fi/Ethernet
image::wifiinfraestructure.png[1200,1200]

=== Encaminador (Router)

.Diferents encaminadors
image::routers.jpg[800,800]

[IMPORTANT]
====
Un **encaminador** (en anglès **router**) és un dispositiu de xarxa que reenvia paquets de dades entre xarxes de computadors. 

L'encaminador pertany al de nivell 3 del model OSI. 

Com que l'encaminador està connectat a dues o mes línies de diferents xarxes, quan un paquet de dades ve d'una de les línies, el router llegeix la informació de direcció en el paquet i així determina la seva destinació final. 

Després usant una política d'enrutament dirigeix els paquets en funció de certs paràmetres (origen, destí, congestió, seguretat...) a la següent xarxa, aquest paquet va passant d'una xarxa a una altra fins a arribar al seu destí.
====

Moltes empreses i organitzacions tenen la necessitat de connectar diferents xarxes locals entre elles per varies raons:

* Per un tema administratiu o de seguretat.
* Per que les topologies de les xarxes són diferents.
* Per que els protocols d’accés al medi són diferents.
* Per fragmentar grans dominis de difusió (és més comú utilitzar VLANs)

Per resoldre aquest problema existeixen dispositius de xarxa específics. 

* Un encaminador s’utilitza per interconnectar xarxes que treballen diferent a nivell de xarxa.
* Per tant té tantes targes de xarxa com xarxes ha de connectar.
* La seva funció bàsica es dirigir els paquets cap els seu destí, per a fer-ho:
** Rep un paquet.
*** Extreu l’adreça del destinatari.
*** Decideix quina es la millor ruta.
*** Envia el paquet cap al destinatari final en cas que estigui a una de les xarxes que connecta o cap a un altre encaminador en cas contrari.
* Aïlla completament (llevat de configuració) les xarxes que connecta.
* En particular no admet paquets de difusió. (Broadcast) ** Com a conseqüència trenca els dominis de difusió**.

Al parlar d’encaminadors es parla de:

* Ruta
** Camí que recorre un paquet des d’un origen a un destí.
* Ruta per defecte
** La ruta que utilitza l’encaminador quan cap de les rutes conegudes sembla que sigui la bona.
* Taula de rutes
** Conjunt de totes les rutes conegudes per l’encaminador.
          
[IMPORTANT]
====
Recordar que la capa 3 només té en compte els veïns immediats per tant els encaminadors no decideixen la ruta sencera dels paquets si no només determinen a quin encaminador adjacent els envien. 
==== 

////
=== Pasarel·la (Gateway)

* Connecta xarxes amb arquitectures diferents (TCP/IP, ATM, X.25, etc...)
* Son components molt complexes que operen a capa 4 i/o a capa 7
* Per exemple si tenim una xarxa que treballa amb circuits virtuals i una altra que treballa amb datagrames caldrà una passarel·la entre elles.
* Molt sovint es munten amb software. 
////

=== Accés a Internet de banda ampla – Router DSL

.Encaminador DSL
image::routeradsl.png[1000,1000]

ifdef::slides[<<<]
ifdef::slides[=== Accés a Internet de banda ampla – Router DSL]

[IMPORTANT]
====
Un encaminador DSL sol ser l'agregació d'un gateway per accés a Internet de banda ampla, un commutador i un punt d'accés sense fils.

Estrictament parlant no és un encaminador
====

////
=== PoE Hub (Concentrador Power over ethernet)

.Concentrador PoE
image::poe.png[400,400]

ifdef::slides[<<<]
ifdef::slides[=== PoE Hub (Concentrador Power over ethernet)]

[IMPORTANT]
====
Permet passar electricitat per una infraestructura LAN estàndard amb cables de CAT5 com a mínim amb l’objectiu de donar corrent a dispositius de xarxa de baix consum com per exemple telèfons voip, càmeres ip o punts d’accés sense fils.
====

Característiques:

* Elimina la necessitat d’utilitzar preses de corrent.
* Permet un ús més senzill dels SAI.
* El corrent subministrat a través de la infraestructura LAN s’activa de manera automàtica si s’identifica un dispositiu compatible, en cas contrari es bloqueja.
*  Existeixen uns adaptadors que permeten convertir dispositius que no suporten PoE en dispositius compatibles. 

=== Domini de col·lisió

[IMPORTANT]
====
Un **domini de col·lisió** és una part de la xarxa on hi poden haver col·lisions.
====

|===
a| 
* En general les col·lisions només es poden produir en entorns half-duplex, o el que és el mateix, en segments de xarxa.
* Els ponts, els commutadors i els encaminadors separen els dominis de col·lisió
* Els concentradors (hubs) no separen els dominis de col·lisió. a|
.Exemple de dominis de col·lisió
image::dominicolisio.png[500,500]
|===

=== Domini de difusió

[IMPORTANT]
====
Un **domini de difusió** (broadcast) és una part de la xarxa en la que es propaga un enviament de broadcast.
====

|===
a|
* Un domini de difusió inclou tots els nodes que es poden connectar entre ells a capa 2.
* Tots els ports en un commutador o en un concentrador estan per defecte dins del mateix domini de difusió.
* Tots els ports d'un encaminador estan en dominis de difusió diferents. A més, els encaminadors no propaguen els broadcasts d'un domini de difusió a un altre.
* Les VLAN serveixen per fragmentar els dominis de difusió d'una xarxa. a|
.Exemple de dominis de difusió
image::dominidifusio.png[500,500]
|===
////