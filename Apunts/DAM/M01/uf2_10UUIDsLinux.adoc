== Introducció als UUID

Un UUID (Universal Unique Identifier) s'utilitza per identificar objectes de forma única.

Està basat en un estàndard que permet crear identificadors únics de 128 bits.

=== Veure els UUID dels discs durs ((tune2fs))

El programa *_tune2fs_* permet modificar els paràmetres de les particions ext2/ext3/ext4 entre d'altres coses es pot utilitzar per:

. Mostrar informació de la partició (Similar a *_dumpe2fs -h_*)
. Habilitar la comprovació de la partició passat un nombre determinat de dies o de muntatges.

.Per saber el UUID d'una partició
[source, bash]
----
josep@odin:~$ sudo tune2fs -l /dev/sda1 | grep UUID
Filesystem UUID:          72a76dcc-489d-464e-9015-be2e8f96fd8c
----

.Passarà un fsck cada 5 muntatges
[source, bash]
----
josep@odin:~$ sudo tune2fs -c 5 /dev/sda1
tune2fs 1.43.4 (31-Jan-2017)
Se pone la cuenta de montajes máxima a 5
----

.Passarà un fsck cada 10 dies
[source, bash]
----
josep@odin:~$ sudo tune2fs -i 10d /dev/sda1
tune2fs 1.43.4 (31-Jan-2017)
Se pone el intervalo entre revisiones en 864000 segundos
----

=== Veure els UUID dels discs durs ((blkid))

La comanda *_blkid_* permet veure els atributs dels dispositius de bloc.

[source, bash]
----
josep@odin:~$ sudo blkid
/dev/sdd1: UUID="tRiqK5-hhFM-eOLS-E2d8-5bIr-WapB-Am4bcl" TYPE="LVM2_member" PARTUUID="bc2c22d3-01"
/dev/sda1: UUID="72a76dcc-489d-464e-9015-be2e8f96fd8c" TYPE="ext4" PARTUUID="d7e83d99-01"
/dev/sda5: UUID="7d221b00-9bcc-4479-b177-3ae944f8089e" TYPE="swap" PARTUUID="d7e83d99-05"
/dev/sdb1: UUID="HlWsiQ-Ft2z-MNnd-EkB7-BmVI-ZmJ3-N5WBWp" TYPE="LVM2_member" PARTUUID="5fb8ebdb-01"
/dev/sdc: UUID="xcCg0C-mz3a-oZOy-m3Gj-tOMs-t8Kx-J3lkFD" TYPE="LVM2_member"
/dev/sde1: UUID="78d55e5c-2f9d-4c3e-9d45-b54121c110d7" TYPE="ext4" PARTUUID="aad602c7-01"
/dev/mapper/vgrup1-lvol0: UUID="26943935-93e9-446c-987b-d4153b6b1ba2" TYPE="ext4"
/dev/mapper/vgrup2-lvol0: UUID="6d73464f-c02e-48c4-8101-19394de0e25b" TYPE="ext4"
----

=== uuid al fitxer /etc/fstab

Es pot utilitzar un UUID per garantir que un volum s'identifica de manera única a *_/etc/fstab_*. Si s'identifica amb un nom aquest pot canviar en funció dels discs que es trobin a l'arrencada del sistema, si s'identifica amb un UUID això no passarà.

Per exemple, si tenim tres discs *_/dev/sda_*, *_/dev/sdb_* i *_/dev/sdc_* i treiem el *_/dev/sdb_*, el tercer disc, *_/dev/sdc_* canviarà de nom i passarà a ser el *_/dev/sdb_*, si treballem amb uuids això no passarà.

[source, bash]
----
josep@odin:~$ cat /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=72a76dcc-489d-464e-9015-be2e8f96fd8c /               ext4    errors=remount-ro 0       1
# swap was on /dev/sda5 during installation
UUID=7d221b00-9bcc-4479-b177-3ae944f8089e none            swap    sw              0       0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
----

=== uuid com a dispositiu d'arrencada

Les distribucions actuals de Linux identifiquen el sistema de fitxers d'arrencada amb un UUID.

[source, bash]
----
josep@odin:~$ sudo dmesg | grep UUID
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-4.9.0-8-amd64 root=UUID=72a76dcc-489d-464e-9015-be2e8f96fd8c ro quiet
[    0.000000] Kernel command line: BOOT_IMAGE=/boot/vmlinuz-4.9.0-8-amd64 root=UUID=72a76dcc-489d-464e-9015-be2e8f96fd8c ro quiet
----
