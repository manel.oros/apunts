== Muntar sistemes de fitxers

En el moment en que es posa un sistema de fitxers en una partició aquesta es pot muntar.

Muntar un sistema de fitxers fa que aquest estigui disponible per a ser utilitzat, normalment en forma de directori.

En general es diu muntar un sistema de fitxers enlloc de muntar una partició per què és possible muntar sistemes de fitxers que no es basen en particions.

En els sistemes Linux, cada fitxer i cada directori formen part d'un mateix arbre de fitxers. Quan s'afegeix un sistema de fitxers al ordinador cal fer-lo disponible en algun lloc de l'arbre de fitxers. El directori on s'enllaça un sistema de fitxers s'anomena un *((punt de muntatge))*.

=== Punts de muntatge

Un *punt de muntatge és* el directori en el qual es fan accessibles un nou sistema d'arxius o un directori. Per a muntar un sistema d'arxius o un directori, el punt de muntatge ha de ser un directori.

Normalment, el sistema d'arxius o el directori es munten en un punt de muntatge buit, però no és necessariament. *Si l'arxiu o directori que serveix de punt de muntatge conté dades*, aquests no estaran accessibles mentre un altre arxiu o directori estigui muntat sobre ells. 

Quan un sistema d'arxius es munta en un directori, *els permisos del directori arrel del sistema d'arxius muntat* tenen prioritat sobre els permisos del punt de muntatge. 

////
L'única excepció és l'entrada de directori pare .. (punt punt) en el directori muntat. Perquè el sistema operatiu accedeixi al nou sistema d'arxius, la informació del directori pare del punt de muntatge ha d'estar disponible.

Per exemple, si el directori actual és *_/home/josep_*, la comanda _cd .._ canvia el directori de treball a *_/home_*. Si el directori *_/home/josep_* és l'arrel d'un sistema d'arxius muntat, el sistema operatiu ha de buscar la informació de directori pare en el directori *_/home/josep_* perquè la comanda _cd .._ tingui èxit.

Per a les comandes que necessiten informació de directori pare per a ser executades, els usuaris han de tenir permís de cerca (*_x_*) en el directori muntat. Si el directori muntat no pot atorgar el permís de cerca els resultats poden ser imprevisibles, especialment perquè els permisos del directori muntat no són visibles. 

Aquest problema es pot evitar establint sempre els permisos del directori muntat en 111 com a mínim.
////

=== Muntar sistemes de fitxers locals

==== ((mkdir))

Per crear un nou punt de muntatge s'utilitza la comanda *mkdir*.

[source,bash]
----
josep@hades:~$ mkdir puntmuntatge
----

==== ((mount))

Si el ja s'ha creat un punt de muntatge i el sistema de fitxers està present a la partició es pot utilitzar la comanda *mount* per muntar el sistema de fitxers al punt de muntatge.

[source,bash]
----
josep@hades:~$ sudo mount -t ext4 /dev/sdb1 ./puntmuntatge/
----

Un cop muntat, el sistema de fitxers és accessible als usuaris.

===== mount -a

Munta tots els sistemes de fitxes presents a *_/etc/fstab_*.

===== mount -r

Munta el sistema de fitxers amb permissos de només lectura.

[source, bash]
----
josep@hades:~$ sudo mount -r -t ext4 /dev/sdb1 ./puntmuntatge/
josep@hades:~$ cd puntmuntatge/
josep@hades:~/puntmuntatge$ touch file
touch: no se puede efectuar `touch' sobre 'file': Sistema de ficheros de sólo lectura
----

===== mount -v

Munta un sistema de fitxers donant informació ampliada del muntatge.

===== mount -t

Permet indicar el sistema de fitxers que es vol muntar, en general es detecta automàticament .


[source, bash]
----
josep@hades:~$ ls -l /dev/cd*
lrwxrwxrwx 1 root root 3 ene 21 16:06 /dev/cdrom -> sr0
----

[source, bash]
----
josep@hades:~$ sudo mount -t iso9660 /dev/sr0 /media/cdrom0
mount: /dev/sr0 está protegido contra escritura; se monta como sólo lectura
----

===== mount -U

Permet muntar un sistema de fitxers indicant-ne el UUID enlloc del nom.

[source, bash]
----
josep@hades:~$ sudo blkid | grep /dev/sdb1
/dev/sdb1: LABEL="home_disk" UUID="49e25359-c5ec-4df7-803d-36b326cd9fbe" TYPE="ext4" PARTUUID="199fbb44-01"
----

[source, bash]
----
josep@hades:~$ sudo mount -U 49e25359-c5ec-4df7-803d-36b326cd9fbe ./puntmuntatge
----

==== ((/etc/filesystems)) 

En la majoria dels casos no cal explicitar el tipus de dispositiu (*-t*). La comanda mount és capaç de detectar automàticament un gran nombre de sistemes de fitxers.

Quan es munta un sistema de fitxers sense especificar-ne el tipus, la comanda mount anirà provant els diferents tipus de sistemes de fitxers enumerats a *_/etc/filesystems_*.

[NOTE]
====
La comanda *mount* ignora les línies que contenen la directiva *nodev* .
====

[source,bash]
----
usuari@RHELv4u4:~$ cat /etc/filesystems
ext3
ext2
nodev proc
nodev devpts
iso9660
vfat
hfs
----

==== ((/proc/filesystems))

En el cas que *_/etc/filesystems_* no existeixi o bé acabi amb un * a la última línia, és llegeix el fitxer *_/proc/filesystems_*.

[source,bash]
----
josep@hades:~$ cat /proc/filesystems 
nodev	sysfs
nodev	rootfs
nodev	ramfs
 ...
nodev	mqueue
	ext3
	ext2
	ext4
nodev	autofs
nodev	vboxsf
	iso9660
----

==== ((umount))

Permet desmuntar una sistema de fitxers previament muntat amb mount.

[source,bash]
----
josep@hades:~$ sudo umount ./puntmuntatge
----

=== Muntar un path sobre un altre path

La comanda _mount_ permet muntar una ruta sobre una altre ruta enlloc de un sistema de fitxers sobre una ruta.

Fer aixó s'anomena un *bind mount* i s'aconsegueix amb la opció *_--bind_* de la comanda *_mount_*.

[source, bash]
----
josep@odin:~$ pwd
/home/josep
josep@odin:~$ mkdir toroot

josep@odin:~$ sudo mount --bind / /home/josep/toroot
josep@odin:~$ cd toroot
josep@odin:~/toroot$ ls
bin   home            lib64       opt   sbin     sys  vmlinuz
boot  initrd.img      lost+found  proc  sda.mbr  tmp  vmlinuz.old
dev   initrd.img.old  media       root  sdc.mbr  usr
etc   lib             mnt         run   srv      var

josep@odin:~/toroot$ mount | grep toroot
/dev/sda1 on /home/josep/toroot type ext4 (rw,relatime,errors=remount-ro,data=ordered)
----

[WARNING]
====
Si tenim un sistema de fitxers muntat en un punt de muntatge dins de la ruta muntada *aquest no serà accessible des del nou punt de muntatge*. 

Aquesta seria la situació si tinguessim *_/var/log_* a *_/dev/sda2_* i la resta de directoris a *_deb/sda1_*, si fessim:

[source, bash]
----
mount --bind / /home/josep/toroot
----

No podriem accedir a *_/val/log_* des del nou punt de muntatge, hauriem de fer el muntatge recursivament:

[source, bash]
----
josep@odin:~$ sudo mount --rbind / /home/josep/toroot
----
====

=== Mostrar sistemes de fitxers muntats

Per mostrar tots els sistemes de fitxers muntats es pot utilitzar la comanda *mount* o bé mirar els fitxers */proc/mounts* i */etc/mtab*

==== mount

La manera més simple de veure tots els "mounts" és amb la comanda *mount* sense arguments.

[source,bash]
----
josep@hades:~$ mount | grep /dev/sda
/dev/sda1 on / type ext4 (rw,relatime,errors=remount-ro,data=ordered)
----

==== ((/proc/mounts))

El kernel proporciona la informació sobre els punts de muntatge dins del fitxer /proc/mounts.

[source,bash]
----
josep@hades:~$ cat /proc/mounts | grep /dev/sda
/dev/sda1 / ext4 rw,relatime,errors=remount-ro,data=ordered 0 0
----

==== ((/etc/mtab))

El fitxer _/etc/mtab_ no s'actualitza directament pel kernel, sinò que l'actualitza directament la comanda mount.

No editar aquest fitxer manualment!!

[source,bash]
----
josep@hades:~$ cat /etc/mtab | grep /dev/sda
/dev/sda1 / ext4 rw,relatime,errors=remount-ro,data=ordered 0 0
----

==== ((df))

Una altra manera de mirar els fitxers muntats és amb la comanda *df* (diskfree). Aquesta comanda a més de mostrar els sistemes de fitxers muntats proporciona informació sobre l'espai lliure a cada disk muntat.

[source,bash]
----
josep@hades:~$ df
S.ficheros     bloques de 1K    Usados Disponibles Uso% Montado en
udev                  498892         0      498892   0% /dev
tmpfs                 102036      3596       98440   4% /run
/dev/sda1           20262736   3708920    15501452  20% /
tmpfs                 510176         0      510176   0% /dev/shm
tmpfs                   5120         4        5116   1% /run/lock
tmpfs                 510176         0      510176   0% /sys/fs/cgroup
shared             243680252 180109444    63570808  74% /media/sf_shared
tmpfs                 102032         4      102028   1% /run/user/110
tmpfs                 102032        12      102020   1% /run/user/1000
/dev/sr0              163036    163036           0 100% /media/cdrom0
----

==== df -h

*df -h* mostra la informació que mostraria df però en format llegible.

[source,bash]
----
josep@hades:~$ df -h
S.ficheros     Tamaño Usados  Disp Uso% Montado en
udev             488M      0  488M   0% /dev
tmpfs            100M   3,6M   97M   4% /run
/dev/sda1         20G   3,6G   15G  20% /
tmpfs            499M      0  499M   0% /dev/shm
tmpfs            5,0M   4,0K  5,0M   1% /run/lock
tmpfs            499M      0  499M   0% /sys/fs/cgroup
shared           233G   172G   61G  74% /media/sf_shared
tmpfs            100M   4,0K  100M   1% /run/user/110
tmpfs            100M    12K  100M   1% /run/user/1000
/dev/sr0         160M   160M     0 100% /media/cdrom0
----

==== ((du))

Aquesta comanda permet summaritzar l'ús del disk per fitxers i per directoris. Utilitzar du (diskusage) a un punt de muntatge mostra l'espai de disk utilitzat pel sistema de fitxers.

La opció *-s* permet llançar du recursivament i dona la quantitat de disk usat per un diretori i tots els subdirectoris.

[source,bash]
----
josep@hades:~$ sudo du -sh /home
72M	/home
----

=== Un exemple complet

.El disk es detecta correctament a l'arrancada
[source, bash]
----
josep@hades:~$ sudo dmesg | grep '\[sdb\]'
[    2.286768] sd 3:0:0:0: [sdb] 16777216 512-byte logical blocks: (8.59 GB/8.00 GiB)
[    2.286812] sd 3:0:0:0: [sdb] Write Protect is off
[    2.286815] sd 3:0:0:0: [sdb] Mode Sense: 00 3a 00 00
[    2.286835] sd 3:0:0:0: [sdb] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[    2.287661] sd 3:0:0:0: [sdb] Attached SCSI disk

----

.Creem una taula de particions i una partició
[source, bash]
----
josep@hades:~$ sudo parted /dev/sdb
GNU Parted 3.2
Using /dev/sdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) mklabel msdos                                                   
(parted) mkpart primary ext4 1 8000
(parted) print                                                            
Model: ATA VBOX HARDDISK (scsi)
Disk /dev/sdb: 8590MB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags: 

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  8000MB  7999MB  primary  ext4         lba

(parted) quit  
Information: You may need to update /etc/fstab.
----

.Assignem un sistema de fitxers a la partició anterior
[source, bash]
----
josep@hades:~$ sudo mkfs.ext4 /dev/sdb1
mke2fs 1.43.4 (31-Jan-2017)
Se está creando un sistema de ficheros con 1952768 bloques de 4k y 488640 nodos-i
UUID del sistema de ficheros: b830bc58-d7af-49ec-b478-6e69bf29bde7
Respaldo del superbloque guardado en los bloques: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

Reservando las tablas de grupo: hecho                           
Escribiendo las tablas de nodos-i: hecho                           
Creando el fichero de transacciones (16384 bloques): hecho
Escribiendo superbloques y la información contable del sistema de ficheros: hecho
----

.Muntem la partició (/mnt esta pensat per muntatges temporals)
[source, bash]
----
josep@hades:~$ sudo mount /dev/sdb1 /mnt
josep@hades:~$ mount | grep /mnt
/dev/sdb1 on /mnt type ext4 (rw,relatime,data=ordered)
----

.Mirem l'estat de la partició
[source, bash]
----
josep@hades:~$ df -h | grep mnt
/dev/sdb1        7,3G    34M  6,9G   1% /mnt
----

.Mirem l'ús que s'en està fent
[source, bash]
----
josep@hades:~$ sudo du -sh /mnt
20K	/mnt
----

.Generem un fitxer a la partició
[source, bash]
----
josep@hades:~$ sudo -i
josep@hades:~$ sudo head -c 1G < /dev/urandom > /mnt/fitxer
----

.Mirem l'ús que s'en està fent
[source, bash]
----
josep@hades:~$ sudo du -sh /mnt
1,1G	/mnt
----

.Desmontem la partició
[source, bash]
----
josep@hades:~$ sudo umount /mnt
----

=== Muntar permanentment

Fins ara els punts de muntatge desapareixen al reiniciar el PC. A continuació s'explica com fer els punts de muntatge permanents.

==== ((/etc/fstab))

La "file system table" localitzada a *_/etc/fstab_* conté una llista de sistemes de fitxers amb la opció de muntar-los automàticament a l'arrancar el sistema.

[source,bash]
----
josep@hades:~$ cat /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=525b742c-39b8-4c3a-8000-2cf9f2959138 /               ext4    errors=remount-ro 0       1
# swap was on /dev/sda5 during installation
UUID=91876273-99ce-4d85-a969-85553935d2fb none            swap    sw              0       0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
----

Afegint la següent línia es pot automatitzar el muntatge del sistema de fitxers.

[source,bash]
----
/dev/sdb1       /home/josep     ext4    defaults        0       0
----

El valor de cada columna es el següent::
<file system> ::: És l'identificador de la partició, el UUID, aquest número el podem obtenir mitjançant la comanda : *_((blkid))_*. A sobre de cada línia i a mode de comentari, sempre trobem la direcció lògica a que fa referència el UUID de sota.
<mount point> ::: es la ruta del punt de muntatge de la partició.
<type> ::: es el tipus de sistema de fitxers en que s'ha formatat la partició.
<options> ::: són opcions de muntatge, si no en volem cap d'especial utilitzarem *defaults*. Algunes de les principals opcions són:
defaults:::: No volem cap opció de muntatge.
rw:::: permisos de lectura i escriptura.
ro:::: permís de només lectura.
noexec:::: La opció *_noexec_* evita que es puguin executar binàris al sistema de fitxers.

auto/noauto:::: muntar o no muntar automàticament amb *_mount -a_*.
user/nouser:::: pot ser muntat i desmuntat per un usuari diferent de root o no.
umask=_valor_, uid=_valor_, gid=_valor_:::: Permet establir el _umask_, uid_ i gid_ del punt de muntatge, útil si el sistema de fitxers muntat no reconeix els permisos Linux.
loop:::: Munta un fitxer com si es tractés d'una partició. Útil per muntar isos per exemple.
nosuid::::Aquesta opció ignora el bit *_setuid_* al sistema de fitxers.
+
[NOTE]
====
Recordeu que el permís *setuid* fa que el fitxer s'executi amb els permisos del propietari del fitxer enlloc dels permisos de qui ha llançat l'executable. 
====
noacl:::: No permet la utilització de acls al sistema de fitxers.
<dump> ::: Indica si aquesta partició s'inclourà en el sistema automàtic de backup. (*0 vol dir desactivat*).
<pass> ::: Indica l'ordre en que es comprovarà l'integritat de les particions al arrancar. (*0 vol dir desactivat*).

==== mount -o

Totes les opcions que es poden especificar al fitxer *_/ect/fstab_* tambés es poden especificar mitjançant la comanda mount amb la opció *_-o_*.

Per exemple:

[source, bash]
----
josep@hades:~$ sudo mount -o umask=0000 /dev/sdc1 puntmuntatge/

josep@hades:~$ ls -ld puntmuntatge/
drwxrwxrwx 2 root root 4096 ene  1  1970 puntmuntatge/

josep@hades:~$ cd puntmuntatge/
josep@hades:~/puntmuntatge$ touch b

josep@hades:~/puntmuntatge$ ls -l
total 3
-rwxrwxrwx 1 root root  0 ene 31 16:08 b
----

[source,bash]
----
josep@odin:~$ sudo umount /home/test
josep@odin:~$ sudo mount -t ext4 -o noexec /dev/sde1 /home/test

josep@odin:~$ cp /bin/cat /home/test

josep@odin:~$ /home/test/cat
bash: /home/test/cat: Permiso denegado
josep@odin:~$ ls -l /home/test/cat
-rwxr-xr-x 1 root root 35688 dic 19 11:30 /home/test/cat
----

[source,bash]
----
josep@odin:~$ sudo mount -t ext4 -o ro /dev/sde1 /home/test
josep@odin:~$ touch /home/test/fitxer
touch: no se puede efectuar `touch' sobre '/home/test/fitxer': Sistema de ficheros de sólo lectura
----

==== mount /mountpoint

Afegir una entrada a *_/etc/fstab_* facilita el muntatge dels sistemes de fitxers. Si un punt de muntatge està donat d'alta a *_/etc/fstab_* n'hi ha prou en llançar la comanda següent per muntar-lo.

[source, bash]
----
josep@odin:~$ cat /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=72a76dcc-489d-464e-9015-be2e8f96fd8c /               ext4    errors=remount-ro 0       1
# swap was on /dev/sda5 during installation
UUID=7d221b00-9bcc-4479-b177-3ae944f8089e none            swap    sw              0       0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0


josep@odin:~$ mount /media/cdrom0
mount: /dev/sr0 está protegido contra escritura; se monta como sólo lectura
----

=== Muntar sistemes de fitxers remots

Ho veurem quan parlem de NFS i de Samba.


////
==== smb/cifs
TODO

The Samba team (samba.org) has a Unix/Linux service that is compatible with the SMB/
CIFS protocol. This protocol is mainly used by networked Microsoft Windows computers.
Connecting to a Samba server (or to a Microsoft computer) is also done with the mount
command.
This example shows how to connect to the 10.0.0.42 server, to a share named data2.

[source,bash]
----
[root@centos65 ~]# mount -t cifs -o user=usuari //10.0.0.42/data2 /home/data2
Password:
[root@centos65 ~]# mount | grep cifs
//10.0.0.42/data2 on /home/data2 type cifs (rw)
----

The above requires yum install cifs-client.

==== nfs
TODO

Unix servers often use nfs (aka the network file system) to share directories over the network.
Setting up an nfs server is discussed later. Connecting as a client to an nfs server is done
with mount, and is very similar to connecting to local storage.
This command shows how to connect to the nfs server named server42, which is sharing
the directory /srv/data. The mount point at the end of the command (/home/data) must
already exist.

[source,bash]
----
[root@centos65 ~]# mount -t nfs server42:/srv/data /home/data
[root@centos65 ~]#
If this server42 has ip-address 10.0.0.42 then you can also write:
[root@centos65 ~]# mount -t nfs 10.0.0.42:/srv/data /home/data
[root@centos65 ~]# mount | grep data
10.0.0.42:/srv/data on /home/data type nfs (rw,vers=4,addr=10.0.0.42,clienta\
ddr=10.0.0.33)
----

==== Opcions de muntatge específiques de nfs


bg If mount fails, retry in background.
fg (default)If mount fails, retry in foreground.
soft Stop trying to mount after X attempts.
hard (default)Continue trying to mount.
The soft+bg options combined guarantee the fastest client boot if there are NFS problems.
retrans=X Try X times to connect (over udp).
tcp Force tcp (default and supported)
udp Force udp (unsupported)
////
