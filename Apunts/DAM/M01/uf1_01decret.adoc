[preface]
== Resultats d’aprenentatge i criteris d’avaluació

. Instal·la sistemes operatius, analitzant-ne les característiques i interpretant-ne la documentació tècnica.
[arabic]
.. Identifica els elements funcionals d’un sistema informàtic i els seus mecanismes d’interconnexió.
.. Identifica les característiques, funcions i arquitectura d’un sistema operatiu.
.. Compara diferents sistemes operatius, les seves versions i llicències d’ús, en funció dels seus requisits, característiques i camps d’aplicació.
.. Realitza instal·lacions de diferents sistemes operatius.
.. Preveu i aplica tècniques d’actualització i rescats.
.. Soluciona incidències del sistema i del procés d’inici.
.. Utilitza eines per conèixer el programari instal·lat en el sistema i el seu origen.
.. Elabora documentació de suport relativa a les instal·lacions efectuades i les incidències detectades.

. Configura el programari de base, atenent les necessitats d’explotació del sistema informàtic.
[arabic]
.. Planifica, crea i configura comptes d’usuari, grups, perfils i polítiques de contrasenyes locals.
.. Assegura l’accés al sistema mitjançant l’ús de directives de compte i directives de contrasenyes.
.. Actua sobre els serveis i processos en funció de les necessitats del sistema.
.. Instal·la, configura i verifica protocols de xarxa.
.. Analitza i configura els diferents mètodes de resolució de noms.
.. Optimitza l’ús dels sistemes operatius per a sistemes portàtils.
.. Utilitza màquines virtuals per realitzar tasques de configuració de sistemes operatius i analitzar-ne els resultats.
.. Documenta les tasques de configuració del programari de base.

[preface]
== Continguts

. Instal·lació de programari lliure i propietari:
[arabic]
.. Estructura i components d’un sistema informàtic. Perifèrics i adaptadors per a la connexió de dispositius. Tipus de xarxes, cablatge i connectors.
.. Mapa físic i lògic d’una xarxa.
.. Arquitectura d’un sistema operatiu.
.. Funcions d’un sistema operatiu.
.. Tipus de sistemes operatius.
.. Tipus d’aplicacions.
.. Llicències i tipus de llicències.
.. Gestors d’arrencada.
.. Consideracions prèvies a la instal·lació de sistemes operatius lliures i propietaris.
.. Instal·lació de sistemes operatius. Requisits, versions i llicències.
.. Instal·lació/desinstal·lació d’aplicacions. Requisits, versions i llicències.
.. Actualització de sistemes operatius i aplicacions.
.. Fitxers d’inici de sistemes operatius.
.. Registre del sistema.
.. Actualització i manteniment de controladors de dispositius.

. Administració de programari de base:
[arabic]
.. Administració d’usuaris i grups locals.
.. Usuaris i grups predeterminats.
.. Seguretat de comptes d’usuari.
.. Seguretat de contrasenyes.
.. Administració de perfils locals d’usuari.
.. Configuració del protocol TCP/IP en un client de xarxa. Direccions IP. Màscares de subxarxa.
.. Configuració de la resolució de noms.
.. Fitxers de configuració de xarxa.
.. Optimització de sistemes per a ordinadors portàtils. Arxius de xarxa sense connexió.
