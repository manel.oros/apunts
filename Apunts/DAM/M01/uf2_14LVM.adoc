== ((LVM))

// https://wiki.archlinux.org/index.php/LVM_(Espa%C3%B1ol)

// https://blog.inittab.org/administracion-sistemas/lvm-para-torpes-i/

De forma simplificada podríem dir que LVM (*Logical Volume Manager*) és una capa d'abstracció entre un dispositiu d'emmagatzematge (per exemple un disc) i un sistema de fitxers. 

En realitat poden existir múltiples capes com ara capes de xifrat o capes de raid tant per damunt o per sota de LVM. 

D'entrada *LVM estarà entre els nostres discs físics i els sistemes de fitxers*.

Els avantatges que tenen són múltiples, però la inicial i més evident és la flexibilitat davant del particionat tradicional. Per exemple:

* Suposem que tenim 4 particions contigües en un disc. Si en el futur volguéssim  augmentar-ne alguna de les 3 primeres no podríem fer-ho sense esborrar les següents, la qual cosa és complexa, perillosa i requereix de parada del servei. Si volguéssim ampliar l'última, sempre tindríem el límit de la grandària del disc. 

* Suposem que comprem un disc nou, i volem ampliar l'espai d'un sistema de fitxers existent en el disc anterior amb l'espai nou, impossible excepte amb “suplements” de nous sistemes de fitxers i punts de muntatge.

Amb LVM totes aquestes limitacions desapareixen. 

* Podem augmentar les seves “particions” (volums lògics d'ara endavant) independentment que no hi hagi espai lliure contigu a aquestes. 
* Podem augmentar els seus volums lògics amb espai lliure de diferents discs físics. 
* I fins i tot podem moure volums lògics entre dispositius físics.

Tot això en calent, Sense desmuntar el sistema de fitxers i sense parar el servei.

=== Blocs de construcció de LVM

.Blocs de construcció de LVMfootnote:[https://commons.wikimedia.org/wiki/File:Lvm.svg]
image::lvm.png[]

LVM permet proporcionar un sistema de particions independents de l'estructura subjacent del disc. Amb LVM és possible crear un espai d'emmagatzematge abstracte així com diferents «particions virtuals», per la qual cosa és més fàcil d'engrandir/encongir particions (sempre subjecte a possibles limitacions del sistema d'arxius subjacent).

Les particions virtuals permeten afegir/eliminar particions sense haver de preocupar-se sobre si es té suficient espai contigu en un disc concret, ni haver de moure altres particions en el camí.

LVM proporciona facilitat de gestió, LVM *no* proporciona cap seguretat.

Els blocs bàsics que construeixen LVM són:

Physical volume (PV)::
"Part" d'un dispositiu de bloc Unix, utilitzable per a emmagatzemar dades  per LVM.
+
Un PV no cal formatar-ho, simplement es proporciona a LVM “en cru” i des d'aquest moment serà gestionat pel propi LVM, no tornarem a tocar-lo.

Per exemple::: 
* Un disc dur.
* Una partició MBR o GPT
* Una targeta SD o similar.
* Un dispositiu RAID
* Un dispositiu loopback (és capaç de convertir un fitxer en un dispositiu de bloc)
* Un dispositiu xifrat.
* Un volum lògic.

Volume group (VG)::
Grup de volums físics que serveix de contenidor per a volums lògics. 
+
Per a poder usar l'espai/emmagatzematge d'un *PV*, aquest ha de pertànyer a un Grup de volums (VG). 
+
Podem dir que un VG és *una espècie de disc dur virtual*. Un VG és un “disc” compost d'UN o més PVs i que creix simplement afegint més PVs. A diferència d'un disc real, un VG pot créixer amb el temps, només cal “donar-li” un PV més. 
+
En una màquina amb un només disc podem crear un VG que estigui compost per només un PV (el disc físic o una de les seves particions). Si amb el temps ens quedem sense espai en el VG, comprem un altre disc *PV), l'afegim al VG i la resta és transparent per a sistemes de fitxers, processos o usuaris.
    
Logical volume (LV)::
«Partició virtual/lògica» que resideix en un grup de volums. Els volums lògics són dispositius de bloc d'Unix anàlegs a les particions físiques, per exemple, es poden formatar directament amb un sistema d'arxius.
+
Els volums lògics (LV) són “el producte final” de LVM. Són els dispositius que usarem per a crear sistemes de fitxers, particions swap, discs per a màquines virtuals, etc… 
+
Per seguir amb l'analogia del “disc dur virtual” que és el VG, els LVs serien les particions. Amb els quals treballarem realment. A diferència de “les seves cosines” les particions tradicionals, els LVs poden créixer (mentre hi hagi espai en el VG) independentment de la posició en la qual estiguin, fins i tot expandint-se per diferents PVs. 
+
Un LV de 1G pot estar compost de 200MB procedents d'un disc dur, 400MB d'un RAID per software, i 400MB d'una partició en un tercer dispositiu físic. L'únic requisit és que tot els PVs pertanyin al mateix VG. 

Physical extent (PE)::
L'extensió contigua més petita (per defecte 4 MiB) en el volum físic que es pot assignar a un volum lògic. 
+
Cada PE s'utilitza completament o no s'utilitza en un LV en particular.
+
Per analogia podríem considerar que els PE fan el paper de clústers en un disc dur.

Exemple:

.Discs físics
[ditaa, options="--scale 2.0, --no-separation , --no-shadows", file="images/uml/lvm2.png"]
....
  Disc1 (/dev/sda):
    +---------------------------------+----------------------------------+
    | Partició1 50 GiB (Volum físic)  | Partició2 80 GiB (Volum físic)   |
    | /dev/sda1                       | /dev/sda2                        |
    +---------------------------------+----------------------------------+
                                    
  Disc2 (/dev/sdb):
    +-------------------------------------------------+
    | Partició1 120 GiB (Volum físic)                 |
    | /dev/sdb1                                       |
    +-------------------------------------------------+
....

.Volums lògics LVM
[ditaa, options="--scale 2.0, --no-separation , --no-shadows", file="images/uml/lvm3.png"]
....
  Grup de volum1 (/dev/vg1/ = /dev/sda1 + /dev/sda2 + /dev/sdb1):
    +---------------------+---------------------+----------------------+
    | Volum lògic1 15 GiB | Volum lògic2 35 GiB | Volum lògic3 200 GiB |
    | /dev/vg1/rootvol    | /dev/vg1/homevol    | /dev/vg1/mediavol    |
    +---------------------+---------------------+----------------------+
....

==== Avantatges

LVM dóna més flexibilitat que la simple partició d'un disc dur per a:

* Poder utilitzar qualsevol nombre de discs com un gran disc.
* Tenir volums lògics establerts sobre diversos discs.
* Crear petits volums lògics i canviar la seva grandària «dinàmicament», quan s'omplin.
* Canviar la grandària dels volums lògics sense importar l'ordre en el disc. No depèn de la posició del volum lògic dins del grup de volums, ni hi ha necessitat d'assegurar l'espai disponible circumdant.
* Redimensionar/crear/esborrar la grandària dels volums lògics i físics en línia. Els sistemes d'arxius en ells encara hauran de ser redimensionats, però alguns (com ext4) donen suport a el canvi de grandària en línia.
* Migració en calent de volums lògics, sent utilitzat pels serveis per a diferents discos sense haver de reiniciar els serveis.
* Permet crear una instantània d'un volum lògic i treballar amb un "diferencial", és a dir, emmagatzemar només els canvis respecte la instantània inicial.
+
Posteriorment es pot decidir si tornar el volum a l'estat inicial de la instantània o bé integrar la instantània al volum.
* Permet la creació de diferents tipus de raids de discs, a la pràctica, raids 1 o mirrors de vloums lògics.
+
.Estructura d'un raid 1
image::raid1.png[]
 
////
* Suport per a albergar diferents "mapejadors" de dispositius, inclòs el xifrat del sistema d'arxius transparent i emmagatzematge en cache de les dades d'ús freqüent. Això permet crear un sistema amb (un o més) discs físics (xifrats amb LUKS) i LVM en la part superior per a permetre el canvi de grandària i l'administració de volums separats (per exemple, per a /, /home, /còpia de seguretat, etc.) sense la molèstia d'introduir una clau diverses vegades en arrencar.
////
==== Desavantatges

* Els passos addicionals en la configuració del sistema, que el fa més complicat.
* Si té una arrencada dual, cal tenir en compte que Windows no és compatible amb LVM; no podrà accedir a cap partició LVM des de Windows.
