== Docker

*Docker* és un projecte de *codi obert* que ha revolucionat la manera de desenvolupar programari gràcies a la senzillesa amb la qual permet gestionar contenidors. Els contenidors *LXC* (Linux Containers) són un concepte relativament antic i utilitzat des de fa temps per grans empreses com Amazon o Google, però la gestió de les quals era complicada.

No obstant això, Docker defineix APIs i eines de línia de comandes que fan gairebé trivial la creació, distribució i execució de contenidors.

Per aquest motiu el lema de Docker és *_“Build, Ship and Run. Any application, Anywhere”_* i s'ha convertit en una eina fonamental tant per a desenvolupadors com per a administradors de sistemes.

.Contenidor
****
Podríem definir un contenidor com un *procés aïllat* de la resta dels processos de la màquina gràcies a que s'executa sobre el seu *propi sistema de fitxers*, el seu propi espai d'usuaris i processos, les seves pròpies interfícies de xarxa…
****

Les característiques principals dels contenidors són la seva portabilitat, la seva inmutabilitat i la seva lleugeresa:

Portabilitat::
Un contenidor és executat pel que es denomina el *_Docker Engine_*, un dimoni que és fàcilment instal·lable en totes les distribucions Linux i també en Windows. 
+
Un *_contenidor_* executa una *_imatge_* de docker. Una *_imatge_* de docker és un paquet de software *_independent_*, *_executable_* que inclou tot allò que necessita per executar una aplicació, *_codi_*, *_runtime_*, *_eines de sistema_*, *_llibreries de sistema_*, *_configuracions_* i *_un sistema de fitxers_*.
+
Una vegada que hem generat una imatge de Docker, ja sigui en el nostre ordinador o via una eina externa, aquesta imatge podrà ser executada per qualsevol *_Docker Engine_*, independentment del sistema operatiu i la infraestructura que hi hagi per sota.

Immutabilitat::
Una aplicació la componen tant el codi font com les llibreries del sistema operatiu i del llenguatge de programació necessàries per a l'execució d'aquest codi.
+
Aquestes dependències depenen al seu torn del sistema operatiu on el nostre codi serà executat, i per això mateix ocorre moltes vegades allò que “no sé, en la meva màquina funciona”.
+
No obstant això, *el procés d'instal·lació de dependències en Docker no depèn del sistema operatiu*, si no que aquest procés es realitza quan es genera una imatge de docker. És a dir, *una imatge de docker conté tant el codi de l'aplicació com les dependències que necessita per a la seva execució*. 
+
Una imatge es genera una vegada i pot ser executada les vegades que siguin necessàries, i sempre executarà amb les mateixes versions del codi font i les seves dependències, per la qual cosa es diu que és *immutable*. 
+
Si unim immutabilitat amb el fet que Docker és *portable*, una vegada generada una imatge, aquesta es comporta de la mateixa manera independentment del sistema operatiu i de la infraestructura on s'estigui executant.

Lleugeresa::
Els contenidors corrent en la mateixa màquina *comparteixen entre ells el sistema operatiu*, però cada contenidor és un procés independent amb el seu propi sistema de fitxers i el seu propi espai de processos i usuaris. 
+
Això fa que l'execució de contenidors sigui molt més lleugera que altres mecanismes de virtualització. 
+
[IMPORTANT]
====
Com que els _contenidors_ de docker comparteixen el sistema operatiu del host, *el sistema operatiu del contenidor ha de coincidir amb el sistema operatiu del host*.
====

=== Arquitectura

Docker està format fonamentalment per tres components:

* Docker Engine
* Docker Client
* Docker Registry

.Arquitectura de docker
[uml, file="images/uml/docker1.png"]
....
cloud Registry #bisque
rectangle Client #lightblue
rectangle {
    rectangle Daemon #salmon
    folder Images { 
      component "debian:stretch" #goldenrod
      component "ubuntu:16.04" #goldenrod
    }
    folder Containers { 
      node "dreamy_babbage"  #aquamarine
      node "quizzical_fermat"  #aquamarine
    }
}

Client <-> Daemon
Registry <--> Daemon
Daemon <-> Images
Daemon <--> Containers
....

Docker Engine o Dimoni Docker::
És un servei que corre sobre qualsevol distribució de Linux (i ara també de Windows i de MacOS) i que exposa una API externa per a la gestió d'imatges i contenidors (i altres entitats que es van afegint en successives distribucions de docker com a *_volums_* o *_xarxes virtuals_*). Podem destacar entre les seves funcions principals:

* Creació d'imatges docker.
* Publicació d'imatges en un *_Docker Registry_* o Registre de Docker.
* Descàrrega d'imatges des d'un Registre de Docker
* Execució de contenidors usant imatges locals.

Una altra funció fonamental del Docker Engine és la gestió dels contenidors en execució, permetent parar la seva execució, rearrancarla, veure les seves logs o les seves estadístiques d'ús de recursos.

Docker Registry o Registre Docker::
El Registre és un altre component de Docker que sol córrer en un servidor independent i on es publiquen les *_imatges_* que generen els Docker Engine de tal manera que estiguin disponibles per a la seva utilització per qualsevol altra màquina. És un component fonamental dins de l'arquitectura de Docker ja que permet distribuir les nostres aplicacions. El Registre de Docker és un projecte open source que pot ser instal·lat gratuïtament en qualsevol servidor, però Docker ofereix Docker Hub, un sistema SaaS de pagament on pots pujar les teves pròpies imatges, accedir a imatges públiques d'altres usuaris, i fins i tot a imatges oficials de les principals aplicacions com són: MySQL, MongoDB, RabbitMQ, Redis, etc.

El registre de Docker funciona d'una manera molt semblant a *_git_*. Cada imatge és una successió de capes. És a dir, cada vegada que fem un build en local de la nostra imatge, el Registre de Docker només emmagatzema el *_diff_* respecte de la versió anterior, fent molt més eficient el procés de creació i distribució d'imatges.

Docker Client o Client Docker::
És qualsevol eina que fa ús de la api remota del Docker Engine, però sol fer referència a la comanda *_docker_*, eina de línia de comandes per a gestionar un Docker Engine. La cli de docker es pot configurar per a parlar amb un Docker Engine local o remot, permetent gestionar tant el nostre entorn de desenvolupament local, com els nostres servidors de producció.

=== Workflows

Docker transforma radicalment el concepte d'entorn local de desenvolupament. La següent figura mostra un esquema d'aquest entorn local de desenvolupament:

.Workflow de docker
[uml, file="images/uml/docker2.png"]
....
rectangle Docker {
  component nginx #goldenrod
  component app1  #goldenrod
  component mariadb  #goldenrod
}

database Registry #bisque

Docker --> Registry : push
Docker <-- Registry : pull
....

Els desenvolupadors poden córrer contenidors amb les dependències externes de l'aplicació que estan desenvolupant, tals com *_nginx_* o *_mysql_*. També corren l'aplicació que estan desenvolupant en el seu propi contenidor.

Una vegada que han acabat el desenvolupament d'una nova funcionalitat i els tests passen en local es pot fer *push* de la nova imatge que han desenvolupat al Registre. 

La imatge "pusheada" pot ser descarregada en els servidors de producció per a desplegar una nova versió de la nostra aplicació amb la garantia que es comportarà de la mateixa manera que en l'entorn local del desenvolupador gràcies a les propietats de portabilitat i immutabilitat dels contenidors.

*Aquest cicle de desenvolupament és el gran avantatge que aporta Docker als cicles de desenvolupament de programari*, i la raó per la qual Docker s'ha fet tan popular.

=== Comandes comunes per a la gestió de contenidors

Una vegada que tenim docker corrent en la nostra màquina, podem començar a executar algunes comandes:

docker version:: Dóna informació sobre la versió de docker que estem corrent.

docker info:: Dóna informació sobre la quantitat de contenidors i imatges que està gestionant la màquina actual, així com els plugins actualment instal·lats.

docker container run:: Crea un contenidor a partir d'una imatge. Aquesta comanda permet multitud de paràmetres, que són actualitzats per a cada versió del Docker Engine, per la qual cosa per a la seva documentació el millor és fer referència a la pàgina oficial.
+
[source, bash]
----
vagrant@dockernode:~$ docker container run -d -p 8080:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
27833a3ba0a5: Pull complete
eb51733b5bc0: Pull complete
994d4a01fbe9: Pull complete
Digest: sha256:50174b19828157e94f8273e3991026dc7854ec7dd2bbb33e7d3bd91f0a4b333d
Status: Downloaded newer image for nginx:latest
9e5f72ed968871ea398dfe5360c25ca4b343a7aadc4c37617980bdb082c489d8
----
+
[NOTE]
====
El paràmetre *_-d_* fa córrer el contenidor en segon pla
====

docker container ps / docker container list:: Mostra els contenidors que estan corrent en la màquina. Amb el flag *_-a_* mostra també els contenidors que estan parats.
+
[source, bash]
----
josep@dockerd:~$ docker container ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
c02fc59f003e        nginx               "nginx -g 'daemon of…"   45 seconds ago      Up 42 seconds       0.0.0.0:8080->80/tcp   silly_bassi
----
+
Els noms són aleatoris però podriem indicar-li el nom:
+
[source, bash]
----
josep@dockerd:~$ docker container run -d -p 8081:80 --name test1 nginx
5c7146270b8d5a5c8755b09d53be091f394ad42c28d981fa1fec9b2ab1ce0776
----

docker container inspect:: Mostra informació detallada d'un contenidor en format json. Es pot accedir a un camp particular amb la comanda *_docker inspect -f '{{.Name}}' nom_contenidor_*.
+
[source, bash]
----
josep@dockerd:~$ docker container inspect test1
[
    {
        "Id": "5c7146270b8d5a5c8755b09d53be091f394ad42c28d981fa1fec9b2ab1ce0776",
        "Created": "2019-04-16T07:09:24.841393621Z",
        "Path": "nginx",
. . .
----
+
[source, bash]
----
josep@dockerd:~$ docker container inspect -f '{{.Name}}' b5afa
/trusting_shtern
----

docker container stop:: Per a l'execució d'un contenidor.
+
Podriem parar tots els contenidors amb:
+
[source, bash]
----
josep@dockerd:~$ docker container list -q
5c7146270b8d
c02fc59f003e
----
+
[source, bash]
----
josep@dockerd:~$ docker container stop $(docker container list -q)
5c7146270b8d
c02fc59f003e
----

docker container start:: reprèn l'execució d'un contenidor.
docker container rm:: elimina un contenidor. 
+
Per a esborrar tots els contenidors d'una màquina es pot executar la comanda següent:
+
[source, bash]
----
josep@dockerd:~$ docker container rm -fv $(docker container list -aq)
----

docker container exec:: executa una comanda en un contenidor. 
+
[source, bash]
----
josep@dockerd:~$ docker container exec test1 ls
----
+
Útil per a depurar contenidors en execució amb les opcions *_docker exec -it contenidor bash_*.
+
[source, bash]
----
josep@dockerd:~$ docker container exec -it test1 /bin/bash
root@c3e65cbca943:/# 
----

docker container cp:: copia arxius entre el host i un contenidor.
+
[source, bash]
----
josep@dockerd:~$ docker container cp trusting_shtern:/etc/hostname .
josep@dockerd:~$ ls
8080     Documents  hostname  Pictures  Templates
Desktop  Downloads  Music     Public    Videos
----

docker logs:: mostra els logs d'un contenidor.
+
[source, bash]
----
josep@dockerd:~$ docker container run -d mysql

josep@dockerd:~$ docker container logs 63e7
error: database is uninitialized and password option is not specified 
  You need to specify one of MYSQL_ROOT_PASSWORD, MYSQL_ALLOW_EMPTY_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD
----

docker container stats:: mostra les estadístiques d'execució d'un contenidor en temps real.

docker system prune:: utilitat per a eliminar recursos que no estan sent usats en aquest moment.
+
[source,  bash]
----
josep@dockerd:~$ docker system prune -a
WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all images without at least one container associated to them
        - all build cache
Are you sure you want to continue? [y/N] y
----

////
També podem configurar el client de docker per a parlar amb Docker Engines remots usant les variables d'entorn:

Al sistema remot:

----
docker container pull kapacitor
----

[source, bash]
----
export DOCKER_HOST="tcp://ucp.dckr.io:443"
export DOCKER_TLS_VERIFY=1
export DOCKER_CERT_PATH="~/ucp/stage"
----

para conectar a un Docker Engine escuchando en la url tcp://ucp.dckr.io:443 y usando TLS y los certificados del directorio ~/ucp/stage. Si la conexión fuera abierta, indicaríamos export DOCKER_TLS_VERIFY=0.
////

[NOTE]
====
Totes les comandes admeten *_--help_* per mostrar l'ajuda.
====

////
=== Microserveis

El concepto de microservicios tiene sus orígenes en la arquitectura SOA (Service Oriented Architecture). SOA se basa en el antiguo principio de “divide y vencerás”, y sostiene un modelo distribuido para el desarrollo de aplicaciones frente a soluciones clásicas más monolíticas. 


Una arquitectura basada en SOA debe seguir una serie de principios para ser exitosa. Estos principios son:

    Cada servicio debe ofrecer un contrato para conectarse con él. Un caso muy común es un servicio que ofrece una API REST. Dicha API debe siempre mantener compatibilidad con versiones anteriores, o gestionar versiones de sus endpoints cuando se producen incompatibilidades, pero es fundamental no romper el contrato con otros servicios.

    Cada servicio debe minimizar las dependencias con el resto. Para esto es fundamental acertar con el scope de un servicio. Una indicación de que el scope no es el adecuado es cuando se producen dependencias circulares entre los servicios.

    Cada servicio debe abstraer su implementación. Para el resto de servicios debe ser transparente si un servicio usa un backend u otro para la base de datos o si ha hecho una nueva release.

    Los servicios deben diseñarse para maximizar su reutilización dado que la reutilización de componentes es una de las ventajas de una arquitectura SOA.

    Cada servicio tiene que tener un ciclo de vida independiente, desde su diseño hasta su implantación en los entornos de ejecución.

    La localización física de donde corre un servicio debe ser transparente para los servicios que lo utilizan.

    En lo posible, los servicios deben evitar mantener estado.

    Es importante mantener la calidad de los servicios. Un servicio con continuas regresiones puede afectar a la calidad final percibida por el resto de servicios que hacen uso de él. 

	Teniendo en cuenta estos principios, el concepto de microservicios es un poco la manera que se ha puesto de moda para referirse a las arquitecturas SOA, pero incidiendo más aún en que la funcionalidad de dichos servicios debe ser la mínima posible. Una medida bastante extendida es que un microservicio es un componente que debería ser desarrollable en unas dos semanas. Las ventajas de una arquitectura basada en microservicios son las siguientes:

    Son componentes pequeños que agilizan los procesos de desarrollo de software y son fáciles de abordar por un equipo de desarrolladores.

    Son servicios independientes, si un microservicio falla no debería afectar a los demás.

    El despliegue de un microservicio a producción es más sencillo que el de una aplicación monolítica.

    Los microservicios son altamente reutilizables.

    Los microservicios son más fáciles de externalizar. 

El despliegue de un microservicio suele ser es más sencillo que el de una aplicación monolítica debido a su mayor sencillez y sus menores dependencias. Por otra parte, los microservicios agilizan los procesos de desarrollo del software permitiendo casos de uso donde al día podemos hacer varios despliegues de distintos microservicios. Es por tanto casi imposible concebir una arquitectura basada en microservicios sin la automatización de los procesos de integración y despliegue continuo. Ésta es la principal relación entre Docker y los microservicios ya que Docker es una herramienta excepcional para la automatización de estas tareas. Docker simplifica la automatización de construir una imagen, distribuirla y ejecutarla en cualquier máquina independientemente de la infraestructura. Esto significa que podemos construir una imagen en nuestros entornos de integración continua, correr nuestras pruebas contra ella, distribuirla en nuestro servidores de producción y por último, ejecutarla en un contenedor. Y todo esto ejecutando simplemente unos cuantos comandos de docker.

Dicho esto, una arquitectura de microservicios es sólo un modelo de desarrollo de software que mal aplicado puede traer enormes quebraderos de cabeza. Los microservicios adquieren más importancia cuando tenemos equipos de ingeniería muy grandes, que interesa dividir en subgrupos y cada uno de ellos se encargue de uno (o unos pocos) microservicios. Además, el proceso de migrar una arquitectura monolítica a una arquitectura basada en microservicios debe ser planeado con cautela. Se recomienda transferir un trozo de lógica a un sólo microservicio a la vez, ya que una arquitectura basada en microservicios puede implicar un cambio de las herramientas utilizadas para el despliegue, monitoreo y sistemas de logging de nuestras aplicaciones.

Nos gustaría destacar que Docker se adapta perfectamente a una arquitectura basada en microservicios, pero sería posible tener una arquitectura basada en microservicios sin usar contenedor, y por supuesto, es perfectamente posible usar Docker en una arquitectura más monolítica o no basada en microservicios. Imaginemos el caso de una aplicación legacy monolítica. Solo por el hecho de meter esta aplicación dentro de un contener, tal y como es, sis cambios en su código fuente, nos vamos a favorecer de muchas de las características de docker, como son:

    Facilidad de levantar entonos locales de desarrollo.
    Portabilidad para correr nuestro contenedor en un Mac, en un Ubuntu, en integración continua o en un servidor de producción.
    Facilidad para distribuir las imágenes de nuestra aplicación.

Dicho de otra manera. No es necesario modificar nuestras aplicaciones de toda la vida para adaptarlas a Docker, Docker se adapta a nuestras aplicaciones tal y como son. Hay un conjunto de buenas prácticas para seguir una arquitectura basada en microservicios, pero solo son eso, buenas prácticas que dependiendo del contexto conviene o no aplicar, si no sería axiomas de desarrollo. Mostramos esta idea en la siguiente figura:

En la parte de la izquierda tenemos una aplicación monolítica corriendo en un host con cuatro procesos principales: systemd, nginx, una proceso python y un proceso node.js. El proceso de dockerizar sería construir una imagen de docker con la misma arquitectura de procesos, pero esto nos permite hacer push y pull de esta imagen de una manera muy sencilla, y corren en cualquier entorno de ejecución con total garantía. Más tarde, y si así se decide, podemos separar cada proceso en su propio microservicio, que sería la parte de la derecha, donde cada proceso corre en un contenedor independiente. Lo que queremos destacar es que la fase de dockerizar, aunque luego no se adopte una arquitectura de microservicios, es de enorme valor en sí mismo. También que saltarse este paso y tratar de pasar de la arquitectura de la derecha a la de la izquierda en un solo paso suele ser una garantía para buscar problemas.
////

=== Construcció d'imatges

Com vam veure a la introducció a Docker, una *imatge* es correspon amb la informació necessària per a arrencar un *contenidor*, i bàsicament es compon d'un sistema d'arxius i d'altres metadades com són la *comanda a executar*, *les variables d'entorn*, els *volums del contenidor*, els *ports* que utilitza el nostre contenidor…

La manera recomanada de construir una imatge és utilitzar un fitxer *_Dockerfile_*, un fitxer amb un conjunt d'instruccions que indiquen com construir una imatge de Docker. Les instruccions principals que poden utilitzar-se en un Dockerfile són:

FROM image:: Defineix la imatge base del nostre contenidor.
RUN comanda:: Executa una comanda en el context de la imatge.
ENTRYPOINT comanda:: Defineix el "entrypoint" que executa el contenidor en arrencar.
CMD comanda:: Declara la comanda que executa el contenidor al entrar o els arguments que es passaran al entrypoint.
+
[WARNING]
====
Aquesta comanda només s'executa si no s'especifica una comanda al executar *_docker container run_*, en aquest cas s'executara la comanda especificada.
====
WORKDIR path:: Defineix el directori de treball en el contenidor.
ENV var=value:: Defineix variables d'entorn.
EXPOSE port:: Declara per a quins ports accepta connexions el contenidor.
VOLUME path:: Defineix un volum en el contenidor.
+
[NOTE]
====
Un volum permet tenir informació persistent al contenidor. A més és independent del sistema de capes de docker, és a dir, no entra dins de l'estructura transaccional de a imatge cosa que fa que sigui molt més eficient.
====
COPY origen destí:: Copia fitxers des del host a la imatge.+
També s'usa per a *_multi-stage builds_*.

Un exemple d'un dockerfile per a una aplicació Flask en python podria ser:

.Dockerfile
[source, docker]
----
FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev
WORKDIR /app
ENV DEBUG=True
EXPOSE 80
VOLUME /data
COPY . /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
----

A mesura que es vagin executant línies del dockerfile generara un "*_diff_*" sobre l'estat del sistema de fitxers abans i després de l'execució de la línia del dockerfile. Aquest 
"diff" s'anomena *capa*. 

Aleshores es treballa amb un sistema transaccional on es realitza un commit per a cadascuna de les línies executades del dockerfile.

=== La memòria cau de docker

La construcció d'una imatge de Docker donat un Dockerfile pot ser un procés costós ja que pot implicar la instal·lació d'un número elevat de biblioteques, i al mateix temps és un procés bastant repetitiu perquè successius builds del mateix Dockerfile solen ser similars entre si. És per això que Docker introdueix el concepte de la *cache* per a optimitzar el procés de construcció d'imatges.

La primera optimització que fa la cache de Docker és la descàrrega de la imatge base del nostre Dockerfile. Docker descarregarà la imatge base sempre que la mateixa no es trobi ja descarregada en la màquina que fa el build. 

Aquesta optimització sembla òbvia ja que aquestes imatges poden tenir una grandària de centenars de MB, però cal anar amb compte ja que si la versió remota de la imatge canvia, Docker continuarà utilitzant la versió local. Per tant, si volem executar nostre Dockerfile amb la nova versió de la imatge base haurem de fer un docker pull manual de la imatge base, o executar *_docker build --pull_*.

Una imatge de Docker té una estructura interna bastant semblant a un repositori de git. El que coneixem com commits en git ho denominem capes d'una imatge en Docker. Per tant, *una imatge* és una successió de capes en un *Registre de Docker*, on cada capa emmagatzema un diff respecte de la capa anterior. Això és important de cara a optimitzar nostres Dockerfiles.

En principi, *cada instrucció del Dockerfile crea una i només una capa de la imatge*. Per tant, la cache de Docker funciona a nivell d'instrucció.

En altres paraules, si una línia del Dockerfile no canvia, en lloc de recomputarla, Docker assumeix que la capa que genera aquesta instrucció és la mateixa que l'execució anterior del Dockerfile. Per tant, si tenim una instrucció tal com:

[source, docker]
----
RUN apt-get update && apt-get install -i git
----

que no ha canviat entre dos build successius, les comandes _apt-get_ no s'executaran, sinó que es reusará la capa que va generar el primer build.

[TIP]
====
Podem desactivar l'ús de la cache executant docker build --no-cache.
====

És important destacar els següents aspectes sobre la cache de Docker:

* *La cache de Docker és local*, és a dir, si és la primera vegada que es fa el build d'un Dockerfile en una màquina donada, totes les instruccions del Dockerfile seran executades, encara que la imatge ja hagi estat construïda en un Registre de Docker.
* Si una instrucció ha canviat i no pot utilitzar la cache, la cache queda invalidada i les següents instruccions del Dockerfile seran executades sense fer ús de la cache.
* El comportament de les instruccions ADD i COPY és diferent quant al comportament de la cache. Encara que aquestes instruccions no canviïn, invaliden la cache si el contingut dels fitxers que s'estan copiant ha estat modificat.

=== Comandes comunes per a la gestió d'imatges

Aquests són les comandes més comunes per la gestió d'imatges:

docker image build:: ens permet crear una imatge a partir d'un Dockerfile.
docker login:: autentica la cli de docker contra un registre, per defecte Docker Hub.
docker pull:: descàrrega una imatge a la qual tinguem accés des d'un registre.
docker image ls:: llista les imatges que estan disponibles en la nostra màquina.
docker image inspect:: mostra informació detallada d'una imatge. Es pot accedir a un camp particular amb la comanda *_docker inspect -f '{{.Size}}'_* imatge.
docker image rm:: elimina una imatge.

=== Bones pràctiques

==== Utilitzar *_.dockerignore_*

El build d'una image s'executa a partir d'un Dockerfile i d'un directori, que es coneix amb el nom de *_context_*. Aquest directori sol ser el mateix que el directori on es troba el Dockerfile, per la qual cosa si executem la instrucció:

[source, docker]
----
ADD app.py /app/app.py
----

Estem afegint a la imatge el fitxer *_app.py_* del context, és a dir, el fitxer *_app.py_* que es troba en el directori on està el Dockerfile. Aquest directori es comprimeix i s'envia al Docker Engine per a construir la imatge, *però pot ser que tingui fitxers que no són necessaris*. És per això que aquest directori pot tenir un fitxer *_.dockerignore_*, que d'una manera similar a fitxer *_.gitignore_*, indica els fitxers que no han de ser considerats com a part del context del build.

==== Reduir la grandària de les imatges al mínim

Una imatge de Docker només ha de contenir el estrictament necessari per a executar l'aplicació. 

Amb l'objectiu de reduir complexitat, dependències, grandària de les imatges, temps de build d'una imatge, cal evitar la instal·lació de paquets només pel fet que puguin ser útils per a depurar un contenidor. 

Un altre opció molt pràctica és l'ús d'imatges base petites, per exemple, fent ús de *_alpine_*. 

[TIP]
====
*_alpine_* és una distribució Linux específica per contenidors.
====

==== Executar un únic procés per contenidor

En general, és recomanable executar un sol procés per contenidor. Això permet *reutilitzar contenidors* més fàcilment, que siguin més *fàcils d'escalar*, i dóna lloc a *sistemes més desacoblats*.

==== Minimitzar el nombre de capes de la imatge.

Recordem que cada capa d'una imatge es correspon amb una instrucció del Dockerfile. 

Comparem el següent fitxer *_Dockerfile_*:

[source, docker]
----
RUN apt-get update
RUN apt-get install -i bzr
RUN apt-get install -i cvs
RUN apt-get install -i git
RUN apt-get install -i mercurial
----

amb aquest altre:

[source, docker]
----
RUN apt-get update && apt-get install -i \
    bzr \ 
    cvs \ 
    git \ 
    mercurial \ 
    apt-get clean
----

Tots dos són igualment llegibles, però el primer genera 5 capes, i el segon només una, que a més executa un *_apt-get clean_* que redueix la grandària d'aquesta capa. 

==== Optimitzar l'ús de la cache.

Optimitzar l'ús de la cache afegint al principi del *_Dockerfile_* les instruccions que menys canvien (com la instal·lació de biblioteques), i deixant per al final les que més canvien (com el copiat del codi font).

==== Parametritzar fitxers *_Dockerfile_* usant arguments.

Els arguments són valors que es passen com a paràmetres a cada build (encara que poden tenir valors per defecte). Per exemple, el Dockerfile:

[source, docker]
----
FROM ubuntu
ARG user=root
ARG password
RUN echo $user $password
----

pot ser parametritzat de la següent manera:

[source, bash]
----
docker build -t imatge --build-arg password=secret .
----

[NOTE]
====
Els arguments passats queden emmagatzemats a les capes, no és una bona idea passar secreta per paràmetres!!
====

==== Utilitzar builds *_multi-stage_*

Els "builds" multi-stage permeten resetejar el sistema de fitxers de la imatge que s'està construint, canviar a un altre sistema de fitxers, però importar fitxers de la imatge anterior.

Per exemple:

.Dockerfile
[source, docker]
----
FROM gcc  as builder
RUN apt-get update -y
WORKDIR /work
COPY . /work
RUN gcc -static -o hello hello.c

FROM scratch
COPY --from=builder /work/hello /hello
CMD ["/hello"]
----

=== Desenvolupament d'aplicacions multi-contenidor

Docker compose és un altre projecte open source que permet definir aplicacions muilti-contenidor d'una manera senzilla i declarativa. És una eina ideal per a gestionar entorns de desenvolupament i per a configurar processos d'integració contínua.

*docker-compose* és una alternativa més còmoda a l'ús de les comandes *_docker image run_* i *_docker image build_*. Amb Docker Compose es defineix un fitxer *_docker-compose.yml_* que té aquesta forma (pres de docker-for-devs/acte-build/docker-compose.yml):

.docker-compose.yml
[source, yaml]
----
web:
  build: .
  ports:
   - "5000:5000"
  depends_on:
   - redis
redis:
  image: redis
----

on estem definint una aplicació que es compon d'un contenidor definit des d'un Dockerfile local, que escolta en el port 5000, i que fa ús de redis com un servei extern. Donada aquesta definició, la manera d'aixecar l'aplicació és simplement:

[source, bash]
----
docker-compose up -d
----

Per defecte totes les imatges dins d'un *_docker-compose_* estan per defecte en una mateixa xarxa i són visibles entre ells. Les ip de tenen responen al nom assigant a cadascún dels serveis.

En el següent exemple, estem representant 2 contenidors, que en el llenguatge de docker compose s'anomenen *serveis*. Cada línia de docker-compose equival a un argument de *_docker run_*

.docker-compose.yml
[source, yaml]
----
db:
	image: mysql:5.7
	restart:always
	environment:
		MYSQL_ROOT_PASSWORD: password
wordpress:
	image: wordpress:latest
	depends_on:
		- db
	ports:
		-"8000:80"
----


=== Comandes de docker-compose

docker-compose --help:: Mostra l'ajuda de la comanda *_docker-compose_*.

docker-compose up -d:: Aixeca l'aplicació en mode dimoni, *_docker-compose up_* l'aixeca en primer pla, mostrant els *logs* dels diferents contenidors.
+
L'execució successiva de la comanda *_docker-compose up -d_* només recrea els contenidors que hagin canviat la seva imatge o la seva definició. *_docker-compose up -d_* no fa el _build_ cada vegada que és invocat de les imatges locals. Per actualitzar l'aplicació sobre la base dels últims canvis del codi, caldrà executar *_docker-compose up --build -d_*. Un truc per a millorar aquest procés és muntar el codi com un volum en el fitxer *_docker-compose.yml_*, de tal manera que el contenidor sempre veu els últims canvis en el codi font. Per aixecar només un o diversos dels serveis en un compose, es pot afegir el seu nom, per exemple *_docker-compose up -d redis_*.

docker-compose pull:: Actualitza les imatges definides en el _docker-compose_ amb la versió actual que hi hagi en el registre. 
+
Amb l'opció --parallel fa el _pull_ en paral·lel.

docker-compose build:: Reconstrueix les imatges dels serveis que tinguin una secció de _build_ definida. 
+
Opcions interessants són: *_--no-cache_* per a invalidar la cahe, *_--pull_* per a fer "pull" de les imatges base i *_--build-arg key=val_* per a passar arguments. 

docker-compose push:: Puja al registre la versió local de les imatges amb una secció de "build" definida. 

docker-compose run:: Executa un contenidor d'un dels serveis definit en el _docker-compose_. 
+
La diferència principal amb *_docker-compose up_* és que permet definir la comanda a executar, així com d'altra informació de context com a variables d'entorn, el _entrypoint_, _volums_, el _directori de treball_…


docker-compose rm:: Elimina els contenidors i altres recursos com a xarxes, creats a partir d'un _docker-compose_.

*_docker-compose_* permet definir pràcticament tots els arguments que suporten tant la comanda *_docker run_* com  la comanda *_docker build_*, però *_docker-compose_* és molt més fàcil d'utilitzar. Les opcions més comunes són:

build:: Per a indicar que el contenidor es construeix des d'un *_Dockerfile_* local. Pot tenir subcamps com *_context_*, *_dockerfile_*, *_cache_from_* o *_args_*.

image:: Per a indicar que el contenidor corre un imatge remota. També indica el nom de la imatge que es crea si hi ha un camp *_build_*.

command:: Per a redefinir la comanda que executa el contenidor en lloc de la comanda definida a la imatge.

environment:: Per a definir variables d'entorn en el contenidor. Es poden passar fent referència a un fitxer usant la propietat *_env_file_*. Si la variable no té un valor donat, el seu valor s'agafarà de l'entorn de *_shell_* que executa el *_docker-compose up_*, la qual cosa pot ser útil per a passar claus, per exemple.

depends_on:: Per a definir relacions entre contenidors.

ports:: Per a mapejar els ports on el contenidor accepta connexions.

=== Volums

((TODO))

////
Quan un contenidor és eliminat, la informació continguda en ell desapareix. Per a evitar aquest problema i que les dades generades a l'interior d'un contenidor no s'eliminin quan el contenidor acaba podem fer ús de volums de dades (data volume). Un volum és un directori dins del contenidor que s'associat amb un directori del host, per la qual cosa persisteix a la finalització del contenidor. Un contenidor pot tenir diversos volums, i un mateix volum pot muntar-se en diversos contenidors per a compartir informació.

Volums de Dades

Els volums de dades tenen les següents característiques:

    Quan esborrem el contenidor, no s'elimina el volum associat.
    Ens permeten guardar i intercanviar informació entre contenidors.
    No són gestionats pels storage drivers, per la qual cosa les operacions d'entrada / sortida són molt més eficients.

Els volums de dades tens la seva pròpia interfície amb la línia de comandos de docker:

    docker volume create: crea un nou volum de dades.
    docker volume ls: mostra els volums de dades de la nostra màquina.
    docker volume inspect: retorna informació relativa a un volum.
    docker volume rm: elimina un volum de dades. També es poden eliminar automàticament en eliminar un contenidor si executem docker rm -f.

Si fem un docker inspect d'un contenidor amb volums associats aquesta informació apareix en el camp Mounts.
Finalment podem comprovar que encara que esborrem el contenidor, el volum no s'esborra.

Volums del Host

Una aplicació particular dels volums és la possibilitat de muntar en el contenidor un directori ja existent en el host. En aquest cas cal tenir en compte que si el directori de muntatge del contenidor ja existeix, no s'esborra el seu contingut, simplement es munta damunt. Vegem un exemple:

$ docker run -it -v `pwd`:/data alpine sh
# cd data
# ls

Gestió de Volums amb docker-compose

El següent exemple il·lustra la gestió de volums amb docker-compose:

[source, yaml]
----
version: '3.4'
services:
  mysql:
    image: mysql
    volumes:
      - mysql:/var/lib/mysql
      - logs:/var/log/mysql
      - /etc:/etc
  analizer:
    image: log-analizer
    volumes:
      - logs:/var/log:ro
volumes:
  data:
  logs:
----
////

=== Xarxes

((TODO))

////
Docker ens permet crear diferents xarxes virtuals per a les nostres necessitats, ja bé per a unir o segmentar diferents contenidors. D'aquesta manera, podem separar contenidors per seguretat en xarxes diferents, o unir-los en la mateixa per conveniència o per connectar els seus serveis entre si.

Per defecte, Docker ens ofereix tres tipus de xarxes diferents. La primera, bridge, és on arrencarien tots els nostres contenidors per defecte. És una xarxa que crea un pont entre la interfície de xarxa del contenidor que arrenquem i una interfície de xarxa virtual que es crea en el nostre equip quan instal·lem Docker. La següent seria host. Host el que fa és copiar la configuració de xarxa del host, és a dir, del servidor o màquina on està Docker en el contenidor que estem arrencant. Si arrenquem un contenidor aquí i executem la revisió de la configuració de xarxa, veurem que és la mateixa que la de la màquina en la qual l'estem corrent. I després tenim la xarxa none, que utilitza el driver null, que el que fa és eliminar tota la configuració de xarxa del nostre contenidor. Si creem un contenidor aquí, només tindrem loopback, només tindrem la direcció 127.0.0.1, i no podrem connectar cap lloc més.

Quan instal·lem Docker, veurem que ens crea una interfície anomenada docker0. Té una adreça IP privada, probablement aquesta, si no dóna col·lisió amb cap altra adreça IP que tingueu configurada. I quan cregueu algun tipus de contenidor que es connecta a la xarxa bridge, el que fa és rebre per DHCP una adreça IP d'aquest rang. Podeu connectar a través d'ell. Tots els vostres contenidors faran NAT a través d'aquesta IP, i a través de la IP de sortida de la màquina host o servidor en la qual teniu Docker instal·lat. Aquesta és la vostra xarxa per defecte. De la mateixa manera, podeu connectar des d'aquí a través d'aquesta interfície i per aquesta adreça IP a les adreces IP dels contenidors que teniu corrent en aquesta xarxa.

Aquesta xarxa bridge no és l'única que podeu tenir, podeu crear més per a separar els vostres contenidors en diferents xarxes. Per a això, haureu d'executar el següent comando: “docker network create”. Heu de triar el driver del tipus de xarxa que voleu crear. El més probable és que sigui una xarxa bridge. I el nom de la xarxa. Veureu dues coses, una que tenim una xarxa nova amb el nom xarxa1, driver bridge i un identificador. Una altra, que si fem un ifconfig, veiem també que tenim una interfície virtual de xarxa nova en la qual tenim les sigles “br” de bridge i l'ANEU de la nostra xarxa. Si veiem aquesta interfície en concret, veiem que tenim una nova adreça IP. Dins d'aquest rang de xarxa, apareixeran tots els contenidors que nosaltres executem en xarxa1. I tant xarxa1 com bridge seran xarxes separades. Els contenidors que tinguem en bridge, la xarxa bridge per defecte, i els contenidors que tinguem en xarxa1, l'altra xarxa bridge que hem creat no es podran comunicar entre si.

A part, tot el que arrenqueu amb Docker Compose, tindrà una xarxa privada per a ell. Docker Compose crearà una xarxa cada vegada que aixequeu una infraestructura completa. Com veieu, és bastant senzill manejar les xarxes de Docker. Podeu segmentar totes les aplicacions que correu, per seguretat o pel sistema que utilitzeu per a connectar-les. Finalment, podeu usar docker-compose per a crear noves xarxes o llançar els contenidors en xarxes específiques.

[source, yaml]
----
version: '3.4'

services:
  proxy:
    image: busybox
    networks:
      - outside
  app:
    image: busybox
    networks:
      - default
      - inside

networks:
  outside:
    external: true
  default:
  inside:
     driver: bridge
    enable_ipv6: true
----

Les xarxes tenen la seva pròpia interfície amb la línia de comandos de docker:

    docker network create: crea una nova xarxa.
    docker network ls: mostra les xarxes de la nostra màquina.
    docker inspect: retorna informació relativa a una xarxa.
    docker network rm: elimina una xarxa.

////
