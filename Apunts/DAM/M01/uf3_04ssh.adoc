== SSH

// http://www.alexonlinux.com/ssh-crash-course
// https://hackertarget.com/ssh-examples-tunnels/
// https://www.hostinger.es/tutoriales/que-es-ssh
// https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process
// https://ioc.xtec.cat/materials/FP/Materials/2251_ASIX/ASIX_2251_M06/web/html/WebContent/u1/a2/continguts.html
// https://wiki.debian.org/SSH
// https://askubuntu.com/questions/311558/ssh-permission-denied-publickey
// https://kb.iu.edu/d/aews

=== Què és SSH?

SSH és un protocol criptogràfic de xarxa ubicat a la capa d'Aplicació del model OSI, que permet gestionar serveis de xarxa de forma segura.

Quan un ordinador envia informació a la xarxa, SSH l'encripta automàticament i quan aquesta arriba a destí SSH és capaç de desencriptar-la també automàticament. El resultat és que els usuaris poden treballar normalment sense ser conscients que les seves comunicacions estan encriptades de forma segura a través d'Internet.

SSH té una arquitectura client/servidor, el servidor SSH accepta o rebutja connexions entrants en funció de les credencials del client.

Existeixen dues versions del protocol SSH:

SSH-1:: Anomenada versió 1.5, obsoleta i amb problemes de seguretat.
SSH-2:: Versió recomanada del protocol.

=== Funcions de SSH

SSH proporciona:

* *Privacitat* de les dades mitjançant encriptació.
** SSH suporta gran varietat de xifrats: AES, ARCFOUR, Blowfish, Twofish, IDEA, DES, i triple-DES (3DES)
* *Integritat* de les comunicacions.
** Garanteix que les dades enviades no han estat alterades.
** Garanteix que les dades provenen de l'origen que se suposa que provenen.
* *Autenticació*, prova la identitat tant d'emissors com de receptors.
** SSH suporta autenticació:
*** Mitjançant passwords.
*** Mitjançant signatures de clau publica per a cada usuari.
*** Algunes implementacions suporten altres sistemes com ara kerberos, mòduls PAM, etc...
* *Autorització*, decidir que pot fer qui.
* *Forwarding* o *tunnelització* , permet encriptar altres sessions basades en TCP/IP
** SSH suporta tres tipus de "forwarding":
TCP port forwarding:::: Assegura qualsevol servei basat en TCP
X forwarding:::: Assegura el protocol X11
Agent forwarding:::: Permet que els clients SSH utilitzin claus privades emmagatzemades en sistemes remots.

=== Tecnologies de xifrat

Hi ha tres tecnologies de xifrat diferents utilitzades per SSH:

Encriptació simètrica::
El xifrat simètric és una forma d'encriptació en la qual s'utilitza una clau secreta tant per al xifrat com per al desxifrat d'un missatge tant pel client com pel host. Efectivament, qualsevol que posseeixi la clau pot desxifrar el missatge que es transfereix.
+
*Ens interessa perquè és ràpida però té l'inconvenient que abans de qualsevol xifrat s'ha de passar la clau al receptor*.

Encriptació asimètrica::
A diferència del xifrat simètric, el xifrat asimètric utilitza dues claus separades per al xifrat i el desxifrat. Aquestes dues claus es coneixen com la clau pública (public key) i la clau privada (private key). Juntes, totes dues claus formen un parell de claus públic-privat (public-private key pair).
+
*La clau privada mai deixa l'ordinador del seu propietari, això elimina el problema del traspas de claus de les encriptacions simètriques, té l'inconvenient que és un xifrat lent i per tant poc adequat per grans quantitats de dades*.

Hashing::
El hashing unidireccional és una altra forma de criptografia utilitzada en Secure Shell Connections. Les funcions de hash unidireccionals difereixen de les dues formes anteriors d'encriptació en el sentit que mai estan destinades a ser desxifrades. Generen un valor únic d'una longitud fixa per a cada entrada que no mostra una tendència clara que pugui explotar-se. Això els fa pràcticament impossibles de revertir.
+
*S'utilitzen, en general, per comprovar la integritat dels missatges rebuts*.

=== Com funciona SSH amb aquestes tècniques de xifrat?

La forma en què funciona SSH és mitjançant l'ús d'un model client-servidor per a permetre l'autenticació de dos sistemes remots i el xifrat de les dades que passa entre ells.

Hi ha *dues etapes* per a establir una connexió: 

Primer:: Tots dos sistemes han d'acordar estàndards d'encriptació per a protegir futures comunicacions.
Segon:: L'usuari ha d'autenticar-se, si les credencials coincideixen, es concedeix accés a l'usuari.

==== Negociació de xifrat de sessió

Quan un client intenta connectar-se al servidor a través de TCP, per defecte port 22, el servidor presenta els protocols de xifrat i les versions respectives que suporta. Si el client té una semblança similar de protocol i versió, s'aconsegueix un acord i s'inicia la connexió amb el protocol acceptat. El servidor també utilitza una clau pública asimètrica que el client pot utilitzar per a verificar l'autenticitat del host.

Una vegada que això s'estableix, les dues parts usen el que es coneix com a Algorisme d'Intercanvi de Claus *Diffie-Hellman* per a crear una clau simètrica. Aquest algorisme permet que tant el client com el servidor arribin a una *clau de xifrat compartida que s'utilitzarà d'ara endavant per a xifrar tota la sessió de comunicació*.

Així és com l'algorisme treballa en un nivell molt bàsic:

Tot plegat es basa en operacions matemàtiques computacionalment ràpides de realitzar i computacionalment lentes de revertir.

Vegem un exemple intuïtiu de com funciona l'algorisme de Diffie-Hellman:

En primer lloc necessitem una operació "fàcil" de fer i "difícil" de desfer::
Considerem l'exemple de barrejar dues gotes, suposem que totes les gotes tenen la mateixa quantitat de pintura i de diferent color. el resultat donarà una nova gota de color diferent. Aquesta operació és fàcil i ràpida de fer.
+
Per contra, obtenir el colors inicials a partir de les dues gotes barrejades és una operació lenta i difícil de realitzar.

Alice i Bob es volen posar d'acord i triar una mateixa  barreja de pintura sense que, el Sr. Swiper, observant els enviaments de pintura entre Alice i Bob, pugui deduir fàcilment cóm s'ha composat tal barreja::
. Alice i Bob trien *públicament* un color, per exemple  el **_G_**roc.
. Alice tria un color privat aleatori, **_À_**mbar.
. Bob tria un color privat aleatori, **_B_**lau.
. Bob envia a Alice la barreja *_G+B_*.
. Alice afegeix el seu color privat, *_A_* a la barreja rebuda de Bob, obtenint *_(G+B)+A_*.
. Alice envia a Bob la barreja *_G+A_*. 
. Bob afegeix el seu color privat, *_B_* a la barreja rebuda de Bob, obtenint *_(G+A)+B_*.
. Alice i Bob decideixen que el color triat és *_A+B+G_*.

Fixeu-vos que els colors *A,B,G* són presents als dos costats de la comunicació i per tant donen lloc a la mateixa barreja, però en cap moment s'han fet circular de forma visible els colors privats per separat.

==== Autenticació de l'usuari

L'etapa final abans que es concedeixi a l'usuari accés al servidor és *l'autenticació de les seves credencials*. 

Es poden utilitzar diferents mètodes per a l'autenticació depenent del que accepti el servidor.

El mètode més senzill és utilitzar una contrasenya. Se li demana a l'usuari que introdueixi el nom d'usuari, seguit de la contrasenya. Aquestes credencials passen amb seguretat a través del túnel xifrat simètricament, així que no hi ha cap possibilitat que siguin capturades per un tercer.

Encara que les contrasenyes es xifren, no es recomana usar contrasenyes per a connexions segures. Això es deu al fet que molts robots poden simplement activar les contrasenyes fàcils o predeterminades i obtenir accés al compte i que trencar un password és computacionalment més senzill que trencar altres mètodes d'encriptació. En el seu lloc, l'alternativa recomanada és *SSH Key Pairs*.

Es tracta d'un conjunt de claus asimètriques utilitzades per a autenticar a l'usuari sense necessitat d'introduir cap contrasenya.

El procediment és el següent:

. El *_Client_* envia un ID per la clau publica amb la que es vol autenticar al servidor.
. El servidor comprova el fitxer de claus autoritzades pel compte al que està intentant accedir el client.
. Si existeix alguna clau pública amb el ID enviat el servidor *genera un número aleatori* i utilitza aquesta clau publica per encriptar aquest número, el servidor envia al client el missatge generat.
. Si el client disposa de la clau privada associada, serà capaç de desencriptar el missatge rebut obtenint el nombre original.
. El *_client_* combina el número desencriptat amb la *clau de sessió* utilitzada per encriptar la comunicació, calcula un hash *_MD5_* i l'envia al servidor com a resposta al missatge rebut.
. El *_servidor_* utilitza la *mateixa clau de sessió* i el *número original* per a tornar a calcular el MD5 i *el compara amb el hash rebut*.
+
Si els dos valors MD5 coincideixen queda provat que el client estava en possessió de la clau privada i per tant el client *és autenticat*.

=== Què es pot fer amb SSH?

* Iniciar una sessió remota.
* Transmetre fitxers de manera segura.
* Fer còpies de seguretat de manera eficient i segura en combinació amb l’ordre rsync.
* Fer túnels per assegurar qualsevol servei que no es transmeti encriptat (HTTP, SMTP, VNC, etc.) o per travessar tallafocs que estiguin bloquejant el protocol.
* Reenviament automàtic de sessions X11 des d’un host remot (disposa d’aquesta funció openSSH però no altres implementacions d’SSH).
* Navegar pel web a través d’una connexió amb un servidor intermediari (proxy) xifrada amb programes clients que siguin compatibles amb el protocol SOCKS.
* Seguiment automatitzat i administració remota de servidors a través d’un o més dels mecanismes exposats anteriorment.
* Fent servir SSHFS (SSH File System), un sistema d’arxius basat en SSH que pot crear de manera segura un directori en un servidor remot i actuar com a sistema de fitxers en xarxa.

== OpenSSH

OpenSSH és una suite d'utilitats basades en el protocol SSH que inclou eines que reemplacen a les antigues utilitats de comunicació insegures.


=== Instal·lació

Pel servidor cal instal·lar:

[source, bash]
----
josep@host1:~$ sudo apt install openssh-server
----

Podem comprovar l'estat del servei:

[source, bash]
----
josep@host1:~$ sudo systemctl status ssh
----

Pel client ssh instal·larem:

[source, bash]
----
josep@host1:~$ sudo apt install openssh-client
----

==== Instal·lació per a Windows

El client de openssh ve instal·lat de sèrie a les últimes versions de Windows 10.

Les últimes versions de Windows també disposen del servidor de openssh com a component instal·lable opcional.

=== Arxius de configuració del client SSH

El client d’OpenSSH es pot configurar de manera prou flexible pert al que l’administrador pugui definir:

* Una configuració general per a tot el sistema.
* Perquè cada usuari pugui modificar els paràmetres adients per a les seves connexions.
* Perquè pugui especificar opcions determinades per a cada connexió individual.

==== Configuració del client SSH

La configuració del client es pot especificar:

* En la línia de comandes
* Al fitxer *_/home/usuari/.ssh/ssh_config_* a la carpeta home del usuari.
* Al fitxer *_/etc/ssh/ssh_config_* per a la configuració global.

Per a cada paràmetre, *el client* farà servir el primer valor trobat. És a dir, si s’especifica un paràmetre a la línia d’ordres no se’n consultarà el valor en els fitxers de configuració.

==== Configuració del servidor SSH

L’arxiu principal de configuració és *_/etc/ssh/sshd_config_*.

==== Altres arxius de configuració

Hi ha altres arxius de configuració que permeten de forma genèrica el filtratge, control d’accés i mecanismes de protecció de diferents serveis (POP, Sendmail, Telnet, SSH, etc.) actuant de fet com un tallafocs bàsic.

Així, si volem habilitar o restringir l’accés a determinats equips i serveis podem editar els arxius de configuració *_/etc/hosts.deny_* i *_/etc/hosts.allow_* indicant en la directiva dintre de l’arxiu el servei que volem controlar, en aquest cas, el dimoni SSH. D’aquesta manera el sistema, davant d’una petició d’accés al servei, fa la cerca següent, que conclou en el moment de la primera coincidència:

. Comprova l’arxiu *-/etc/hosts.allow_*. Si hi troba coincidència valida l’accés.
. Comprova l’arxiu *_/etc/hosts/deny_*. Si hi troba coincidència no valida l’accés.
. En cas de no trobar coincidència en cap dels arxius valida l’accés.

Exemples de directives d’aquests arxius:

[source, bash]
----
sshd: ALL # permet/denega l’accés ssh a tothom
sshd: 192.168.56.10 # permet/denega l’accés SSH de la IP 192.168.56.10
----

=== Connexió a una estació remota amb SSH

La funció més comuna per a SSH és establir una sessió de treball remota, les possibilitats més habituals són:

* Accés mitjançant usuari i password, el servidor ho ha d'admetre amb el paràmetre:
+
./etc/ssh/sshd_config
[source, bash]
----
PasswordAuthentucation yes
----

* Accés mitjançant clau compartida.

==== Accés mitjançant usuari i password

[IMPORTANT]
====
Per què el servidor admeti connexions ssh autenticades amb usuari i password cal la directiva:

----
PasswordAuthentication yes
----

Habilitada al fitxer *_/etc/ssh/sshd_conf_*
====

L’ús del client SSH és força senzill.

[source, bash]
----
ssh user@host
----

user:: és l’usuari que es connectarà a la màquina remota.
host:: representa la IP o el nom de domini del servidor SSH al qual ens volem connectar.

[source, bash]
----
usuari1@host1:~$ ssh usuari1@172.16.0.102
usuari1@172.16.0.102's password: 

usuari1@host2:~$ 
----

i finalitzem la sessió amb la comanda *_exit_*:

[source, bash]
----
usuari1@host2:~$ exit
logout
Connection to 172.16.0.102 closed.
usuari1@host1:~$ 
----

[IMPORTANT]
====
Si no s’especifica el nom d’usuari a l’hora d’invocar l’ordre SSH, intentarà fer la connexió amb l’usuari amb el qual estem connectats al terminal de GNU/Linux.
====

[source, bash]
----
usuari1@host1:~$ ssh 172.16.0.102
usuari1@172.16.0.102's password: 

usuari1@host2:~$ 
----

==== Accés mitjançant clau compartida

. Des del client generem un parell de claus:
+
[source, bash]
----
usuari1@host1:~$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/usuari1/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/usuari1/.ssh/id_rsa.
Your public key has been saved in /home/usuari1/.ssh/id_rsa.pub.
...
----
+
[IMPORTANT]
====
Si no protegim les claus amb una paraula de pas *qualsevol persona amb accés al ordinador podrà connectar-se en nom teu via SSH a qualsevol sistema que tingui la teva clau pública*.
====

. Verifiquem el què hem creat:
+
[source, bash]
----
usuari1@host1:~$ ls -l .ssh/
total 12
-rw------- 1 usuari1 usuari1 1675 Apr  9 09:52 id_rsa
-rw-r--r-- 1 usuari1 usuari1  395 Apr  9 09:52 id_rsa.pub
-rw-r--r-- 1 usuari1 usuari1  222 Apr  9 09:40 known_hosts
----
. Enviem la clau pública al servidor, *dues maneres*:
.. Copiant la clau manualment al servidor.
+
[source, bash]
----
usuari1@host1:~/.ssh$ scp /home/usuari1/.ssh/id_rsa.pub usuari2@172.16.0.102:/home/usuari2/
usuari2@172.16.0.102's password: 
id_rsa.pub                                  100%  395   687.0KB/s   00:00  
----

.. A la home de *_usuari1_* al servidor tindrem la clau pública enviada des del client.
+
[source, bash]
----
usuari2@host2:~$ ls
id_rsa.pub
----

.. Al servidor, a la compta d'usuari remota, caldrà copiar la clau pública al fitxer *_~/.ssh/autorized_keys_*.
+
[source, bash]
----
usuari1@host2:~$ mkdir ~/.ssh
usuari1@host2:~$ touch ~/.ssh/authorized_keys

usuari1@host2:~$ cat ~/id_rsa.pub >> ~/.ssh/autorized_keys 

usuari1@host2:~$ rm id_rsa.pub
----
+
[NOTE]
====
Hem donat la clau del *_usuari1@host1_* a *_usuari2@host2_*, per tant podrem entrar com a usuari2 a host2 sense que ens demani un password.
====

.. Ara hauriem de ser capaços d'accedir al compte remot
+
[source, bash]
----
usuari1@host1:~$ ssh usuari2@172.16.0.102

usuari2@host2:~$ 
----
+
[NOTE]
====
Si hem posat una paraula de pas a la clau en aquest punt la demanarà.
====

. Utilitzant la comanda *_ssh-copy-id_* que fa tots els passos anteriors automàticament.
+
[source, bash]
----
usuari2@host1:~$ ssh-key -t rsa

usuari2@host1:~$ ssh-copy-id usuari2@172.16.0.102
----

==== Opcions del client ssh

El client d’OpenSSH disposa de diferents opcions que es detallen en el seu manual. Algunes de les més freqüents són les descrites  continuació.

-i:: Permet especificar un fitxer amb la clau privada que intentarà l'accés remot.

-X:: Habilita la redirecció de X11.
-C:: Activa la compressió gzip en la connexió. Es recomana activar la compressió si s’està emprant SSH amb un enllaç lent, com un mòdem. Si l’enllaç és de banda ampla es recomana treballar sense compressió.
-p port:: Port al qual es connectarà en l’equip remot. De manera predeterminada *el servidor SSH s’executa al port TCP 22*, però si es tracta d’un servidor accessible des d’Internet és recomanable escollir un altre port per evitar els intents de connexió.
-v/-vvvv:: Mostra diferents nivells d'informació quan s'executa la comanda *_ssh_*. útil per depurar.

=== Transferència d'arxius entre equips

Entre les utilitats incloses en la distribució d’OpenSSH hi ha l'ordre *_scp_*. Aquesta ordre permet transferir fitxers per una connexió ssh.

La sintaxi bàsica de la instrucció *_scp_* és:

----
scp [opcions] [[user@]host_origen:]fitxer1 [[user@]host_desti:]fitxer2
----

==== Copiar un fitxer del host local a un host remot

[source, bash]
----
usuari1@host1:~$ scp file usuari1@172.16.0.102:/home/usuari1/Desktop/filecpy
----

==== Copiar un fitxer d'un host remot al host local

[source, bash]
----
usuari1@host1:~$ scp usuari1@172.16.0.102:/home/usuari1/Desktop/filecpy filecpy2
----

==== Copiar una carpeta d'un host remot al host local

[source, bash]
----
usuari1@host1:~$ scp -r usuari1@172.16.0.102:/home/usuari1/tmp/ ~/tmp/
----

==== Copiar una carpeta del host local a un host remot

[source, bash]
----
usuari1@host1:~$ scp -r ~/tmp/ usuari1@172.16.0.102:/home/usuari1/tmp/ 
----

==== Algunes opcions útils

*_scp_* suporta algunes opcions específiques que no existeixen a l'eina *_ssh_*.

-l limit:: Establir un límit a l’amplada de banda en kbps.
-p	:: Preservar el temps de modificació, accés i els permisos dels fitxers copiats.
-r:: Fer una còpia recursiva pels directoris.


=== Paràmetres habituals per les connexions SSH

==== Llençar una comanda remotament

La comanda *_ssh_* permet llençar una comanda determinada al servidor:

[source, bash]
----
usuari1@host1:~$ ssh usuari1@172.16.0.102 "ls /home/usuari1"
----

Un altre exemple:

[source, bash]
----
usuari1@host1:~$ ssh usuari1@172.16.0.102 "mkdir /home/usuari1/.ssh"

usuari1@host1:~$ cat ~/.ssh/id_rsa.pub | ssh usuari1@172.16.0.102 "cat >> .ssh/authorized_keys"
----

==== netcat

Netcat és una eina de xarxa que permet a través d'intèrpret de comandes i amb una sintaxi senzilla:

. Obrir ports TCP/UDP en un HOST (quedant netcat a l'escolta)
. Associar una shell a un port en concret (per a connectar-se per exemple a MS-DOS o a l'intèrpret bash de Linux remotament) 
. Forçar connexions UDP/TCP (útil per exemple per a realitzar rastrejos de ports o realitzar transferències d'arxius bit a bit entre dos equips).

Va ser originalment desenvolupada per *Hobbit* al 1996 i alliberada sota una llicència de programari lliure permissiva per a UNIX. Posteriorment va ser portada a Windows i Mac US X entre altres plataformes. Existeixen molts forks d'aquesta eina que afegeixen característiques noves com GNU *_Netcat_* o *_Cryptcat_*.

Entre les seves múltiples aplicacions, és freqüent la depuració d'aplicacions de xarxa. També és utilitzada sovint per a obrir portes posteriors en un sistema.

L'ús més senzill és obrir un port i posar-lo en estat LISTEN:

[source, bash]
----
usuari2@host2:~$ nc -l  -p 10000
----

I permetre la connexió a un port obert des d'un host remot:

[source, bash]
----
usuari2@host1:~$ nc 172.16.0.102 10000
----

Podem veure el contingut dels paquets enviats amb:

[source, bash]
----
usuari2@host2:~$ sudo tcpdump -A -i eth1 -t tcp 
----

-i:: Intercície per la qual escoltem
-t:: Protocol que escoltem
-A:: Veure el contingut del paquet

==== SSH Tunnel (port forwarding)

Fent servir SSH es poden establir túnels encriptats pels quals es pot transmetre qualsevol protocol que faci servir TCP. 

La versió més simple de túnel ssh obre un port a la màquina local que connecta a un altre port al final de túnel.

.Al client:
[source, bash]
----
usuari1@host1:~$ ssh -f -N -L 0.0.0.0:9999:127.0.0.1:10000 usuari1@172.16.0.102
----

-N:: Especifica que no es vol executar cap ordre remota i no s’obrirà cap terminal interactiu. És d’utilitat quan es fa servir SSH per al forwarding de ports.
-f:: Com que no ens cal interactivitat, aquesta opció fa que SSH quedi en segon pla i es dissocii de la shell actual, cosa que converteix el procés en un dimoni (cal l’opció -N).

.Al servidor:
[source, bash]
----
usuari2@host2:~$ nc -l  -p 10000
----

.Al client:
[source, bash]
----
usuari1@host1:~$ nc 127.0.0.1 9999
----

==== Túnel invers (Reverse forwarding)

Podríem obrir un túnel des del servidor al client.

.Al Client (host1)
[source, bash]
----
usuari1@host1:~$ nc -l -p 10000
usuari1@host1:~$ ssh -f -N -R 0.0.0.0:1999:127.0.0.1:10000 usuari1@172.16.0.102
----

.Al servidor (host2) 
[source, bash]
----
usuari2@host2:~$ nc 127.0.0.1 1999
----

[WARNING]
==== 
Estem exposant la xarxa interna a l'exterior, això és un forat de seguretat.
qualsevol persona amb la clau pública podria accedir al servidor.
====

==== X11 Forwarding

El servidor ha de tenir

----
X11Forwarding yes
----

El usuari que utilitzem per entrar ha d'haver entrat en algun moment amb entorn gràfic per tenir el fitxer *_.xauthority_* etc configurats.

[source, bash]
----
usuari1@host1:~$ ssh -X usuari1@172.16.0.102 mousepad
----

==== Proxy SOCKS

SSH permet crear un túnel des de l’equip local fins a un servidor remot i un cop connectats a aquest servidor remot utilitzar el túnel per fer la navegació per Internet.

. Treiem internet a *_host1_*
+
[source, bash]
----
usuari1@host1:~$ sudo ifdown eth0
----

. Volem accedir a Internet fent servir de proxy el *_host2_*.
+
[source, bash]
----
usuari2@host1:~$ ssh -f -N -D 0.0.0.0:8888 usuari2@172.16.0.102
----

-D 8888:: Habilita el forwarding dinàmic de ports fent servir, en aquest cas, el port 1080 estàndard per a servidors SOCKS.

+
[TIP]
====
0.0.0.0 indica qualsevol interfície de xarxa de la màquina.
====

. Configurem Navegador per utilitzar SOCKS
+
image::socks.png[]

. Podem tancar la màquina virtual del *_host2_* per veure que efectivament la connexió a Internet de *_host1_* s'interromp.
