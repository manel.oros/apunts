== Configuració de samba

// https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Standalone_Server

// https://www.sololinux.es/instalar-un-servidor-samba-en-debian-9-stretch

// https://linuxconfig.org/how-to-configure-samba-server-share-on-debian-9-stretch-linux

// https://www.samba.org/samba/docs/current/man-html/smbclient.1.html

// https://www.lifewire.com/net-use-command-2618096

=== Nota prèvia footnote:[https://support.microsoft.com/en-us/help/4034314/smbv1-is-not-installed-by-default-in-windows]

A Windows versió 1709 (RS3) i les versions posteriors, *el protocol SMBv1 ja no està instal·lat de manera predeterminada*.

[TIP]
====
Pots utilitzar *winver* o bé *system info*.
====

SMBv1 té el comportament següent en Windows 10 versió 1709 (RS3):

* SMBv1 ara té sub-característiques de client i servidor que es poden desinstal·lar per separat.
* Windows 10 Enterprise i Windows 10 Education ja no contenen el client o el servidor SMBv1 per defecte després d'una instal·lació neta.
* Windows Server 2016 ja no conté el client o el servidor SMBv1 de forma predeterminada després d'una instal·lació neta.
* Windows 10 Home i Windows 10 Professional ja no conté el servidor SMBv1 per defecte després d'una instal·lació neta.
* Windows 10 Home i Windows 10 Professional encara contenen el client SMBv1 per defecte després d'una instal·lació neta. Si el client SMBv1 no s'utilitza durant 15 dies en total (excloent que l'ordinador està desactivat), es desinstal·la automàticament.
* Si el client o el servidor SMBv1 no s'utilitza durant 15 dies en total (excloent el temps durant el qual l'ordinador està desactivat), es desinstal·len automàticament.
* Les característiques de la versió 2.02, 2.1, 3.0, 3.02 i 3.1.1 de la versió SMB encara són totalment compatibles i s'inclouen per defecte com a part dels binaris SMBv2.
* *Atès que el servei de detecció de xarxes es basa en SMBv1, el servei es desinstal·la si el client o servidor SMBv1 està desinstal·lat. Això significa que no apareixeran dispositius de xarxa en base a NetBIOS*. 
* SMBv1 encara es pot tornar a instal·lar a totes les edicions de Windows 10 i Windows Server 2016.

[IMPORTANT]
====
Per unificar l'entorn de pràctiques assegureu-vos que teniu la versió SMBv1 deshabilitada, ja posats treballarem amb SMBv3: 

image::win_smb1.png[]
====

=== Instal·lació

. Instal·lació del servidor:
+
[source, bash]
----
abcd@abcd:~$ sudo apt install samba

abcd@abcd:~$ sudo smbstatus --version
Version 4.13.14-Ubuntu
----
+

. Permetre la connexió a servidors SMB/CIFS:
+
[source, bash]
----
abcd@abcd:~$ sudo apt install smbclient
----

. Muntar sistemes de fitxers remots:
+
[source, bash]
----
abcd@abcd:~$ sudo apt install cifs-utils
----

=== Daemons de samba

Samba consisteix en tres serveis::
* *_nmbd_*
* *_smbd_*
* *_winbindd_*.

==== nmbd

Té la responsabilitat de registrar i resoldre els noms. És el primer servei de samba en arrencar.

[source, bash]
----
josep@sambaServer:~$ ps -C nmbd
  PID TTY          TIME CMD
  621 ?        00:00:00 nmbd
----

==== smbd

El servei *_smbd_* gestiona la transferència de fitxers i l'autenticació.

[source, bash]
----
josep@sambaServer:~$ ps -C smbd
  PID TTY          TIME CMD
  625 ?        00:00:00 smbd
----

==== winbindd

El servei *_winbindd_* només s'engega per gestionar la membresia a un Domini de Windows.

En Debian cal instal·lar-lo separadament amb el paquet anomenat *_windbind_*.

=== Gestionar dimoni Samba

Amb les comandes status, start, stop i restart podem gestionar el dimoni Samba. És important tenir en compte que el "daemon" de Samba no detecta de forma immediata els canvis en el fitxer *smb.conf*. Els canvis s'actualitzen aproximadament cada 60 segons. Per tant, si volem verificar un canvi de forma immediata, s'ha de reiniciar el dimoni perquè els canvis siguin efectius al moment.

També ens pot ser útil per verificar l'estat del servei i si aquets no s'ha pogut iniciar per algun motiu.

La comanda que ens ho permet és *_systemctl_*:

[source, bash]
----
abcd@abcd:~$ sudo systemctl status smbd
● smbd.service - Samba SMB Daemon
     Loaded: loaded (/lib/systemd/system/smbd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-01-31 10:56:32 CET; 8min ago
       Docs: man:smbd(8)
             man:samba(7)
             man:smb.conf(5)
   Main PID: 2599 (smbd)
     Status: "smbd: ready to serve connections..."
      Tasks: 4 (limit: 1082)
     Memory: 10.9M
     CGroup: /system.slice/smbd.service
             ├─2599 /usr/sbin/smbd --foreground --no-process-group
             ├─2601 /usr/sbin/smbd --foreground --no-process-group
             ├─2602 /usr/sbin/smbd --foreground --no-process-group
             └─2603 /usr/sbin/smbd --foreground --no-process-group

ene 31 10:56:32 abcd systemd[1]: Starting Samba SMB Daemon...
ene 31 10:56:32 abcd update-apparmor-samba-profile[2593]: grep: /etc/apparmor.d/samba/smbd-shares: No such file or >
ene 31 10:56:32 abcd update-apparmor-samba-profile[2596]: diff: /etc/apparmor.d/samba/smbd-shares: No such file or >
ene 31 10:56:32 abcd systemd[1]: Started Samba SMB Daemon.

----

[source, bash]
----
abcd@abcd:~$ sudo systemctl status nmbd
● nmbd.service - Samba NMB Daemon
     Loaded: loaded (/lib/systemd/system/nmbd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-01-31 10:56:32 CET; 9min ago
       Docs: man:nmbd(8)
             man:samba(7)
             man:smb.conf(5)
   Main PID: 2589 (nmbd)
     Status: "nmbd: ready to serve connections..."
      Tasks: 1 (limit: 1082)
     Memory: 3.0M
     CGroup: /system.slice/nmbd.service
             └─2589 /usr/sbin/nmbd --foreground --no-process-group

ene 31 10:56:32 abcd systemd[1]: Starting Samba NMB Daemon...
ene 31 10:56:32 abcd systemd[1]: Started Samba NMB Daemon.
----

També podem fer-ho amb la comanda *_smbcontrol_* de la següent forma:

_smbcontrol smbd reload-config_

==== Estat dels processos ambd i nmbd a Linux
Tot servei està lligat també a un procès principal. Aquest es pot consultar amb la següent comanda:
[source, bash]
----
bcd@abcd:~$ ps -C nmbd; ps -C smbd
    PID TTY          TIME CMD
   2589 ?        00:00:00 nmbd
    PID TTY          TIME CMD
   2599 ?        00:00:00 smbd
----

==== Firewall UFW
En alguns casos és possible que la configuració del tallafocs de Linux UFW (Uncomplicated Firewall) incorpori alguna regla que no permeti el tràfic de paquets SMB. Per a evitar problemes d'aquest tipus podem deshabilitar la protecció temporalment:

[source, bash]
----
abcd@abcd:~$ sudo ufw allow samba
Rules updated
Rules updated (v6)
----

Per tornar-ho a habilitar de nou:

[source, bash]
----
abcd@abcd:~$ sudo ufw deny samba
Rules updated
Rules updated (v6)
----

==== Firewall de Windows
De la mateixa manera que amb UFW, és possible que les máquines Windows incorporin una regla per defecte que no permeti el tràfic de paquets SMB o bé qualsevol tràfic entrant. S'ha de crear una nova regla que permeti aquest tràfic o bé desactivar el firewall temporalment mentre efectuem les proves de comunicació.

=== Documentació

La documentació oficial de samba es troba a https://www.samba.org/samba/docs/

Si hem instal·lat paquets de samba al servidor es pot trobar documentació sobre la versió, les millores i les novetats a la carpeta *_/usr/share/doc/_*

La pàgina del manual *_man smb.conf_* dona molta informació sobre el protocol smb.

=== Fitxer de configuració "_/etc/samba/smb.conf_"
La configuració de samba es realitza al fitxer *_/etc/samba/smb.conf_*. 
En funció de la distribució el fitxer *_smb.conf_* pot estar a diferents llocs, la comanda *_smbd -b_* mostra informació del _build_ actual de l'aplicació.

[source, bash]
----
abcd@abcd:~$ sudo smbd -b | grep -i config
   CONFIGFILE: /etc/samba/smb.conf
   HAVE_CONFIG_H
   HAVE_KRB5_CONFIG_GET_BOOL_DEFAULT
   CONFIG_H_IS_FROM_SAMBA
----

==== Configuració mínima de smb.conf

L'exemple següent determina el mínim fitxer de configuració de samba. Permet arrencar i ser visible a d'altres ordinadors. 

[source, bash]
----
[global]
workgroup = WORKGROUP  

[firstshare]
path = /srv/public
----

El fitxer està format per diferents seccions:

.Windows workgroup
****
Un *grup de treball* en el context d'un sistema Windows fa referència a una xarxa d'àrea local entre iguals.

Els ordinadors que estan al mateix grup de treball poden compartir fitxers, impressores o la connexió a Internet.
****

==== Forçar versions concretes de SMB a client i servidor

Podem afegit els paràmetres *_client max protocol_*, *_client min protocol_*, *_server max protocol_* i *_server min protocol_*.

[source, bash]
----
josep@sambaServer:/etc/samba$ cat smb.conf
[global]
workgroup = WORKGROUP  
client max protocol = SMB3 
client min protocol = SMB3 
server max protocol = SMB3 
server min protocol = SMB3 

[firstshare]
path = /srv/public
----

==== smbclient

Amb la comanda smbclient executada al servidor podem llistar els recursos compartits:

_smbclient -N -L //localhost_

=== ((/usr/bin/testparm))

La comanda *_testparm_* permet verificar la sintaxi del fitxer *_smb.conf_*.

==== Verificar la sintaxi del fitxer smb.conf

La comanda *_testparm_* permet verificar la sintaxi del fitxer *_smb.conf_*.

[source, bash]
----
josep@sambaServer:/etc/samba$ testparm
Load smb config files from /etc/samba/smb.conf
rlimit_max: increasing rlimit_max (1024) to minimum Windows limit (16384)
Processing section "[firstshare]"
Loaded services file OK.
Server role: ROLE_STANDALONE

Press enter to see a dump of your service definitions

# Global parameters
[global]
	client max protocol = SMB3
	client min protocol = SMB3
	server min protocol = SMB3
	idmap config * : backend = tdb


[firstshare]
	path = /srv/public
----

==== testparm -v

La opció *_-v_* de la comanda *_testparm_* mostra els valors per defecte junt amb els valors modificats del fitxer *_smb.conf_*.

[source, bash]
----
josep@sambaServer:/etc/samba$ testparm -v
Load smb config files from /etc/samba/smb.conf
rlimit_max: increasing rlimit_max (1024) to minimum Windows limit (16384)
Processing section "[firstshare]"
Loaded services file OK.
Server role: ROLE_STANDALONE

Press enter to see a dump of your service definitions
 
# Global parameters
[global]
	bind interfaces only = No
	config backend = file
	dos charset = CP850
	enable core files = Yes
	interfaces = 
	multicast dns register = Yes
	netbios aliases = 
	netbios name = SAMBASERVER
	netbios scope = 
	realm = 
	server services = s3fs, rpc, nbt, wrepl, ldap, cldap, kdc, drepl, winbindd, ntp_signd, kcc, dnsupdate, dns
	server string = Samba 4.5.12-Debian
	share backend = classic
	unix charset = UTF-8
	workgroup = WORKGROUP
....
----

==== testparm -s

Els "daemons" de samba comproven constantment (cada 60 segons) el fitxer *_smb.conf_* i per tant *és interessant mantenir aquest fitxer el més petit possible*.

Per altra banda, des d'un punt de vista de la mantenibilitat, val la pena:

* Comentar el fitxer de configuració.
* Fer explicites les opcions necessàries que tenen els mateixos valors que els valors per defecte.

Els dos punts anteriors fan més gran el fitxer de configuració, cosa que no volem.

La solució és:

. Mantenir el fitxer de configuració en un altre fitxer, posem *_smb.conf.full_*, comentat i explicitant-ho tot de cara a fer-lo *el més intel·ligible possible*.
. Utilitzar la comanda *_testparm -s_* per generar el fitxer de configuració equivalent a l'anterior amb la mínima longitud. (això és el que fa precisament aquesta opció)
. Utilitzar el nou fitxer com a fitxer de configuració *_smb.conf_*.

Per exemple:

. Mantenim un segon fitxer de configuració:
+
[source, bash]
----
josep@sambaServer:/etc/samba$ cat smb.conf.full
[global]

# Grup de treball
workgroup = WORKGROUP

# Forcem versió SMB3
client max protocol = SMB3
client min protocol = SMB3
server max protocol = SMB3
server min protocol = SMB3

# Carpetes compartides
[firstshare]
path = /srv/public
----

. Mirem què retorna *_testparm -s_*:
+
[source, bash]
----
josep@sambaServer:/etc/samba$ testparm -s smb.conf.full
Load smb config files from smb.conf.full
rlimit_max: increasing rlimit_max (1024) to minimum Windows limit (16384)
Processing section "[firstshare]"
Loaded services file OK.
Server role: ROLE_STANDALONE

# Global parameters
[global]
	client max protocol = SMB3
	client min protocol = SMB3
	server min protocol = SMB3
	idmap config * : backend = tdb


[firstshare]
	path = /srv/public
----

. Guardem el resultat a *_smb.conf_*.
+
[source, bash]
----
root@sambaServer:/etc/samba# testparm -s smb.conf.full > smb.conf
----

=== Clients de Samba 

Podem veure els servidors de samba de diferents maneres.

==== (Windows) net view

Des de Windows 10 cal habilitar la "detección de redes i recursos compartidos".

image::smb1.png[]

Cal verificar que la màquina es troba al mateix grup de treball que els clients samba.

image:smb2.png[]

Si volem habilitar les capacitats de cerca de recursos compartits (*_Red_*) caldrà habilitar els següents dos serveis:

image:smb7.png[]

image:smb8.png[]

Amb aquests passos obtindrem:

image:smb3.png[]

Per fer completament operativa la comanda *_net view_* a les ultimes versions de Windows caldrà habilitar una directiva de grup:

[source, bash]
----
gpedit.msc
----

Editor de directivas de grupo local -> Configuración de equipo -> Plantillas administrativas -> Red -> Estacion de trabajo Lanman -> Habilitar inicios de sesión de invitado no seguros

image:smb9.png[]

===== new view - Deprecated

La comanda *_net view_* mostra els ordinadors accessibles del grup de treball o domini actual.

[source, bash]
----
C:\Users\pep>net view
Servidor               Descripción

-------------------------------------------------------------------------------
\\SAMBASERVER          Samba 4.5.16-Debian                                 
\\WINSMB                                                                   
Se ha completado el comando correctamente.
----

[WARNING]
====
Les últimes actualitzacions de Windows 10 limiten l'ús del descobriment de servidors samba.

En particular la comanda *_net view_* ha deixat de funcionar amb el següent error:

[source, bash]
----
C:\Users\pep>net view
Error de sistema 6118.

La lista de servidores de este grupo de trabajo no se encuentra disponible en este momento.
----
====

===== new view \\servidor_samba

La comanda *_net view \\servidor_samba_* mostra totes les carpetes compartides del servidor. 

[WARNING]
====
Caldrà habilitar la directiva de grup "Habilitar inicios de sesión de invitado no seguros".
====

[source, bash]
----
C:\Users\pep>net view \\172.16.0.2
Recursos compartidos en \\172.16.0.2

Samba 4.5.12-Debian

Nombre de recurso compartido  Tipo   Usado como  Comentario

-------------------------------------------------------------------------------
carpeta_compartida                         Disco
Se ha completado el comando correctamente.
----

==== ((smbtree)) - Deprecated

La comanda *_smbtree_* és similar al "Entorno de red" de Windows, té els mateixos problemes que aqueta i no funciona correctament amb les últimes versions de Windows i de SMB.

====  (Linux) ((smbclient))

La comanda *_smbclient_* permet veure i compartir informació del servidor smb. Mostra totes les comparticions, el grup de treball i d'altres informacions.

* La opció *_-N_* permet entrar un password buit. (*_Deperecated_* en Windows)
* La opció *_-L host_* mostra les comparticions de _host_.
* La opció *_-m_* permet determinar la versió de SMB amb la que es vol treballar.
* La opció *_-U_* permet indicar quin usuari i opcionalment quin password utilitzar per accedir al recurs compartit.
** La sintaxi és *_usuari%password_*.

===== smbclient anonim

L'accés anònim segueix funcionant en servidors samba Linux però no en Windows.

.Accés anònim contra un servidor samba Linux
[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3 -NL sambaServer
WARNING: The "syslog" option is deprecated
Anonymous login successful

	Sharename       Type      Comment
	---------       ----      -------
	firstshare      Disk      
	IPC$            IPC       IPC Service (Samba 4.5.12-Debian)
Anonymous login successful

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
----

.Accés anònim contra un servidor samba Windows
[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3 -NL winsmb
WARNING: The "syslog" option is deprecated
session setup failed: NT_STATUS_ACCESS_DENIED
----

.Accés anònim contra un servidor samba Windows explicitant l'usuari guest sense password
[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3  -U guest% //sambaServer/firstShare 
WARNING: The "syslog" option is deprecated
Anonymous login successful
tree connect failed: NT_STATUS_ACCESS_DENIED
----

===== smbclient amb credencials

Podem utilitzar *_smbclient_* proporcionant credencials, en aquest cast segueix funcionant en les versions actuals de Windows.

[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3 -L winsmb
WARNING: The "syslog" option is deprecated
Enter josep's password: 
Domain=[WINSMB] OS=[] Server=[]

	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Admin remota
	C$              Disk      Recurso predeterminado
	IPC$            IPC       IPC remota
	shared          Disk      
Domain=[WINSMB] OS=[] Server=[]
----

==== Nautilus

L'explorador de fitxers *_Nautilus_* pot fer de client de recursos compartits en Samba de forma gràfica.

image::smb4.png[]

image::smb6.png[]

=== Comprovar les connexions al servidor samba ((smbstatus))

La comanda *_smbstatus_* permet veure l'estat actual de les connexions amb el servidor samba.

[source, bash]
----
josep@sambaServer:/etc/samba$ sudo smbstatus

Samba version 4.5.12-Debian
PID     Username     Group        Machine                                   Protocol Version  Encryption           Signing              
----------------------------------------------------------------------------------------------------------------------------------------
2564    josepsamba   josepsamba   172.16.0.3 (ipv4:172.16.0.3:53528)        SMB3_11           -                    partial(AES-128-CMAC)

Service      pid     Machine       Connected at                     Encryption   Signing     
---------------------------------------------------------------------------------------------
firstshare   2564    172.16.0.3    vie mar 29 12:38:54 2019 CET     -            -           

No locked files
----

=== Compartir un directori 

Per compartir una carpeta cal especificar una secció *_[nom_carpeta_compartida]_* al fitxer *_smb.conf_*.

Alguns dels paràmetres més comuns són els següents:

path:: Determina la ruta de la carpeta compartida al sistema de fitxers.
read only:: Determina els permisos de lectura/escriptura de la carpeta compartida.
guest ok:: Habilita l'accés anònim.
browsable:: Mostra la carpeta compartida a la llista de comparticions quan un client SMB o demana, per exemple *_net view_*.

.Un exemple de fitxer smb.conf
[source, bash]
----
josep@sambaServer:/etc/samba$ cat smb.conf
[global]
workgroup = WORKGROUP
security = user # Accés als recursos compartits amb usr/pwd
map to guest = Bad User 

log file = /var/log/samba/%m
log level = 1

client max protocol = SMB3
client min protocol = SMB3
server max protocol = SMB3
server min protocol = SMB3

[guest]
path = /srv/guest
read only = yes
guest ok = yes
browsable = yes

[firstshare]
path = /srv/public
read only = no
guest ok = no
browsable = no
----

[NOTE]
====
El paràmetre *_map to guest = Never | Bad User | Bad Password_* indica que si s'accedeix amb un usuari o password incorrecte automàticament es proposa un accés anònim.
====

Amb l'anterior exemple obtindríem:

[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3 -NL sambaServer

	Sharename       Type      Comment
	---------       ----      -------
	guest           Disk      
	IPC$            IPC       IPC Service (Samba 4.5.12-Debian)

	Server               Comment
	---------            -------

	Workgroup            Master
	---------            -------
----

[NOTE]
====
Veiem que no apareix *_[firstshare]_* ja que no es *_browsable_*.
====

==== Accedir a un recurs compartit des de smbclient 

[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3   --user josep%1234 //sambaServer/guest
smb: \> ls
  .                                   D        0  Wed Mar 27 10:39:38 2019
  ..                                  D        0  Tue Mar 26 11:43:02 2019
  estic_a_guest.txt                   N        0  Wed Mar 27 10:39:38 2019

		7158264 blocks of size 1024. 3205112 blocks available
smb: \> get estic_a_guest.txt 
getting file \estic_a_guest.txt of size 0 as estic_a_guest.txt (0,0 KiloBytes/sec) (average 0,0 KiloBytes/sec)
smb: \> exit
josep@sambaClient:~$ ls
Descargas   Escritorio         Imágenes  Plantillas  tmp
Documentos  estic_a_guest.txt  Música    Público     Vídeos
----

[NOTE]
====
Intentem accedir al recurs compartit amb un usuari inexistent, *_josep_*,  i entrem com a *_guest_*.
====

==== Accedir a un recurs compartit des de Windows amb net use

Podem utilitzar la comanda *_net use_* per muntar un recurs compartit en samba.

En Windows, l'accés anònim no està permès per defecte:

[source, bash]
----
C:\Users\pep>net use k: \\sambaServer\firstshare
Error de sistema 1272.

No se puede obtener acceso a esta carpeta compartida porque las directivas de seguridad de la organización bloquean el acceso de invitados no autenticado. Estas directivas ayudan a proteger el equipo de los dispositivos malintencionados o no seguros en la red.
----

=== Creació d'un usuari de samba

Per un tema de seguretat volem deixar d'utilitzar l'usuari *_root_* com a propietari de dels fitxers i dels directoris gestionats per samba.

Creem un nou usuari *_sambauser_* que serà el propietari de les carpetes compartides per samba.

Afegim una descripció clara de l'ús de l'usuari i no li assignem shell de login.

[source, bash]
----
josep@sambaServer:/etc/samba$ sudo useradd -s /bin/false sambauser
josep@sambaServer:/etc/samba$ sudo usermod -c "Acces anonim a samba" sambauser
josep@sambaServer:/etc/samba$ sudo passwd sambauser
Introduzca la nueva contraseña de UNIX: 
Vuelva a escribir la nueva contraseña de UNIX: 
passwd: contraseña actualizada correctamente
----

Canviem la propietat de fitxesrs i directoris compartits.

[source, bash]
----
josep@sambaServer:/etc/samba$ sudo chown -R sambauser:sambauser /srv/guest/

josep@sambaServer:/etc/samba$ sudo chown -R sambauser:sambauser /srv/public/
----

Forcem al servidor Samba que tots els fitxers creats a través de Samba siguin propietat del nou usuari.

[source, bash]
----
josep@sambaServer:/etc/samba$ sudo smbpasswd -a sambauser
New SMB password:
Retype new SMB password:
Added user sambauser.
----

Per veure els usuaris que hem donat d’alta amb Samba (i que per tant poden compartir arxius), només cal fer : 

[source, bash]
----
josep@sambaServer:/etc/samba$ cat /etc/group | tail -1
sambauser:x:1001:
----

O utilitzar la comanda més especifica *_((pdbedit))_*:

[source, bash]
----
josep@sambaServer:/etc/samba$ sudo pdbedit -L
sambauser:1001:Acces anonim a samba
----

Arribats a aquest punt podem forçar la propietat de tots els fitxers de la carpeta compartida de la següent manera:

[source, bash]
----
[firstshare]
	path = /srv/public
	browseable = No
	force group = sambauser
	force user = sambauser
	read only = No
----

[NOTE]
====
Fixeu-vos que quan un usuari escriu un fitxer a la carpeta compartida o farà "impersonat" en l'usuari *_sambauser_* i no li caldrà saber el password.
====

[NOTE]
====
Si intentem entrar a la carpeta compartida amb aquest nou usuari podríem sense problemes.

[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3   --user sambauser%1234 //sambaServer/firstshare
Domain=[SAMBASERVER] OS=[] Server=[]
smb: \> ls
  .                                   D        0  Wed Mar 27 10:39:18 2019
  ..                                  D        0  Tue Mar 26 11:43:02 2019
  estic_a_public.txt                  N        0  Wed Mar 27 10:39:18 2019

		7158264 blocks of size 1024. 3205056 blocks available
smb: \> 
----
====

=== Autenticació amb samba

Seguint la línia de l'apartat anterior, ara volem crear usuaris que tinguin permisos d'accés a la carpeta compartida.

El procediment és similar a l'exemple anterior.

. Creem tots els usuaris que facin falta:
+
[source, bash]
----
josep@sambaServer:/etc/samba$ sudo useradd -c "Josep Samba" josepsamba
josep@sambaServer:/etc/samba$ sudo passwd josepsamba
Introduzca la nueva contraseña de UNIX: 
Vuelva a escribir la nueva contraseña de UNIX: 
passwd: contraseña actualizada correctamente

----
. Els afegim com a usuaris de samba:
+
[source, bash]
----
josep@sambaServer:/etc/samba$ sudo smbpasswd -a josepsamba
New SMB password:
Retype new SMB password:
Added user josepsamba.
----

En aquest punt els usuaris, *tots*, ja poden entrar a les carpetes compartides.

==== Des de Linux

[source, bash]
----
josep@sambaClient:~$ smbclient -m SMB3   --user josepsamba%1234 //sambaServer/firstshare
Domain=[SAMBASERVER] OS=[] Server=[]
smb: \> ls
  .                                   D        0  Wed Mar 27 10:39:18 2019
  ..                                  D        0  Tue Mar 26 11:43:02 2019
  estic_a_public.txt                  N        0  Wed Mar 27 10:39:18 2019

		7158264 blocks of size 1024. 3205052 blocks available
smb: \> mkdir dirjosepsamba
smb: \> exit
----

Fixeu-vos que el directori s'ha creat amb propietari *_sambauser_*.

[source, bash]
----
josep@sambaServer:/etc/samba$ ls -l /srv/public/
total 4
drwxr-xr-x 2 sambauser sambauser 4096 mar 27 12:13 dirjosepsamba
-rw-r--r-- 1 sambauser sambauser    0 mar 27 10:39 estic_a_public.txt
----

==== Des de Windows

Podem accedir al recurs compartit amb la comanda *_((net use))_*.

[source, bash]
----
C:\Users\pep>net use k: \\172.16.0.2\firstshare /user:josepsamba
Escriba la contraseña de "josepsamba" para conectar a "172.16.0.2":
Error de sistema 86.

La contraseña de red especificada no es válida.
----

[WARNING]
====
Les últimes versions de samba deshabiliten el protocol NTLMv1 i per això obtenim un error desde WIndwos 10.

Cal afegir al fitxer *_smb.config_* a global:

*_ntlm auth = yes_*
====

[source, bash]
----
C:\Users\pep>net use k: \\172.16.0.2\firstshare /user:josepsamba
Se ha completado el comando correctamente.

----

[IMPORTANT]
====
No tinc clar si també fa falta modificar la següent directiva de seguretat local: (_secpol.msc_)

Directivas Locales -> Opciones de seguridas -> Seguridad de red: nivel de autenticación LAN Manager -> Enviar LM i NTLM: ....
====

=== Seguretat de les comparticions

==== valid users

Podem restringir els usuaris per compartició amb el paràmetre *_valid users_* al fitxer *_smb.conf_*.

[source]
----
valid users = sambacli1, sambacli2
----

==== invalid users

Tenim la possibilitat d'especificar una llista negra d'usuaris per _share_.

[source]
----
invalid users = sambacli3
----

==== read list

En un _share_ amb permisos d'escriptura podem determinar una llista d'usuaris amb només permisos de lectura.

[source]
----
[firstshare]
path = /srv/public
read only = No
guest ok = No
read list = sambacli1, sambacli3
----

==== write list

En un _share_ de només lectura es pot especificar una llista d'usuaris amb permisos d'escriptura.


[source]
----
[firstshare]
path = /srv/public
read only = Yes
guest ok = No
write list = sambacli1, sambacli3
----

==== allow hosts

Podem controlar l'accés als _shares_ a nivell d'adreça ip.

.Accés a dos hosts
[source]
----
allow hosts = 192.168.1.5, 192.168.1.40
----

.Accés a una subxarxa
[source]
----
allow hosts = 192.168.1.
----

.Amb màscara de xarxa
[source]
----
allow hosts = 10.0.0.0/255.0.0.0
----

.Afegint excepcions
[source]
----
hosts allow = 10. except 10.0.0.12
----

==== hosts deny

És la contrapartida a _allow hosts_, s'aplica la mateixa sintaxi.

==== hide unreadable

*_hide unreadable  = yes_* impedeix que els usuaris puguin veure els fitxers sobre els que no tenen permisos de lectura.

[source]
----
hide unreadable = yes
----

==== browsable

*_browsable = no_* amagarà els _shares_ a *_net view_* i a *_smbclient -L_*. No obstant els _shares_ segueixen sent accessibles si es coneix el seu nom.


