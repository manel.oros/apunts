== Llicències

Les llicències de programari són considerades com l'*acord d'ús o el contracte que es realitza entre el fabricant o autor d'un determinat programa amb la persona que el farà servir*. En aquest contracte o llicència es donen a conèixer una sèrie de termes que l'usuari ha de conèixer abans de comprar la llicència, ja que ha de seguir al peu de la lletra totes les clàusules i termes. 

Aquestes llicències regeixen diversos programes tant gratuïts com comercials i lliures, la diferència entre ells són les condicions que s'estableixen. *Per a un usuari poder descarregar, instal·lar, copiar, modificar o distribuir un programa requereix d'una llicència, en la qual s'estableixen certes condicions d'ús que pot o no prohibir la còpia, la modificació o distribució del programa*.

=== Termes generals que poden aplicar a les llicències

Llicències amb copyright::
És la llicència més utilitzada, especialment per empreses i autors de prestigi, i implica que *només el seu autor pot utilitzar, modificar i distribuir el seu contingut*. Si un tercer desitgés utilitzar-lo *és imprescindible l'autorització expressa de l'autor* per a aquest fi concret i, en molts casos, el pagament pel seu ús. 
+
[IMPORTANT]
====
*Qualsevol contingut disponible a Internet que no especifiqui un tipus de llicència, està automàticament protegit per Copyright, encara que poden utilitzar-se, amb limitacions, amb fins educatius o en notícies*.
====

Llicències amb copyleft::
És un tipus de llicència que ofereix la possibilitat d'usar, copiar i redistribuir una obra i les seves versions derivades *sense la necessitat de demanar permís al seu autor*. 
+
A més, *obliga a que la obra es comparteixi sota la mateixa llicència que l'original*.
+ 
En particular, perquè una llicència es consideri copyleft, és obligatori que juntament amb el programa es distribueixi el seu codi font.

Programari de domini públic::
És un programari que no requereix llicència. Els drets d’explotació són per a tota la humanitat, perquè pertany a tots per igual. És un programari que qualsevol pot fer servir sempre dins de la legalitat i fent referència a l’autor original. Aquest programari pot venir d’un autor que l’ha donat a la humanitat o els drets d’autor del qual han expirat.

Free software (o programari lliure)::
Fa referència a programari que *respecta la llibertat dels usuaris per fer el que vulguin amb el programari adquirit* (no obliga al fet que el programari sigui gratuït) i, per tant, una vegada obtingut el programari l'usuari pot usar-ho, copiar-ho, estudiar el seu codi font, canviar-ho i redistribuir-ho lliurement.
+
El programari lliure *sol estar disponible gratuïtament, no obstant això, no és obligatori que sigui així*, per tant, no cal associar programari lliure a "programari gratuït" (denominat usualment freeware), ja que, conservant el seu caràcter de lliure, pot ser distribuït comercialment ("programari comercial").
+
Anàlogament, el "programari gratis" o "gratuït" no té per què ser lliure.
+
D'acord amb la definició de la Free Software Foundation, un programari és «lliure» quan es garanteixen les següents «llibertats»:
+
[cols="1,5"]
|====
| Llibertat |	Descripció
| 0 |	La llibertat de fer servir el programa, amb qualsevol propòsit.
| 1 |	La llibertat d'estudiar com funciona el programa i modificar-lo, adaptant-lo a les teves necessitats.
| 2 |	La llibertat de distribuir còpies del programa.
| 3 |	La llibertat de millorar el programa i fer públiques aquestes millores per a tothom, de manera que tota la comunitat en surti beneficiada.
2+| Les llibertats 1 i 3 requereixen accés al codi font perquè estudiar i modificar el programari sense el seu codi font és molt poc viable. 
|====

Open Source (o codi obert)::
El programari de codi obert és el programari el codi del qual en codi font i altres drets que normalment són exclusius pels qui posseeixen els drets d'autor, són publicats sota una llicència de codi obert o formen part del domini públic.
+
En les llicències compatibles amb la Open Source Definition *el propietari dels drets d'autor permet als usuaris utilitzar, canviar i redistribuir el programari, a qualsevol, per a qualsevol propòsit, ja sigui en la seva forma modificada o en la seva forma original*.
+
La cultura del programari lliure no es queda únicament al programari, sinó que va més enllà. D'aquesta manera existeixen projectes de maquinari lliure, de música lliure, d'imatges lliures, etc.

=== Algunes llicències concretes

Llicència GNU GPL (GNU General Public License) o GPL::
Una de les més utilitzades és la Llicència Pública General de GNU. 
+
L'autor conserva els drets d'autor (copyright), i permet la redistribució i modificació sota termes dissenyats per assegurar-se que totes les versions modificades del programari romanen sota els termes més restrictius de la pròpia GNU GPL. 
+
*Això fa que sigui impossible crear un producte amb parts no llicenciades GPL, el conjunt ha de ser GPL*.
+
És a dir, la llicència GNU GPL *possibilita la modificació i redistribució del programari, però únicament sota aquesta mateixa llicència*. I afegeix que, si es reutilitza en un mateix programa codi "A" llicenciat sota llicència GNU GPL i codi "B" llicenciat sota un altre tipus de llicència lliure, el codi final "C", independentment de la quantitat i qualitat de cadascun dels codis "A" i "B", ha d'estar sota la llicència GNU GPL. És a dir, *si que posem una sola línia al nostre programa que s'hagi llançat com GPL, tot el nostre programa està obligat a ser llançat com GPL*.
+
A la pràctica això fa que les llicències de programari lliure es divideixin en *dos grans grups*:

* *aquelles que poden ser barrejades amb codi llicenciat sota GNU GPL* (i que inevitablement desapareixeran en el procés, en ser el codi resultant llicenciat sota GNU GPL) 
* i *les que no ho permeten* en incloure millores o altres requisits que no contemplen ni admeten la GNU GPL i que per tant no poden ser enllaçades ni barrejades amb codi governat per la llicència GNU GPL.
+
En el lloc web oficial de GNU hi ha una llista de llicències que compleixen les condicions imposades per la GNU GPL i unes altres que no.
+
Aproximadament el 60% del programari llicenciat com a programari lliure empra una llicència GPL.
+
*Existeix també una variant de GPL que no presenta aquest caràcter víric coneguda com LGPL*. (Lesser GPL).

[NOTE]
====
GNU, acrònim de GNU's Not Unix, és un sistema operatiu de tipus Unix desenvolupat pel projecte GNU sota la tutela de la Free Software Foundation.

Està format en la seva totalitat per software lliure en general copyleft.
====

Llicència BSD::
Anomenades així perquè s'utilitzen en gran quantitat de programari distribuït al costat dels sistemes operatius BSD. 
+
L'*autor manté la protecció de copyright únicament per a la renúncia de garantia i per requerir l'adequada atribució de l'autoria en treballs derivats*, però permet la lliure redistribució i modificació, fins i tot si aquests treballs tenen propietari.
+
Són llicències molt permissives, tant que *són fàcilment absorbides en ser barrejades amb la llicència GNU GPL amb els qui són compatibles*. 
+
Pot argumentar-se que aquesta llicència assegura “veritable” programari lliure, en el sentit que l'usuari té llibertat il·limitada pel que fa al programari, i que *pot decidir fins i tot redistribuir-ho com no lliure*.
+
Altres opinions estan orientades a destacar que aquest tipus de llicència no contribueix al desenvolupament de més programari lliure.

Llicències de l'estil MPL (Mozilla Public License)::
Aquesta llicència de Programari Lliure va ser la que va utilitzar l'empresa Netscape per alliberar el seu Netscape Communicator 4.0, que va ser utilitzat per crear el projecte Mozilla, que ha creat diversos programes molt usats avui dia com Firefox, Thunderbird, FileZilla, etc.
+
La llicència MPL evita l'efecte "viral" de la GPL (si uses codi llicenciat GPL, el teu desenvolupament final ha d'estar llicenciat GPL) i no és tant permissiva com les llicencies tipus BSD. 
+
Aquestes llicències són denominades de *copyleft feble*.

Apache License::
La llicència de programari apache va ser creada per distribuir Apache, el principal servidor de pàgines Web a nivell mundial. 
+
És una llicència de *programari lliure no copyleft* (no obliga a lliurar el codi font juntament amb el programa) i *no vírica*, és a dir, no obliga al fet que les modificacions realitzades al programa siguin distribuïdes com a Apache License, ni tan sols obliga al fet que siguin distribuïdes com a programari lliure. 
+
Això sí, aquesta llicència obliga a informar clarament que els productes estan basats en un programari original amb Apache License, i a publicitar els autors originals de l'obra.
+
Sobre aquesta llicència s'han alliberat per exemple Android.

Llicència MIT::
És una llicència lliure permissiva generada a l'institut tecnològic de Massachusetts.
+
Aquesta llicència permet reutilitzar software dins de software propietari i per tant software amb una llicencia MIT es pot integrar a software GNU però no a la inversa.

=== I pensant en sistemes operatius propietaris

Llicència mitjançant CLUF (contracte de llicència per a l’usuari final) o EULA (end user license agreement)::
Llicència per la qual la utilització del producte només està permesa a un únic usuari, en aquest cas el comprador. Aquesta llicència es troba en format paper en el mateix producte o en format electrònic. Tant el Word com l’Excel de Microsoft són exemples clars d’utilització d’aquesta llicència.

Llicències OEM (Original Equipment Manufacturer)::
Aquest tipus de llicències de software van lligades a un hardware concret, en general els software es proporcions junt amb la compra d'aquest hardware.
+
La particularitat d'aquest tipus de llicències és el que el software ve preparat per a aquest maquinari específicament, de manera que no en tenim realment una llicència d'ús, sinó una llicència d'ús únicament per a aquest maquinari en concret.
+
Aquestes llicències són les més econòmiques, i solen posseir restriccions especials, a part de venir sense manuals ni caixa.

Llicències retail::
És la llicència que comprem directament del desenvolupador. Som propietaris de la llicència, podem instar-ho en qualsevol tipus de maquinari compatible, podem revendre la llicència o cedir-la, etc.
+
Normalment solament permeten el seu ús en una sola maquina alhora. Vénen amb la seva caixa i manuals.
+
En les llicències de tipus retail, normalment podem triar entre una llicència completa, o una llicència d'actualització, que permet actualitzar un sistema anterior al nou, per un cost una mica més reduït.

Llicències VLM (llicencies per volum)::
Per a una empresa amb centenars d'ordinadors, és complicat controlar les llicències individuals de cadascuna de les seves màquines. Existeix la possibilitat de contractar un tipus de llicència especial amb el desenvolupador, de manera que, amb una única clau de llicència, podem utilitzar diverses màquines alhora. És habitual que existeixin llicències de 25 usos concurrents, 50, etc.
