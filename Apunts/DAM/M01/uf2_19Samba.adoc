== Samba

*Samba* és un programari obert i lliure que implementa el protocol *CIFS* (Common Internet File System) desenvolupat per Microsoft a finls dels anys '90. *CIFS* és un protocol que va evolucionar del protocol original *SMB* (Server Message Block) de IBM creat als anys '80. 

Samba va començar anomenant-se *_smbserver_*, però li van haver de canviar el nom a causa de problemes amb una altra empresa que tenia aquesta marca registrada. Per obtenir el nom nou van buscar una paraula que contingués les lletres SMB i d'aquí prové el nom samba.

Samba va ser desenvolupat per Andrew Tridgell el 1992. Com que en aquell moment no existia un document públic i lliure que indiqués com funcionava SMB, Tridgell va haver de deduir, mitjançant *enginyeria inversa*, el funcionament del protocol amb l’ajut d’un sniffer de xarxa.

=== NetBIOS

*Samba* va ser definit per treballar sobre **NetBIOS** (Network Basic Input/Output System), que és un protocol del **nivell de sessió** del model **OSI** que s’utilitza en xarxes locals i que s’encarrega de **garantir l’accés a serveis de xarxa entre màquines**, independentment del maquinari de xarxa que facin servir.

image::smb_osi.png[]

[TIP]
====
El nivell de *sessió* del model *OSI* és l'encarregat de mantenir i controlar l'enllaç entre dos nodes de xarxa mentre comuniquen i de recuperar la connexió en cas d'alguna interrupció sobtada.
====

Principalment s’utilitza per a **identificar amb un nom** els equips connectats per mitjà de xarxes locals, amb la finalitat d’**establir una sessió i mantenir la connexió** entre equips de la xarxa. NetBIOS **no pot transportar per si mateix les dades** entre els nodes. És per aquest motiu que ha de funcionar amb altres protocols de la capa de transport com, per exemple, TCP/IP, IPC/IPX o NetBEUI.

[WARNING]
====
Actualment, per configurar samba sobre Windows ja no és necessari *netBIOS*. Segueix sent-ho si es treballa amb versions antigues com Win 2000 o Win XP.
====

=== SMB vs Samba

El protocol Server Message Block (SMB) fou inventat per IBM, però
actualment la versió més comuna és la de Microsoft, modificada àmpliament.

L’any 1998, Microsoft va reanomenar aquest protocol com a *CIFS* (Common Internet File System) i hi va afegir més característiques.

L’SMB és un **protocol del nivell d’aplicació de tipus client - servidor** en el que l’ordinador que fa de servidor ofereix recursos (arxius,impressores, etc.) que els ordinadors clients poden utilitzar remotament mitjançant la xarxa. A més, segueix un disseny "**petició - resposta**", ja que les comunicacions sempre s’inicien des del client com una **petició** de servei cap al servidor. El servidor la processa i torna una **resposta** a aquest client. La resposta del servidor pot ser **positiva o negativa**, en funció del tipus de petició, la disponibilitat del recurs, els permisos del client, etc.

El protocol SMB (aplicació) s’implementa habitualment sobre el
protocol NetBIOS (sessió), que al seu torn s’implementa sobre el protocol TCP/IP (transport).

*Samba* és una *implementació lliure en Linux* del *protocol SMB sobre NetBIOS* amb les *extensions de Microsoft*, o sigui, les extensions del protocol *CFIS*. Utilitza i configura diferents directoris Unix / Linux com a recursos que es poden compartir per mitjà de la xarxa. Des de Windows, aquests recursos apareixen com carpetes comunes de xarxa. Cada directori pot tenir diferents permisos d'accés amb proteccions del sistema d'arxius que s'estigui fent servir en Linux.

=== Usos de Samba

Podem utilitzar Samba per:

* **Compartir impressores.**
* **Compartir** un o diversos **sistemes d'arxius** entre usuaris.
* **Autenticar clients**.
* Permetre a màquines que utilitzen Linux connectar-se a carpetes compartides per màquines Windows i compartir carpetes com si
 fossin Windows. Gràcies a Samba, en una xarxa podem tenir Windows i
Linux de manera que puguin intercanviar informació amb carpetes compartides.

=== Serveis de Samba

Essencialment, Samba està constituït per dos "daemons" o serveis anomenats *_smbd_* i *_nmbd_*. Aquests dimonis utilitzen el protocol NetBIOS per accedir a la xarxa, de manera que poden conversar amb ordinadors Windows.

==== smbd

EL servei *smbd* s’encarrega d’oferir els serveis d’**accés remot** a fitxers i impressores (per fer-ho, implementa el protocol **SMB**), a més d’**autenticar** i **autoritzar** **usuaris**.

El dimoni **smbd** ofereix les dues maneres de **compartició** de recursos a Windows:

* Compartició basada en usuaris (user-level).
* Compartició basada en recursos (share-level).

==== nmdb

El servei **nmbd** permet que el sistema GNU/Linux participi en els mecanismes de resolució de noms propis del Windows, la qual cosa inclou:

* L’anunci en el grup de treball.
* La gestió de la llista d’ordinadors del grup de treball.
* La resposta a peticions de resolució de noms.
* L’anunci dels recursos compartits.

=== Seguretat a Samba

Samba, a més dels nivells de seguretat que proporciona el protocol SMB (share i user), incorpora tres subnivells de seguretat en el nivell d’usuari. Així, en total podem utilitzar els nivells de seguretat següents:

share:: cada recurs compartit utilitza una contrasenya. Tothom qui la sàpiga pot accedir al recurs. Aquesta opció és en vies de desaparició.
user:: cada recurs compartit està configurat per permetre l’accés a certs grups d’usuaris. En cada connexió inicial al servidor Samba, s’autentica l’usuari.
server:: el sistema és idèntic a l’anterior, però s’utilitza un altre servidor per obtenir la informació dels usuaris.
domain:: Samba es converteix en membre d’un domini de Windows i utilitza un Controlador de domini per implementar l’**autenticació**. Un cop autenticat, l’usuari manté un testimoni (token) que conté la seva informació i a partir de la qual es podrà determinar a quins recursos té accés.
ADS:: Samba es comporta com a membre d’un domini de directori actiu i, per tant, requereix un servidor W2000 Server o millor.

=== Recursos addicionals

http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m4/instalacin_y_configuracin_de_samba.html[http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m4/instalacin_y_configuracin_de_samba.html]