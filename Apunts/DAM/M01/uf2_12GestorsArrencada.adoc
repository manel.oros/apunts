== Gestors d'arrancada

Ja sabem que el maquinari per si sol és incapaç de realitzar cap acció, necessita un programari que li indiqui què és el que ha de fer.

Quan encenem un sistema informàtic, estem engegant maquinari, per la qual cosa es necessiten mitjans especials per fer que es carregui un primer programari. 

Hem de contemplar dos process d'arrencada en funció de si estem treballant amb un sistema amb BIOS o amb un sistema amb UEFI.

=== Descripció general del procés d'arrencada BIOS / MBR

.Procés d'arrencada BIOS/MBR
[uml, file="images/uml/biosmbrboot1.png"]
....
scale 2.0
skinparam state {
    backgroundcolor lightgreen
}

state BIOS : * El PC s'engega 
state BIOS : * La BIOS inicialitza el hardware
state MBR : * La BIOS crida el codi \n emmagatzemat a el MBR \n al principi del disc 0.
state "Partició Activa" as PA: * El MBR carrega \n el codi del sector d'arrancada \n de la partició activa
state "Bootloader" as BL: * El sector d'arrencada \n carrega i excuta \n el bootloader del sistema de fitxers
state "Bootloader" as BL: * Es determina quin nucli càrregar

BIOS-> MBR 
MBR -> PA
PA --> BL
....


=== BIOS i CMOS

.BIOS (Basic Input/Output System)
image::bios.png[]

La BIOS (Basic Input/Output System) és un firmware gravat en memòria ROM (històricament no  es podia regravar) habitualment en tecnologia Flash actualitzable via software on *es troben guardades les primeres instruccions que executa el processador abans de carregar el SO*. És un firmware estretament relacionat amb els PC compatibles IBM.

.((Firmware))
****
Un *firmware* és un programa informàtic que estableix la lògica de més baix nivell que controla els circuits electrònics d'un dispositiu, és el software que té interacció directa amb el hardware.
****

*La seva funció principal és trobar el SO i carregar-lo a la memòria principal (RAM)*. 

Tot i que actualment les BIOS es poden actualitzar, en general s'utilitzen arrencar l'ordinador i, per tant, per LLEGIR el seu contingut.

La BIOS no requereix la *pila* per mantenir les seves instruccions. Si apaguem l'ordinador el codi de la BIOS es manté. La pila serveix per alimentar la memòria volàtil *((CMOS))* que *emmagatzema les variables que utilitzarà la BIOS durant l'arrencada* (data, hora, freqüència de bus, seqüència d'inici de dispositius, contrasenya de la BIOS, etc. De fet l'ultima configuració). 

Si traiem la pila, perdrem el contingut de la memòria CMOS, no el de la BIOS. 

Per esborrar el contingut de la CMOS (cosa que comporta omplir amb els valors per defecte els seus paràmetres) tenim a la nostra disposició tres formes de fer-ho:

. Traient la pila (com hem comentat ja abans).
. Seleccionant al menú l'opció LOAD SETUP DEFAULTS o similar, "Load Fail-Safe Defaults" o "Load Optimized Defaults"  depenent de la versió.
. Posant el jumper CLEAR CMOS a la posició d'esborrat (per fer això hem de consultar el nostre manual de la placa base).

.Detall de la pila i el jumper clear CMOS
image::clearcmos.jpg[]

==== Arrencada inicial. POST

En els ordinadors compatibles actuals el procés de càrrega d'un sistema operatiu qualsevol es compon d'una sèrie de passos que s'inicien quan s'encén o reinicia l'ordinador. 

El procés comença sempre a la BIOS, i salvant algunes petites variacions que pot haver-hi en funció de cada fabricant de maquinari i del propi BIOS, la seqüència pas a pas és la següent:

. Quan es dóna tensió a la font d'alimentació i una vegada que l'alimentació s'estabilitza, genera un senyal denominat "*Power Good*" en un dels cables que va de la font d'alimentació a la placa mare; aquest senyal és rebut en el joc de xips instal·lat a la placa, i és dispara un senyal de *reset al processador*. La finalitat d'aquest procés és evitar que el processador arranqui prematurament, quan les tensions d'alimentació no són encara correctes, la qual cosa podria produir danys en el maquinari. És el mateix sistema que s'utilitza per un reset en calent quan es prem en el botó de reset.
. *El processador arrenca quan es retira el senyal de reset*. En aquest moment no existeix en la seva memòria cap instrucció o dada, per la qual cosa no pot fer res. Per tant, els fabricants inclouen a la circuitería de la placa mare un mecanisme especial. 
+
El sistema es *dirigeix a una adreça fixa de memòria* (la FFFF0h en concret). Aquesta adreça, situada molt prop del final de la memòria del sistema en els primers ordinadors compatibles, *és el punt d'inici de la BIOS*. En realitat aquest punt d'inici *conté una instrucció de salt (jump) que indica al processador on ha de dirigir-se per trobar el punt on comença realment el programa de càrrega (BOOTSTRAP) de la BIOS*.
+
Aquest programa contingut en aquesta adreça es porta a la CPU i s'executa.
. La primera part d'aquest programa de la BIOS inicia un procés de *comprovació del maquinari* denominat POST (*Power On Self Test*), en cas d'existir errors greus, el programa es deté emetent una sèrie de xiulets footnote:[http://www.bioscentral.com/] que indiquen el tipus d'error.
+
L'*ordre de les comprovacions del POST* depèn del fabricant, però generalment la seqüència de comprovacions es resumeix de la següent manera:

.. Comprovació del *processador*.
.. Diverses comprovacions sobre la memòria *RAM*.
.. Inicialitzar els dispositius de *vídeo i teclat*. La comprovació del dispositiu de vídeo inclou carregar i executar la part de *BIOS inclosa en l'adaptador de vídeo*. La majoria de les targes modernes mostren en pantalla informació sobre si mateixes; és per aquesta raó que a vegades el primer que es veu en pantalla és informació sobre la pròpia controladora de vídeo abans que cap missatge de la BIOS del sistema.
.. Determinar la *grandària de la RAM* completa i *comprovar el seu funcionament* (el recompte que es veu en pantalla). Si arribat a aquest punt existís algun error en la memòria es mostraria un missatge d'error (el dispositiu de vídeo ja està operatiu, així que no fa falta emetre beeps).
.. Inicialitzar els *ports*: COM (comunicacions serie), LPT (comunicacions paral·lel), USB, S-SATA, SCSI, etc.
.. Inicialitzar, si escau, el sistema de disquet.
.. Inicialitzar el sistema IDE, S-SATA o SCSI. (Discos durs, CDROMS, etc.).
. A continuació, la BIOS recorre la memòria a la *recerca* de la possible existència d'altres programes en ROM per veure si algun d'ells té *una BIOS pròpia*, això passa, per exemple, amb les controladores de disc dur IDE/SATA; altres elements que solen comptar amb les seves pròpies BIOS són les targetes de xarxa i les controladores SCSI. Aquests mòduls són carregats i executats.
. A continuació, la BIOS mostra la seva *pantalla principal* (generalment amb els crèdits del fabricant, nombre de versió i data). I un resum de les proves realitzades per la BIOS.
+
En els PC originals la configuració del maquinari disponible s'efectuava mitjançant interruptors ("Jumpers") situats en la placa mare. Avui dia s'utilitza l'estàndard PnP (Plug and Play), que és capaç de detectar i configurar els dispositius connectats automàticament.
+
L'última instrucció del programa POST s'encarrega de buscar un altre programa que pugui ser carregat en el processador del PC perquè s'encarregui de seguir arrencant el sistema informàtic, normalment carregant ja un sistema operatiu.
+
*Però on buscarà el POST el programa a carregar? I en cas que existeixin diversos sistemes operatius en diversos suports, quin d'ells serà el triat?*

==== Sel·lecció i arrencada del sistema operatiu

En el punt en que ens trobem, el programa que està a la CPU és el POST, i ja ha realitzat la seva feina. Però si aquest programa simplement alliberés la CPU, l'equip es quedaria penjat ja que cap altre programari entraria en el microprocessador. Per això, l'última missió del POST és buscar un altre programa, i carregar-ho en la CPU abans d'alliberar-la.

En un sistema informàtic actual podem tenir múltiples discos durs, cadascun d'ells amb diverses particions on poden estar emmagatzemats diversos sistemes operatius, podem tenir un CD en la unitat lectora que també compti amb el seu propi sistema operatiu, podem tenir un disquet d'inici en la disquetera, podem tenir un petit sistema operatiu en un dispositiu USB, podem tenir un disc dur extern connectat per USB, etc...

*Com pot saber el POST a quin de tots aquests programes ha de cedir-li el control?*

De moment, a la BIOS de gairebé tots els equips moderns és possible trobar unes opcions que indiquen com és el suport d'informació des del qual es va a arrencar el sistema (*Boot*). En alguna opció d'aquest menú, normalment se'ns permet indicar l'ordre del diversos dispositius que utilitzarem per a l'arrencada.

Si el sistema operatiu s'executa des de disquet o CD, no hi ha massa problemes, atès que en un disquet o en un CD solament pot haver-hi un únic procés d'arrencada per a un únic sistema operatiu.

No obstant això, és possible que en disc dur tinguem diversos sistemes operatius per arrencar  en diverses particions. A més, podem tenir diversos discos durs en el nostre sistema, i en cada disc podem tenir diversos sistemes operatius instal·lats.

*Des de la BIOS podem indicar des de quin dispositiu volem arrencar però no podem indicar quin dels sistemes operatius instal·lats en aquest disc és el que volem iniciar.*

=== Descripció general del procés d'arrencada UEFI / GPT

.Procés d'arrencada EFI/GPT
[uml, file="images/uml/biosgptboot1.png"]
....
scale 2.0
skinparam state {
    backgroundcolor lightgreen
}

state EFI : * El PC s'engega 
state EFI : * Es carreguen drivers de hardware
state EFI : * Es sel·lecciona el disc d'arrancada
state "Partició EFI" as PE : * Cada sistema operatiu té el seu propi directori\n amb els fitxers per carregar-lo.

EFI-> PE
....

==== UEFI

.UEFI (Unified Extensible Firmware Interface)
image::uefi.jpg[]

La Interfície de Firmware Extensible Unificada, Unified Extensible Firmware Interface (UEFI), és una especificació que defineix una interfície entre el sistema operatiu i el firmware. 

UEFI reemplaça l'antiga interfície del Sistema Bàsic d'Entrada i Sortida (BIOS) estàndard present en els computadors personals IBM PC com a IBM PC ROM BIOS. 

Una de les avantatges principals de la interfície UEFI és que inclou bases de dades amb informació de la plataforma, inici i temps d'execució dels serveis disponibles llestos per carregar el sistema operatiu.

UEFI destaca principalment per::
* Només suporta sistemes de 64 bits.
* Compatibilitat i emulació del BIOS per als sistemes operatius només compatibles amb aquesta última.
* Suport complet per a la Taula de particions GUID (GPT), es poden crear fins a *128 particions per disc*, amb una capacitat total de 8 ZB.[1]
* Capacitat d'arrencada des d'unitats d'emmagatzematge grans, atès que no sofreixen de les limitacions del MBR.
* Independent de l'arquitectura i controladors de la CPU.
* Entorn amigable i flexible Pre-Sistema Operatiu, incloent capacitats de xarxa.
* Disseny modular i extensible.

=== Gestors d'arrancada

Un gestor d'arrencada (en inglés «bootloader») es un programa dissenyat específicament per preparar tot el que fa falta per arrencar el sistema operatiu.

GNU-Linux no té un gestor d'arrencada predeterminat com en el cas de Windows, sinó que pot utilitzar qualsevol gestor d'arrencada d'altres companyies.

Existeixen molts d'aquests gestors, el més usat fa un temps era *Lilo* que avui dia ha estat substituït per *GRUB* i encara més recentment pel *GRUB versió 2*.

=== Gestor d'arrencada de Windows

.Gestor d'arrencada de Windows
image::arranqueWin.jpg[]

Windows en general instal·la un dels gestors d'arrencada en el nostre sistema, no obstant això, *quan únicament tenim un sistema operatiu instal·lat aquest gestor no apareix en l'arrencada carregant automàticament el sistema*.

Si s'instal·lne diversos sistemes operatius Windows (per exemple Windows 7 i Windows 8) podem aprofitar perfectament un dels gestors d'arrencada de Windows per poder triar el sistema operatiu que volem arrencar per defecte en l'arrencada del sistema. 

La seva instal·lació i configuració és molt senzilla, l'única cosa que cal fer és assegurar-se d'instal·lar en primer lloc el sistema operatiu més antic i a continuació el més recent perquè hi hagi total compatibilitat entre ells.

==== Arrencada de Windows 7/vista/8/10

La seqüència d'arrencada de Windows va canviar a partir de XP. 

La principal diferència consisteix en que s'ha canviat el gestor d'arrencada, ja no s'usa el *ntldr* sinó que s'usa el Windows *Boot  Manager* o  (*bootmgr*).

ntldr::
Utilitza el fitxer *boot.ini* per configurar les seves opcions.
bootmgr::
Utilitza una base de dades anomenada *BCD* (Boot Configuration Data) *no editable directament*

Sequència d'arrencada amb *bootmgr*:

. Es carrega i executa el POST.
. Es carrega el MBR del disc dur (si és l'opció triada en la BIOS)
. Es carrega el sector d'arrencada de la partició primària activa
. Es carrega el programa *bootmgr*.
. *bootmgr* ajusta el processador per treballar a 32 bits o 64 bits.
. *bootmgr* llegeix la base de dades *BCD* i mostra un menú si és necessari.
. L'usuari selecciona un sistema operatiu del menú, o es carrega per defecte un d'ells.
. *bootmgr* carrega *winload.exe*.
. *Winload.exe* carrega *NTOSKRNL.EXE* (Nucli del sistema operatiu Windows)
. *NTOSKRNL.EXE* llegeix el registre de Windows, i procedeix a anar carregant el sistema complet.

[NOTE]
====
Windows disposa de la comanda *bcedit.exe* per configurar la BCD, és una comanda complicada d'utilitzar. 

Existeixen eines gràfiques de tercers, per exemple EasyBCD, que permeten modificar la base de dades d'una manera més sencilla.
====

==== Arrencada de Windows 8/10

Encara que l'arrencada de Windows actual és molt similar al de Windows 7 incorpora diverses novetats, moltes d'elles basades en l'ús de UEFI en lloc de BIOS. Una de les més importants és la del *((Secure Boot))*.

Els ordinadors quan trobaven el sector d'arrencada del SO que volien carregar, es limitaven a executar aquest codi, sense comprovar què era el que s'estava executant.

*Secure boot* és una caracterísitca dels sistemes UEFI que fa que el firmware dle sistema comprovi la signatura digital del sector d'arrencada, per comprovar si és d'un sistema reconegut, i si s'ha produït algun tipus de modificació sobre el mateix. 

Per permetre l'arrencada del sistema operatiu, s'han de donar una de les següents situacions:

* El codi de càrrega està signat utilitzant un certificat “de confiança”. Per exemple un certificat de Microsoft.
* L'usuari ha aprovat la signatura digital del codi de càrrega. La especificació UEFI diu que l'usuari hauria de poder relitzar aquesta acció. (No sempre ocorre).
* L'usuari deshabilita *Secure Boot* en la configuració de UEFI.
* L'usuari deshabilita totalment UEFI, i en el seu lloc utilitza BIOS.

A part del *secure boot*, que és una caracterísitca de UEFI, Windows suporta tres mesures adicionals de seguretat a l'arrencada del sistema.

Trusted Boot::
Una vegada que *secure boot* ha acabat les comprovacions, el codi de càrrega (bootloader) verifica el signat del kernel de Windows abans de carregar-l'ho. 

Early Launch Anti-Malware (ELAM)::
El kernel de Windows verifica tots els drivers de dispositiu de la pròpia Microsoft que es carreguen en l'arrencada. Si un fitxer ha estat modificat, el bootloader detecta el problema i es nega a seguir carregant el component. Windows  intenta reparar el component corrupte automàticament.

Measured Boot::
Tot el procés d'arrencada és auditat pel firmware, Windows pot enviar l'auditoria a un servidor confiable que pugui mesusrar la salut del PC.

=== Arrencada de Linux

Linux no compta amb un gestor d'arrencada pròpia, sinó que permet usar qualsevol gestor d'arrencada que desitgem.

El que se sol incloure actualment en totes les versions de Linux s'anomena *GRUB* (Grand Unified Bootloader)

GRUB és un gestor d'arrencada múltiple que s'usa comunment per iniciar dos o més sistemes operatius instal·lats en un mateix ordinador. Altres gestors d'arrencada usats anteriorment en Linux són *syslinux* i *lilo*.

En l'actualitat ens podem trobar amb GRUB en les seves versions 1 i 2.

El procés d'inici de *GRUB 1* és el següent:

. La BIOS busca un dispositiu d'inici (com el disc dur) i passa el control al MBR (Màster Boot Record), els primers 512 bytes del disc dur.
. El MBR conté la *fase 1 de GRUB*. Com que el MBR és petit (512 bytes), la fase 1 només s'encarrega de buscar i carregar la següent fase del GRUB (situat físicament en qualsevol part del disc dur). La fase 1 pot carregar ja sigui la fase 1.5 o directament la 2 
. La *fase 1.5 de GRUB* està situada en els següents 30 kilobytes del disc dur. La fase 1.5 càrrega la fase 2. Aquesta fase és optativa i normalment no s'usa.
. La *fase 2 de GRUB* (carregada per les fases 1 o 1.5) rep el control, i presenta a l'usuari el menú d'inici de GRUB. Aquest menú es configura mitjançant un fitxer de text amb nom *menu.lst*.
. GRUB *carrega el nucli seleccionat per l'usuari* a la memòria i li passa el control perquè carregui la resta del sistema operatiu.

[NOTE]
====
GRUB 2 substitueix el fitxer *_menu.lst_* (que editem manualment) per un procés modular, de manera que automàticament s'afegeixen els sistemes operatius i les opcions dels mateixos. 
====

[IMPORTANT]
====
GRUB no és en realitat un gestor d'arrencada per Linux, sinó un gestor d'arrencada per a qualsevol sistema operatiu. De fet, GRUB és perfectament capaç d'arrencar qualsevol sistema operatiu de la família Windows sense cap tipus de problemes.
====

==== LILO

.Gestor d'arancada LILO
image::lilo.png[]

Lilo (Linux Loader) és un gestor d'arrencada que permet triar el sistema operatiu a carregar al moment d'iniciar un equip amb més d'un sistema operatiu disponible. No és capaç únicament d'arrencar Linux, sinó que també pot arrencar altres sistemes operatius.

LILO funciona en una varietat de sistemes d'arxius i pot arrencar un sistema operatiu des del disc dur o des d'un disc flexible extern i permet seleccionar entre 16 imatges en l'arrencada.

En les primeres distribucions de Linux, LILO era el gestor de facto utilitzat per arrencar el sistema. En l'actualitat és una segona opció en favor del gestor d'arrencada GRUB.

L'arxiu _lilo.conf_ es localitza típicament en el directori _/etc_ i és la forma de configurar el gestor lilo. Dins de _lilo.conf_ es poden trobar dues seccions. 

* La primera secció, que defineix opcions globals, conté paràmetres que especifiquen atributs sobre la localització del carregador. 
* La segona secció conté paràmetres associats a les imatges del sistema operatiu que van a ser carregades.

.lilo.conf
----
boot = /dev/hda # la partició de la qual s'arrenca.
delay = 10 # temps durant el qual apareixerà el menú.
image = /boot/vmlinux # El fitxer amb el nucli de Linux.
root = /dev/hda1 # La partició on es munta l'arrel de l'arbre.
label = Linux # Nomeni que apareix en el menú.
read-only # opcions de muntatge.
other = /dev/hda4 # Altres sistemes operatius en el sistema.
label = windows # Nom que apareix en el menú.
----

==== GRUB (VERSIÓ 1)

.Gestor d'arrencada GRUB 1
image::grub.png[]

GRUB es carrega i s'executa en 4 passos:

. La primera etapa del carregador és molt petita i s'emmagatzema en el MBR del disc dur, des d'on és llegida per la BIOS.
. La primera etapa carrega la resta del carregador (segona etapa). Si la segona etapa està en un dispositiu gran, es carrega una etapa intermèdia (anomenada etapa 1.5), la qual conté codi extra que permet llegir cilindres majors que 1024. (*Aquesta fase ha deixat d'existir en la versió 2*)
. La segona etapa mostra el menú d'inici de GRUB. Aquí es permet triar un sistema operatiu juntament amb paràmetres del sistema.
. Quan es tria un sistema operatiu, GRUB càrrega a la CPU el principi d'aquest SO. (Aquest sistema operatiu pot ser un Linux i llavors carrega a la CPU el nucli de Linux, o bé un Windows, en aquest cas GRUB li cedeix el control al gestor d'arrencada de Windows).

GRUB suporta tres interfícies: 

* un menú de selecció
* un editor de configuració 
* una consola de línia de comandes.

GRUB no presenta el problema que presentava LILO de dependre exclusivament de la BIOS del sistema, però a canvi ha de ser capaç de treballar amb els sistemes de fitxers dels volums de dades. En particular, la versió 1 de GRUb no és capaç de treballar amb sitemes de fitxers _ext4_.

GRUB versió 1 es configura des del fitxer _menu.lst_, que sol estar emmagatzemat en _/boot/grub/menu.lst_. 

.menu.lst
----
default 0
timeout 15
#Debian Etch
title Debian GNU/Linux Etch, kernel 2.6.18-4-686 (on /dev/hdb1)
root (hd1,0)
kernel /boot/vmlinuz-2.6.18-4-686 root=/dev/hdb1 ro
initrd /boot/initrd.img-2.6.18-4-686
boot
#Microsoft Windows 7 on /dev/hda1
title Microsoft Windows XP Home Edition
root (hd0,0)
makeactive
chainloader +1
----

==== GRUB (VERSIÓ 2)

És una versió millorada del gestor GRUB1, actualment és el gestor per defecte de les noves distribucions Linux.

L'arxiu *_/boot/grub/grub.cfg_* reemplaça l'antic *_/boot/grub/menu.lst_* però a diferència d'aquest últim l'arxiu de configuració per a la nova versió és generat automàticament a partir de l'arxiu *_/etc/default/grub_* i els scripts situats en *_/etc/grub.d_*.

.GRUB en un disc particionat MBR
image::768px-GNU_GRUB_on_MBR_partitioned_hard_disk_drives.svg.png[]

.GRUB en un disc particionat GPT
image::GNU_GRUB_on_GPT_partitioned_hard_disk_drives.svg.png[]
=== Recuperació d'errors en l'arrencada

El més mínim problema que s'origini en el procés d'arrencada, farà impossible el funcionament del sistema operatiu.

Les zones que cal vigilar i conèixer com recuperar si és necessari són:

* El *MBR*
* El *sector d'arrencada de la partició primària activa* 
* El *programa gestor d'arrencada*.

Però, quins errors es poden produir a l'arrencada?

fallades de maquinari::
En usar un disc dur sempre existeix la possibilitat que es corrompin clústers del mateix. Normalment aquests errors no solen tenir massa importància, però si es dóna la casualitat que es corromp el primer clúster del disc dur, *que és on se situa el sector del MBR i el primer sector d'arrencada de la primera partició*, els sitemes operatius de la màquina no arrencaran.
+
Normalment en aquests casos el millor és *canviar el disc dur complet*, i *intentar recuperar la informació que existia en el disc dur amb algun programa de recuperació de dades*.

L'acció del malware::
Aquestes amenaces poden esborrar el MBR i els sectors d'arrencada.

L'usuari ha espatllat l'arrencada d'un sistema operatiu, probablement implement instal·lant un segon operatiu::
Cada sistema operatiu compta amb el seu propi programa per instal·lar en el MBR, el seu propi programa per instal·lar en el sector d'arrencada, i també compten amb el seu propi gestor d'arrencada.
+
Si s'instal·len més d'un sistema operatiu en un disc el bootloader que quedarà instal·lat serà el de l'ultim sistema operatiu instal·lat.
+
En general:

** *Grub és capaç d'arrencar qualsevol sistema operatiu*, per la qual cosa respectarà sempre (o almenys ho intentarà) qualsevol sistema operatiu que hi hagués en disc dur abans que s'instal·lés aquest gestor d'arrencada.
** Els *gestors d'arrencada de Windows mai respectaran a Linux*. De fet, el gestor d'arrencada de Windows solament és capaç d'arrencar automàticament a sistemes operatius Windows, és complicat aconseguir arrencar altres sistemes operatius no de Microsoft.
** Els gestors d'arrencada de Windows respecten als sistemes operatius Windows però solament als anteriors a a la versió instal·lada, si respecten o no les versions posteriros dependrà de si el gestor d'arrencada ha canviat o no entre versions. 
+
Per exemple, Windows 8 reconeix i respecta a Windows 7, però al contrari no.

=== Problemes d'arrencada amb UEFI i BIOS

UEFI és el firmware que ara mateix podem trobar en els PC comercials, és molt estrany trobar un equip que no ho suporti. 

El gran problema que ens podem trobar en l'arrencada, és que actualment podem configurar UEFI:

* En mode legacy o compatibilitat BIOS.
* En mode UEFI pur.

Un sistema operatiu que s'hagi instal·lat en mode UEFI no podrà ser carregat per un sistema que es passi a BIOS, i viceversa. Això fa que moltes vegades ens trobem amb sistemes que no arrenquen simplement perquè no s'ha escollit la versió correcta.

Un disc dur instal·lat per defecte sota UEFI tindrà una estructura com la següent:

.Particions predeterminades del disc UEFI
[ditaa, options="--scale 2.0",  file="images/uml/particionsUEFI.png"]
....
+--------------------------------------------------+
| Disc 0                                           |
|  +-------------+ +-----+ +--------------------+  |
|  | Partició de | | MSR | | Partició principal |  |
|  | sistema EFI | |     | |       WINDOWS      |  |
|  +-------------+ +-----+ +--------------------+  |
|                                                  |
+--------------------------------------------------+
....

Veiem com es crea de forma predeterminada una partició de sistema EFI, una partició reservada de Microsoft (MSR, Microsoft Reserved Partition) i una partició principal de Windows que és on s'instal·larà el nostre sistema operatiu.

[NOTE]
====
Una partició MSR només es crea en el cas d'instal·lar Windows en un disc EFI.
====

En una instal·lació per defecte BIOS el disc dur quedarà de la següent forma:

.Particions predeterminades del disc BIOS
[ditaa, options="--scale 2.0",  file="images/uml/particionsBIOS.png"]
....
+----------------------------------------+
| Disc 0                                 |
|  +-----------+ +--------------------+  |
|  | Partició  | | Partició principal |  |
|  | principal | |       WINDOWS      |  |
|  +-----------+ +--------------------+  |
|                                        |
+----------------------------------------+
....

////
==== Arrencada a mode de fallades

((TODO))
////

////
En punt anterior hem vist com podem solucionar les fallades de l'arrencada més importants, que comporten sobreescriure el *MBR o bé el sector d'arrencada. No obstant això existeixen molts altres tipus d'errors que es poden produir en l'inici del sistema operatiu, i que no es poden solucionar amb aquestes tècniques. Errors típics d'aquest tipus poden ser la instal·lació d'un *driver corrupte, l'esborrat accidental d'un fitxer del sistema, etc.
Quan un sistema no pot iniciar-se a causa d'un error d'aquest tipus, sempre podem intentar iniciar el sistema operatiu en una manera especial coneguda com a manera a prova de fallades, on es carregaran les funcions bàsiques del sistema, intentant saltar-se les parts que poden estar provocant fallades. Per ingressar en la manera a prova de fallades en un Windows, n'hi ha prou amb prémer la tecla F8 just quan el sistema inicia la seva càrrega.
Aquesta pantalla que veiem aquí per exemple, és la que s'obté si iniciem Windows 7 després d'haver-nos sortit del mateix d'una forma *descontrolada (apagant l'ordinador sense tancar el sistema, per exemple).

Si premem F8 quan el sistema s'està iniciant, no obstant això, Windows ens presenta la següent pantalla:
Veiem com a més de les maneres segures, permet arrencar el sistema amb altres configuracions establertes, com poden ser amb gràfics de baixa resolució. Aquest menú apareix d'una forma o una altra en totes les versions de Windows, encara que en Windows 8/10 cal activar-ho abans de poder usar-ho des del panell de control del propi Windows.
Per fer això en Windows 10 cal anar al menú de configuració de Windows (Windows + I).
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 32 I.S.O. Arrencada d'un sistema operatiu.
En aquest panell de configuració escollim Actualització i seguretat. Després Recuperació i Inici Avançat – Reiniciar ara.
Amb la qual cosa arribarem a la pantalla de recuperació / inicio avançat de Windows 10.
L'opció apagar l'equip ens permet realitzar un apagat total de l'equip, perquè després puguem arrencar-ho en “*frio”, la qual cosa ens permet entrar en el *Setup del *BIOS, etc. Si no apaguem l'equip així, realment no s'apaga del tot i després realitza una arrencada en “calenta”. Aquest tipus d'arrencada no comprova les pulsacions del teclat amb la qual cosa no podrem entrar en *BIOS.
L'opció que ens interessa en aquest moment és la de solucionar problemes.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 33 I.S.O. Arrencada d'un sistema operatiu.
Les opcions restaurar i restablir permeten reinstal·lar completament el sistema operatiu, bé sense perdre arxius o ben realitzant un formato i per tant perdent-ho tot. A nosaltres ara mateix ens interessa Opcions avançades.
Des d'aquí podem realitzar moltes coses (una d'elles és intentar automàticament reparar l'inici, encara que normalment no funciona). Les opcions que ens interessen són ben Símbol del Sistema, per poder accedir a la consola de recuperació i executar els comandos *Bootrec com ja vam veure anteriorment, o bé Configuració d'inici, que ens mostrarà el menú de la manera d'arrencada a prova de fallades.
En Linux no tenim una manera segura com a tal, però podem passar-li paràmetres al *kernel indicant com volem llançar el nostre Linux, desactivant per exemple els gràfics en alta resolució, el *multiusuario, els ports USB, etc. Ho veurem en el tema de Linux.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 34 I.S.O. Arrencada d'un sistema operatiu.
EVOLUCIÓ HISTORIA DELS SISTEMES OPERATIUS.
Al llarg de la història, han existit centenars de famílies de sistemes operatius, cadascuna d'elles composta per desenes de sistemes operatius diferents. És una història que comença en els anys 1960 i que segueix fins al dia d'avui.
Podria semblar que existeixen pocs sistemes operatius al mercat, però si investiguem una mica ens adonem que existeixen centenars d'alternatives possibles. És més, avui dia s'ha obert el mercat dels sistemes operatius per a mòbils i *tablets, on existeixen desenes de sistemes operatius nous que es van presentat cada any.
////