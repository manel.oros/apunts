== Sistemes de fitxers 

Un fitxer o arxiu (a efectes de sistemes d'informació són termes equivalents) és un mecanisme d'abstracció que serveix com a unitat lògica d'emmagatzematge d'informació. El fitxer agrupa una col·lecció d'informacions relacionades entre si i definides pel seu creador. A tot fitxer li correspon un nom únic que l'identifiqui entre els altres fitxers.

Sense un sistema de fitxers, un determinat medi d'emmagatzemament sería una gran successió de dades sense forma de coneixer on acaba una dada i on comença la següent.

Segons la RAE, un arxiu és un "_Conjunto de datos almacenados en la memoria de una computadora u otro dispositivo electrónico, que puede manejarse con una instrucción única._"

És necessari que el sistema operatiu compti amb un *sistema que s'encarregui d'administrar la informació emmagatzemada en els dispositius en forma de fitxers*, aquesta és la missió dels *sistemes de fitxers*.

El sistema operatiu ofereix una visió lògica i uniforme de l'emmagatzematge d'informació realitzant una abstracció de les propietats físiques dels seus dispositius d'emmagatzematge. Per a això, defineix el concepte lògic de fitxer. El *sistema operatiu s'ha d'encarregar de l'acoblament entre els fitxers i els dispositius físics*, per mitjà del sistema de fitxers, que ha de ser independent del suport físic concret sobre el qual es trobi.

En definitiva:

****
*Un sistema de fitxers s’encarrega d’estructurar, gestionar i administrar eficientment la informació guardada en una unitat d’emmagatzemament massiu de dades com pot ser un disc dur*. 
****

Un sistema d’arxius és un component essencial d’un sistema operatiu i acostuma a contenir:

Mètodes d’accés:: Relacionats amb la forma d’accedir a les dades emmagatzemades en els arxius.
Administració d’arxius:: Fent referència a la provisió de mecanismes per a que els arxius siguin emmagatzemats, referenciats, compartits i assegurats.
Administració d’emmagatzemament auxiliar:: Per a l’assignació de l’espai als arxius en els dispositius d’emmagatzemament secundari.
Integritat dels arxius:: Per tal de garantir la integritat de la informació dels fitxers o arxius.

La següent taula ens mostra una visió més general sobre els sistemes de fitxers o arxius, així com els sistemes operatius que les fan servir.

.Alguns sistemes de fitxers i el context d'utilització
[options="header, autowidth"]
|====
| Sistema Operatiu | Sistema d’arxius
| DOS - Win 95 | Fat 12 i 16
| Win 98 | FAT 16 i FAT 32
| WIN 2000 / XP | FAT 16 - FAT 32 - NTFS
| WIN VISTA / 7 / 10| NTFS
| Linux | Ext2, 3, 4, ReiserFS, Swap
| MAC OS | HFS
| Discs òptics | iso9660
|====

=== Característiques d'alguns sistemes de fitxers

A part de l'organització a baix nivell dels fitxers, *alguns* sistemes de fitxers també proporcionen altres característiques.

Algunes d'elles poden ser: 

Mida de clúster configurable:: Permet decidir la mida del clúster al moment d'establir el sistema de fitxers a la partició.
Journaling:: Permet treballar transaccionalment en les operacions d'accés a disc.
Enllaços:: Permet que diferents noms de fitxers apuntin al mateix contingut sense haver de duplicar les dades.
Quotes de disc:: Assignació d'espai de disc per usuari.
Volume shadow copy:: Pot mantenir versions històriques de fitxers i carpetes.
ACL (Llistes de control d’accés):: Assignació d'un *descriptor de seguretat* a cada fitxer que defineix el propietari i dues ACL indicant les accions permeses/no permeses i els usuaris/grups associats.
Reserva retardada:: Permet esperar a reservar els blocs de dades al disc just en el moment de la escriptura de dades, això permet reservar espais contigus de de disc més fàcilment reduint la fragmentació de dades.
Encriptació transparent:: Permet la encriptació nativa de fitxers al disc.
Compressió transparent d’arxius:: Permet la compressió nativa d'arxius preservant capacitat de disc.

.Journaling
****
El *((journaling))* o *sistema transaccional* és un mecanisme pel qual un sistema informàtic pot implementar transaccions. Es basa en portar un registre de diari en el qual s’emmagatzema la informació necessària per restablir les dades afectades per una transacció en el cas que aquesta falli. La majoria de sistemes d'arxius actuals implementen journaling.

El funcionament és el següent:

. Es bloquegen les estructures de dades afectades per una transacció per a que cap altre procés pugui modificar-les mentre dura la transacció.
. Es reserva un recurs per tal d’implementar el journal, per tal que si es para el sistema es pugui continuar el journal.
. Es realitza una a una les modificacions en l’estructura de dades, per a cadascuna:
.. S’apunta en el journal cóm desfer la modificació i s’assegura de que aquesta informació s’escrigui físicament en el disc.
.. Es realitza la modificació
. Si en qualsevol moment es vol cancel·lar la transacció, llavors es desfan els canvis un a un llegint-los i esborrant-los del journal.
. Si tot ha anat bé, s’esborra el journal i es desbloquen les estructures de dades afectades.
****

=== Alguns sistemes de fitxers concrets

==== FAT 32 (File Allocation Table)

Amb Windows 95-98 es va començar a introduir FAT32, que va ser la reposta per a superar el límit de grandaria de  FAT16. 

Amb un adreçament de 28 bits (els 4 bits més significatius estan reservats), la mida del clúster en FAT32 és d'entre 4KB i 32KB. Per tant, això permet la mida màxima de les particions oscil·la entre 1 TB i 8TB. Aquest resultat és purament teòric ja que recordeu que, per exemple, MBR imposa una mida màxima de partició de 2TB.

Algunes de les característiques de FAT32:

* Mida màxima dels fitxers: 4GB
* Nombre màxim de fitxers: 268.173.300 (clusters de 32K)
* Mida màxima del volum: 8 TB (clusters de 32K)
* Màxima profunditat de directoris: 60 nivells
* Compressió transparent: si

Utilitza tècniques d'<<assignacio_enllacada, assignació enllaçada>> per emmagatzemar els fitxers al disc.

==== NTFS (New Technology File system)

És un sistema d’arxius dissenyat específicament per a la família Windows NT (2000,2003,XP, vista, Server) amb l’objectiu de crear un sistema d’arxius eficient, robust i amb seguretat incorporada des de la seva base. També admet compressió nativa de fitxers i xifrat. 

NTFS permet definir la mida del clúster a partir de 512 bytes de forma independent a la mida de la partició. Es un sistema adient per a les particions de gran mida requerides en estacions de treball d’alt rendiment i servidors. Pot gestionar volums de fins a 16 Terabytes reals. 

Hi ha diferents versions de NTFS. El 1993 es va alliberar la primera versió. A partir de 1994 es va alliberar la v1.2 per Windows NT. Posteriorment, NTFS V3.0 per a Windows 2000 i la V3.1 per a Windows XP,2003,vista i 2008.

Algunes de les característiques de NTFS:

* Mida màxima dels fitxers: 16TiB
* Nombre màxim de fitxers: 2^32-1
* Mida màxima del volum: 16EiB
* Compressió transparent: si
* Xifrat transparent: si

Utilitza tècniques d'<<assignacio_indexada, assignació indexada>> per emmagatzemar els fitxers al disc.

==== EXT2 (Second Extended Filesystems)

És un sistema d’arxius pel Kernel de Linux. Va ser el sistema de fitxers per defecte de les distribucions de Linux Red Hat, Fedora core i Debian fins que es va canviar per EXT3.

Aquest sistema té un tipus de taula FAT de mida fixa on s’emmagatzemen els i-nodes. Aquest i-nodes són una versió molt millorada de FAT, on un apuntador i-node emmagatzema la informació del fitxer (path, mida i ubicació física), on la ubicació física consisteix en una referència a un sector del disc on es troben tots i cadascuna de les referències als blocs del fitxer fragmentat. Els límits són d’un màxim de 2 TB de fitxer i de 4 TB de partició.

Característiques Límit:

* Mida màxima del fitxer: 2TB
* Nombre màxim de fitxers: 10^18^
* Mida màxima del nom del fitxer: 255 caràcters
* Mida màxima de la partició: 4 TB
* Caràcters permesos en els noms dels fitxer: Tots menys Null i ‘/’

Utilitza tècniques d'<<assignacio_indexada, assignació indexada>> per emmagatzemar els fitxers al disc.

==== EXT3 (Third Extended Filesystems)

Evolució del sistema d'arxius EXT2 que es diferencia d'aquests  perquè disposa d’un registre de diari (journaling). 

És possible actualitzar de EXT2 a EXT3 sense pèrdua de les dades, sense formatar el disc i amb un consum mínim de CPU.

Característiques Límit:

* Mida màxima del fitxer 2TB
* Mida màxima del nom del fitxer 255 bytes
* Mida màxima de la partició 32 TB (En arquitectures que suporten blocs de 8KiB)
* Caràcters permesos en els noms dels fitxer Tots menys Null i ‘/’

Utilitza tècniques d'<<assignacio_indexada, assignació indexada>> per emmagatzemar els fitxers al disc.

==== EXT4 (Fourth Extended Filesystems)

Al 2006 es va crear EXT4 com a millora compatible de EXT3 que permet suportar volums de fins a 1024 PBytes i permetent un suport afegit anomenat Extent. 

Els Extent han estat introduïts per canviar l’esquema tradicional de blocs que es feia servir en ext2 i 3. i millorar  el rendiment al treballar amb fitxers de mides grans i eliminant pràcticament la fragmentació. 

Característiques Límit:

* Mida màxima del fitxer 16TB
* Mida màxima del nom del fitxer 256 bytes
* Mida màxima de la partició 1024 PiB (= 1 exbibyte)
* Caràcters permesos en els noms dels fitxer Tots menys Null i ‘/’

Utilitza tècniques d'<<assignacio_indexada, assignació indexada>> per emmagatzemar els fitxers al disc..

=== HPFS (High Performance File System)

Va ser creat específicament per al sistema operatiu OS/2 per millorar les limitacions de FAT. Permetia noms grans, metadades (dades que descriuen altres dades, índex per localitzar objectes) e informació de seguretat. Altre característica era que tenia la taula situada en la meitat de la partició, d’aquesta forma es reduïa el temps de d’accés.

==== ReiserFS (High Performance File System)

ReiserFS és un sistema d’arxius de propòsit general, dissenyat i implementat per un equip de la empresa Namesys. 

Actualment es suportat per Linux i existeixen plans de futur per incloure’l en altres sistemes operatius. També es suportat per Windows de forma no oficial, encara que de moment de forma inestable. 

A partir de la versió 2.4.1 del nucli de Linux, ReiserFS es va convertir en el primer sistema de fitxers amb journal en ser inclòs en el nucli estàndard.

També és el sistema d’arxius per defecte en diferents distribucions com SuSe (menys en la versió 10.2 que es Ext3), Knopixx, Xandros...

==== iso9660

Es tracta d'un estàndard tècnic internacional que defineix un sistema de fitxers per a discs òptics.

Utilitza tècniques d'<<assignacio_contigua, assignació contigua>> per emmagatzemar els fitxers al disc.

=== Clústers

Un clúster (o unitat d'assignació segons la terminologia de Microsoft) és un conjunt de sectors contigu que componen la unitat més petita d'emmagatzematge d'un disc. 

=== Tipus de fragmentació

En un sistema d'arxius existeixen 3 tipus de fragmentació: 

Fragmentació interna::
Es refereix a l'espai perdut en clústers assignats a fitxers que no estan completament plens. És un fenomen inevitable.
Fragmentació externa::
Es refereix a l'espai que perdem per no tenir un espai lliure contigu prou gran com per a guardar tot el fitxer allà. Aquest tipus de fragmentació és inevitable en certs moments, atès que la eliminació de fitxers sempre deixa espais lliures.
Fragmentació de dades::
Es refereix al fet que els clústers assignats a un fitxer estiguin contigus o no. Això té implicacions a nivell de rendiment però no al nombre de bytes que es tornen inútils com els altres dos tipus de fragmentació.

=== Estratègies d'emmagatzematge  de fitxers al disc

Essencialment voldrem emmagatzemar dos tipus d'estructures:

* Fitxers
** Nom
** Tipus
** Mida
** Posició
** Permisos d'accés
** Nombre de processos que utilitzen simultàniament el fitxer
** Data: de creació, d'últim accés, etc..
** Identificació: Procés creador, procés últim accés, etc...
* Directoris
** Mateixes metadades que un fitxer.
** Taula de continguts

[IMPORTANT]
====
Els sistemes de fitxers solen tractar els directoris com si fossin fitxers i per tant podem considerar que en un sistema de fitxers només tenim fitxers.
====

Per emmagatzemar fitxers al disc considerarem una sèrie d'estratègies diferents:

* Assignació contigua
* Assignació enllaçada
* Assignació indexada

Abans de veure com s'emmagatzemen els fitxers al disc val la pena comentar com es gestiona l'espai lliure.

==== Assignació contigua [[assignacio_contigua]]

El mètode d'assignació contigua funciona de manera que *cada fitxer ocupi un conjunt de clústers consecutius en el disc*. 

Amb l'assignació contigua, la situació d'un fitxer queda perfectament determinada per mitjà de:

* La adreça del seu primer clúster 
* La seva longitud. 

image::discassignaciocontigua.png[]

El problema d'aquesta estratègia apareix a l'hora de trobar espai per a un nou fitxer. Si el fitxer a crear ha de tenir _n_ clústers de longitud, caldrà localitzar n clústers consecutius a la llista de blocs lliures.

A més, al eliminar fitxers queden clústers sense assignar que possiblement siguin petits per emmagatzemar futurs fitxers, aquest problema es coneix amb el nom de *((fragmentació externa))* i és un problema solucionable mitjançant tècniques de compactació o defragmentació.

Aquesta estratègia, utilitzada fa anys pels disc durs, és factible per l'emmagatzematge en *CD-ROMs* i *DVD* on es pot saber, prèviament a la gravació, els fitxers i les seves longituds.

==== Assignació enllaçada [[assignacio_enllacada]]

En aquest cas es tracta de mantenir cada fitxer com una llista enllaçada de clústers de disc.

* O bé fent que els primers bits de cada clúster mantinguin un apuntador al següent clúster del fitxer.
+

////
[ditaa, options="--scale 2.0", file="images/uml/assignacioenllacada1.png"]
....
Disc dur
+------+------+------+------+------+
|00    |01    |02    |03    |04    |
|      |      |      |  32  |      |    Directori
|      |      |      |cGRE  |      |  +--------+----------+-------+
+------+------+------+------+------+  | Fitxer | Principi | Final |
|05    |06    |07    |08    |09    |  |  file  |    07    |   32  |
|      |      |  18  |      |      |  +--------+---+------+-------+
|      |      |cGRE  |      |      |
+------+------+------+------+------+       
|10    |11    |12    |13    |14    |
|      |  27  |      |      |      |
|      |cGRE  |      |      |      |
+------+------+------+------+------+
|15    |16    |17    |18    |19    |
|      |      |      |  11  |      |
|      |      |      |cGRE  |      |
+------+------+------+------+------+
|20    |21    |22    |23    |24    |
|      |      |      |      |      |
|      |      |      |      |      |
+------+------+------+------+------+
|25    |26    |27    |28    |29    |
|      |      |  03  |      |      |
|      |      |cGRE  |      |      |
+------+------+------+------+------+
|30    |31    |32    |33    |34    |
|      |      |  XX  |      |      |
|      |      |cGRE  |      |      |
+------+------+------+------+------+

                    +------+------+------+------+------+------+
El fitxer seria     |07    |18    |11    |27    |03    |32    |
------------->      |      |      |      |      |      |      |
                    |cGRE  |cGRE  |cGRE  |cGRE  |cGRE  |cGRE  |
                    +------+------+------+------+------+------+
....
////


image::uml/assignacioenllacada1.png[]

* O bé mantenint un vector a part, de longitud el nombre de clústers de la partició, on s'indica per a cada posició o bé zero,si el clúster esta buit o, bé una adreça de memòria apuntant al següent clúster del fitxer.
+
.El fitxer estaria als clústers 2,3,4,7,11

////

[ditaa, options="--scale 2.0", file="images/uml/assignacioenllacada2.png"]
....

                    +------+------+------+------+------+------+------+------+------+------+------+------+------+------+
Apuntador al proper |0     |0     |3     |4     |7     |0     |0     |11    |0     |0     |0     |X     |0     |0     |
                    |      |      |cGRE  |cGRE  |cGRE  |      |      |cGRE  |      |      |      |cGRE  |      |      |
                    |0     |1     |2     |3     |4     |5     |6     |7     |8     |9     |10    |11    |12    |13    |
Nº de clúster       +------+------+------+------+------+------+------+------+------+------+------+------+------+------+
....

////

image::uml/assignacioenllacada2.png[]

Aquesta darrera estratègia era la utilitzada per el sistema de fitxers FAT.

Millores::
* No es produeix fragmentació externa permament, però sí que es produeix *fragmentació de dades*.

Problemes::
* El principal desavantatge d'aquest mètode és que tota la taula ha d'estar en memòria durant tot el temps perquè funcioni. Amb un disc de 200 GB i una grandària de clúster d'1 KB, la taula necessita 200 milions d'entrades, una per a cadascun dels 200 milions de clústers de disc.
* No podem accedir directament a un clúster sense haver començat pel primer del fitxer.
* Si es malmet un punter en un bloc assignat a un fitxer, la resta del fitxer seria il·localizable, i hi hauria la possibilitat d'accedir a blocs no assignats o, pitjor encara, a blocs assignats a altres fitxers.

===== FAT (File Allocation Table)

És un sistema d’arxius desenvolupat per a MS DOS al 1987 i el més utilitzat fins a Windows Millenium. 

* Es caracteritza per què l’estat de cada unitat d’informació del dispositiu està reflexat en un catàleg anomenat “*File Allocation Table*”. Aquesta taula és l’índex del contingut del disc. 

* L'àrea de dades d'un volum està dividida en una sèrie de *clústers d'idèntica mida*. La mida dels clústers canvia en funció del tipus de sistema FAT utilitzat, en general van de 2KB a 32KB.

* Cada fitxer ocupa un o més d'aquests clústers en funció de la seva mida, és a dir, un fitxer està representat per una cadena de clústers *no necessàriament contigus*.

* Les diferents versions de FAT utilitzaven un nombre diferent de bits per l'adreçament dels clústers cosa que limitava la capacitat màxima utilitzable dels discs durs.

* Qualsevol error en aquesta taula podia convertir el disc en no utilitzable, per això estava duplicada. 

A mode d'exemple anem a veure l'estructura d'una partició FAT16.

===== FAT 16 (File Allocation Table)

Un volumfootnote:[Windows anomena les seves particions volums] FAT16 té la següent estructura:

.Estructura d'un volum de Windows en FAT16

////
[ditaa, options="--scale 2.0", file="images/uml/fat16volume.png"]
....
+-------------+-----------+-----------+-----------+--------------------+
|    Sector   | Taula FAT | Taula FAT | Directori | Altres directoris  |
| d'arrancada |           | duplicada |   arrel   | i tots els fitxers |
+-------------+-----------+-----------+-----------+--------------------+
....
////

image::uml/fat16volume.png[]


L'estructura d'una taula FAT16 és la següent:

image::exempleFAT16.png[]

image::exempleFAT16_2.png[]

* En FAT 16, *tenim 16 bits per adreçar clústers*, i per tant 2^16^ = 65.536 clústers com a màxim. Alguns dels quals representen situacions especials i no s'utilitzen per emmagatzemar dades.

* Això determina l'espai màxim adreçable, per exemple amb clústers de 256 bytes es podien aconseguir capacitats de 256 x 65536 = 16 MB o de 33 MB amb clústers de 512 bytes. 

* Per arribar a tenir discs de 2 GB el clúster havia de ser de 32 KB (32 * 1024 * 65536 = 2 GB), que és la mida màxima raonable per a cadascuna de les unitats d’assignació.

* El contingut d'una cel·la d'una taula FAT16 s'ha d'interpretar de la següent manera:
.. Cada cel·la és un valor de 16 bits emmagatzemat en format *little endian* (és a dir, el byte menys significatiu al principi)
.. El contingut d'una cel·la representa l'adreça del següent clúster del fitxer, sabent que FFFF vol dir que el clúster actual és l'últim clúster del fitxer.

==== Assignació indexada [[assignacio_indexada]]

Hem vist com l'assignació enllaçada resolia els problemes de la fragmentació externa i de la declaració de la grandària màxima del fitxer en el moment de la seva creació. També hem vist que no dona suport a l'accés directe per estar tots els blocs del fitxer (i els seus punters) dispersos per tot el disc.

L'assignació indexada resol aquests problemes reunint a tots els punters en un mateix lloc: *el clúster índex*.

A cada fitxer li correspon el seu propi clúster índex, que no és més que una *taula amb les adreces de tots els clústers que formen el fitxer*.

L'entrada d'un fitxer en el directori només necessita mantenir la direcció del bloc índex per a localitzar tots i cadascun dels seus clústers.

Ara sí que es permet l'accés directe ja que l'accés al bloc _i_ només exigeix emprar la _i-esima_ entrada del bloc índex. 

.Exemple de clúster índex
----
ID: 1234567
Clúster: 13
Clúster: 130
Clúster: 100
Clúster: 154
Clúster: -1
Clúster: -1
Clúster: -1
Clúster: -1
Clúster: -1
Clúster: -1
...
Clúster: -1
----

El procés de creació d'un fitxer implica inicialitzar tots els punters del seu bloc índex a -1 i a continuació anar apuntant els blocs que va usant.

Aquesta estratègia permet l'accés aleatori als fitxers però té alguns inconvenients:

Problemes amb els fitxers petits::
Cal tenir en compte que clústers índexs són també clústers de disc, i deixen d'estar disponibles per a emmagatzemar dades, així que perdem una mica de la capacitat del volum. Com que la majoria dels fitxers existents en un sistema són petits el malgastament d'espai pot ser considerable un clúster com a índex per a cada fitxer, encara que aquest només ocupi un o dos clústers.

Problemes amb els fitxers molt grans::

I suposem ara un fitxer realment gran, és tan gran que la seva llista de blocs usats no cap en un únic bloc índex. Com que un bloc índex és també un bloc de disc, podríem enllaçar varis d'aquests blocs índexs per a aconseguir-ne un de major. 
+
Aquesta filosofia empra una assignació enllaçada per als blocs índexs, la qual cosa podria no resultar convenient en termes d'accés directe. Com a alternativa, podríem pensar a emprar una estructura d'arbre binari.
+
Aquesta és la estratègia seguida pels sistemes de fitxers *_ext_* utilitzats a Linux.

==== Sistemes de fitxers Linux

Un cop definida una partició i establert un sistema de fitxers Linux, l'estructura de la partició és la seguent:

////
[ditaa, options="--scale 2.0, --no-separation , --no-shadows",  file="images/uml/extfilesystem.png"]
....
+-------------+-------+--------+------------+
|  Clústers   | Super | Inodes | Clúters de |
| d'arrencada | bloc  |        |   dades    |
+-------------+-------+--------+------------+
....
////

image::uml/extfilesystem.png[]

Bloc d'arrencada::
Pot contenir el programa d'arrencada per a carregar el sistema operatiu.
Super bloc::
Descriu l'estat del sistema de fitxers.
** Manté la mida total de la partició.
** Manté  la mida dels clústers.
** Manté una llista amb els clústers no assignats. (l'espai buit)
** Manté la llista dels inodes del directori arrel.
** Manté la llista dels inodes no utilitzats.
Inodes::
Estructura d'arbre amb inodes. Els inodes són el què hem anomenat al punt anterior *clúster index*.
+
.Estructura dels inodes ext4
image::inode1.jpg[] 
Clústers de dades::
Clústers amb el contingut dels fitxers.

Al arrencar el sistema, es munten els sistemes de fitxers tant locals com remots i es llegeix el _superbloc_ a la memòria.

Cada cop que des d'un procés s'accedeix a un fitxer *és necessari consultar al superbloc* i a les llistes de inodes. Per agilitzar aquest procés tant el superbloc com les taules de inodes es carreguen en memòria. 

Això genera problemes de sincronització, assegurar la consistència entre les dades del disc i les emmagatzemades en la memòria és responsabilitat d'un _daemon_ anomenant *_syncer_* que arrenca al iniciar el sistema i sincronitza les versions dels inodes i del superbloc entre el disc i la memòria. *Si la màquina no es para elegantment és possible que no s'hagin sincronitzat correctament les dades i el sistema resulti malmès*.


