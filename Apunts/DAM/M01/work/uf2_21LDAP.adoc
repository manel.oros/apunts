=== LDAP

==== Introducció 

LDAP és un protocol d’accés a directoris, que ens permet l’accés a diferents recursos per mitjà d’una base de dades. 

El més popular és l’Active Directory, que sota aquest nom es comparteix un esquema de LDAP v3 (i per tant són compatibles). 

Nosaltres utilitzarem la versió lliure de LDAP (OpenLDAP). 

==== Autenticació en Linux 

Hi ha diverses formes d’autenticació en Linux, però el més comú és utilitzar una combinació de PAM, NSS i LDAP. 

La idea original és utilitzar un servidor que faciliti l’autenticació de clients a la xarxa, de manera que el compte d’usuari no s’ha d’especificar en 
cada equip client, sinó que es pot validar a qualsevol lloc de la xarxa. 

===== NSS 

NSS és un servei que permet la resolució de noms d’usuari i contrasenyes. En condicions normals, aquesta informació es troba als arxius locals del sistema (etc/passwd, etc/shadow i etc/group), però pot venir d’altres llocs, com DNS, WINS, NIS o LDAP. 

L’objectiu de NSS és que els programes o les comandes del SO puguin manipular informació administrativa relacionada amb els usuaris, els passwords i els grups (incloent aspectes com la caducitat de passwords o la seva complexitat), sense tenir en compte on es troben emmagatzemats. 

===== PAM 

PAM estableix una interfície entre els programes i els diferents mètodes d’autentificació, fent que aquest mètode d’autentificació sigui transparent per als programes. Amb PAM podem, sense realitzar modificacions en el sistema, utilitzar mètodes d’identificació que vagin des del típic usuari i contrasenya, als tokens, tokens OTP, lectors de petjada digitals o de biometria. També podem restringir usuaris per horari entre d’altres. 

Bàsicament PAM complementa en alguns aspectes el funcionament de NSS. Mentre NSS es centra en mapejar usuaris, PAM controla l’autentificació, l’inici de sessió i la seva configuració. 

Actualment porten PAM Red Hat Linux, Caldera, Debian (des de 2.2). També FreeBSD des de 3.1. 

===== LDAP 

* LDAP es pot executar sobre TCP/IP o sobre qualsevol protocol orientat a connexió. 
* Es pot considerar un sistema d’emmagatzematge en xarxa (normalment construit sobre una BBDD). 
* La versió actual és la v3 (1997), però s’han afegit numeroses extensions que han incorporat noves capacitats. 
* OpenLDAP és un desenvolupament del protocol LDAP. Es va iniciar al 1998. 
* Els 2 estan orientats a estar optimitzats a situacions de lectura intensiva, doncs la majoria de consultes (peticions) a la base de dades seran de lectura. 
* Per aquest motiu si utilitzéssim un directori de LDAP per a guardar dades de manera freqüent, els resultats serien inferiors als que ens donaria una BBDD relacional (tipus SQL). 
* Al complir els estàndars de LDAP, pot ser compatible amb altres directoris com Active 
Directory de Windows. 

==== LDAP - funcionament

El model d’informació de LDAP es basa en entrades, que són un conjunt d’atributs identificats per un nom global únic (DN – Distinguished Name). 

Aquestes s’organitzen de forma lògica i jeràrquica mitjançant un esquema de directori (DIT), que conté una definició dels objectes que en poden formar part. 

* Cada entrada representa un objecte ,que pot ser abstracte o real (una persona, un moble, una funció…). 
* Cada atribut de l’entrada tindrà un tipus i un valor. Aquests atributs tenen noms que fan referència al seu contingut, i poden ser de 2 tipus: 
Atributs normals:: identifiquen al objecte (nom, cognoms…) 
Atributs operatius:: usats pel servidor per administrar-los (data de creació, 
mida…). 

Les entrades s’indexen per mitja del DN, amb les seves parelles atribut/valor (separats per comes), que reflecteixen la ruta inversa a la posició lògica del objecte fins a l’arrel del arbre. Per a referir-nos al nom complert, farem servir les sigles RDN (Relative Distinguished Name). 

Entre els atributs que es solen trobar, hi ha aquests (entre d’altres): 

----
dn: uid=jmanel, ou=smix, dc=domini, dc=local 
objectClass: person 
cn: Joan Manel Garcia 
givenname: Joan Manel 
sn: garcia 
o: domini 
u: asix
mail: jonamanelgarcia@gmail.com 
----

Com es pot interpretar, el nom d’usuari és jmanel, i està ubicat en un domini de tipus asix.domini.local (on el domini és domini.local i asix és un subdomini). També sabem que es tracta d’un objecte de tipus persona (“person”). 

L’arbre de directoris (DIT) podria ser el següent: 

[uml, file="images/uml/arbreLDAP.png"]
....
rectangle "Arbre directoris LDAP" as Tree #lightblue
rectangle "dc=com" as dc1 #limegreen
rectangle "dc=local" as dc2 #limegreen
rectangle "dc=es" as dc3 #limegreen
rectangle "dc=domini" as dc4 #darkorchid
rectangle "ou=asix" as ou1 #aquamarine
rectangle "ou=dam" as ou2 #aquamarine
rectangle "ou=daw" as ou3 #aquamarine
rectangle "jmanel" as u1 #orange

Tree -down- dc1 
Tree -down- dc2
Tree -down- dc3
dc2 -down- dc4
dc4 -down- ou1
dc4 -down- ou2
dc4 -down- ou3
ou1 -down- u1
....

Actualment, LDAP utilitza DNS per l’estructura superior dels directoris. En els nivells inferiors, les entrades representaran un altre tipus d’unitats organitzatives, usuaris o grups. 

Gracies al atribut especial anomenat *objectClass*, podem controlar quins atributs són vàlids i quins imprescindibles en una entrada, ja que aquests estableixen les regles que han de seguir els valors d’entrada. 

Lògicament, LDAP estableix operacions per consultar o modificar el directori per mitjà d’entrades. 

A més, LDAP inclou mecanismes de integritat i confidencialitat de les dades que emmagatzema. 

==== LDAP - instal·lació

Aspectes previs: 

LDAP sol treballar conjuntament amb NFS, especialment per tenir perfils mòbils. També hem de tenir present que hem de triar un nom de domini. És una bona idea utilitzar una nomenclatura tipus “servidor.domini.local” si el servidor no ha de ser web. El “.local” es fa per conveni. 

Cal tenir un servidor Linux configurat amb: 

* IP estàtica 
* /ect/hosts i /etc/hostname ben configurats
+
./etc/hosts
----
xx.xx.xx.xx server.domini.local server
----
+
./etc/hostname
----
server
----
* Instal·lem el paquet *_slapd_*, i també les utilitats *_ldap-utils_*. Durant el procés us demanaran una contrasenya per al administrador de LDAP. 
* Un cop fet, comprovarem que s’ha instal·lat correctament amb la comanda *_slapcat_*. El format que ens mostra s’anomena format LDIF. 

==== LDAP - estructura del directori

Un cop instal·lat, cal crear la estructura del directori, anomenada *DIT* (Directory Information Tree). Una de les formes més senzilles per afegir informació, és utilitzar arxius en format LDIF. En realitat són arxius de text pla amb un format que cal construir correctament: 

.Format d'un fitxer LDIF
----
# comentari 

dn: <nom global únic> 

<atribut>: <valor> #cn, objectClass… 

<atribut>: <valor> 

...Administració de directives de grup 
----
 
[IMPORTANT]
====
TOTES les entrades que realitzarem, les farem en format LDIF i les passarem al directori per mitjà de comandes de creació, modificació i esborrat d’entrades. 
====

Per exemple:

. Creem un arxiu base (p.ex: base.ldif) i l’editem amb nano. 
. La informació inicial serà la creació de les unitats organitzatives (ou) per als usuaris i per als grups. Ha de ser similar a: 
+
----
dn: ou=usuaris,dc=domini,dc=local 
objectClass: organizationalUnit 
ou: usuaris 

dn: ou=grups,dc=domini,dc=local 
objectClass: organizationalUnit 
ou: grups 
----
+
[NOTE]
====
Evidentment els valors “domini” i “local” cal que siguin els que en interessin. 
====
+
[IMPORTANT]
====
Respecta majúscules, minúscules i el format (després de donar valors als atributs, cal afegir la informació a sota). També cal que entre DN i DN deixem una línia en blanc. 
====
. Ara cal “afegir” la informació al nostre directori. Farem servir la comanda *_ldapadd_*. La comanda ha de ser similar a: 
+
----
sudo ldapadd -x -D cn=admin,dc=domini,dc=local -W -f base.ldif 
----

==== Creació d’usuaris

En aquest apartat ens centrarem en com afegir usuaris nous (més endavant ja veurem com modificar-los o eliminar-los).

. Crear un arxiu LDIF amb les dades del usuari.
. Afegim els atributs i els valors del compte d’usuari. 
. Cal que tinguem en compte que qui defineix aquests paràmetres, són els atributs objectClass.

Podeu trobar més informació dels objectClass en aquests enllaços:

http://www.zytrax.com/books/ldap/ape/[Llistat d’atributs comuns per objectClass, incloent els obligatoris i els opcionals]

http://www.zytrax.com/books/ldap/ape/inetorgperson.html[llistat de paràmetres del objectClass inetOrgPerson]

http://www.zytrax.com/books/ldap/ape/nis.html[llistat de paràmetres del objectClass Posix Account] 

L’arxiu *_usuari.ldif_*, ha de tenir un contingut similar al següent:

----
dn: uid=jmanel,ou=usuaris,dc=domini,dc=local
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
uid: jmanel
sn: Garcia
givenName: Joan Manel
cn: Joan Manel Garcia
displayName: Joan Manel
uidNumber: 2000
gidNumber: 10000
userPassword: elmeupassword123
gecos: Joan Manel
loginShell: /bin/bash
homeDirectory: /home/jmanel
shadowExpire: -1
shadowFlag: 0
shadowWarning: 7
shadowMin: 8
shadowMax: 999999
shadowLastChange: 10877
mail: jonamanelgarcia@gmail.com
postalCode: 08650
o: Copèrnic
initials: JM
----

[WARNING]
====
Aquest fitxer és molt sensible! Cal:

* Posar espaisentre atributs i valors.
* No hi pot haver espais al final de cada valor.
====

Observacions:

Hem utilitzat l’uidNumber 2000 i el gidNumber 10000. Ens hem d’assegurar que els uidNumber i gidNumberdel servei de directori de xarxa no coincideixin (se solapin) amb uid’s i 
gid’s que ja existeixin en màquines locals. 

Evidentment cal que, per a cada usuari creat, l’uidNumber, gidNumber, uid i homeDirectory siguin ÚNICS (i el passwordtambé si es vol). La resta de paràmetres poden coincidir sense problema.

Ara només cal afegir aquesta entrada. Ho farem amb la comanda *_ldapadd-* (ja la vam veure per afegir l’estructura de directori DIT):

----
sudo ldapadd -x -D cn=admin,dc=domini,dc=local -W -f usuari.ldif
----

Les opcions que apareixen serveixen per:

-x:: determina que utilitzarem autenticació simple (usuari i password)
-D:: determina el nom distintiu del usuari (admin)
-W:: determina que se’ns demani el password per a executar l’acció
-f:: determina l’arxiu que farem servir com a entrada (usuari.ldif)

Podem verificar que tot ha anat bé tornant a executar la comanda *_slapcat_*, on hauria d’aparèixer l’entrada feta:

La creació de grups és igualment senzilla o més, doncs no cal incloure tants paràmetres:

----
dn: cn=grupsmix,ou=grups,dc=domini,dc=local
objectClass: posixGroup
cn: grupsmix
gidNumber: 10000
----

I creem l’entrada següent, també amb *_ldapadd_* (les opcions són les mateixes que per afegir 
usuaris):

----
sudo ldapadd -x -D cn=admin,dc=domini,dc=local -W -f grup.ldif
----

==== Buscar, modificar i eliminar entrades de LDAP 

Fins ara hem vist com afegir entrades al LDAP, però és senzill equivocar-se i voler rectificar, o bé eliminar una entrada completament. Anteriorment ja hem vist com buscar una entrada (amb slapcat), però estaria bé tenir-ne una mica més d’informació concreta, sense haver de veure la BBDD sencera. 

Aquest apartat doncs, ens servirà per: 

* Buscar entrades (*_ldapsearch_*) 
* Modificar entrades (*_ldapmodify_*) 
* Esborrar entrades (*_ldapdelete_*) 

===== Buscar entrades

----
ldapsearch -xLLL -b dc=domini,dc=local uid=* sn givenName mail 
----

-x:: indica que farem servir autentificació simple. 
LLL:: serveix perquè la sortida sigui de tipus LDAPv1. 
-b:: va seguida del punt del arbre on ha de començar la cerca. En aquest cas, de dc=domini, dc=local. 

Després s’inclou la condició que hauran de complir els objectes buscats. En l’exemple, qualsevol valor (*) per l’atribut uid. 

Per últim, s’inclou el nom dels atributs que volem obtenir en el resultat de la consulta. 

[NOTE]
====
Aquest tipus d’estructura, té certa similaritat estructural amb les comandes de SQL.
====

===== Buscar entrades a LDAP 

En aquest exemple, hem fet sortir, de forma addicional, els uid i gid que vam donar anteriorment als usuaris “manel” i “ramon”. 

((TODO)) Captura

Com pots apreciar, el resultat està en format LDIF, cosa que facilitarà el fet de fer backups o transferir aquestes dades cap a qualsevol arxiu, tot mantenint una part de l’estructura. 

===== Modificar entrades a LDAP 

Caldrà crear un arxiu .ldif on avisem de quin camp modifiquem. L’estil és similar al següent: 

----
dn: uid=ramon,ou=usuaris,dc=domini,dc=local 
changetype: modify #important! 
replace: mail #important! 
mail: josep@prova.es 
----
 
Com has vist en l’apartat anterior, els correus que teníem per al usuari josep era prova@prova.es. Amb l’acció actual ho canviarem per josep@prova.es. 

El pas següent serà la comanda que utilitzarem (*_ldapmodify_*) per a canviar la entrada. 

----
ldapmodify -x -D cn=admin,dc=domini,dc=local -W -f canvis.ldif 
----

Com veiem, un cop entrada la modificació, podem cercar els canvis per comprovar que efectivament s’ha canviat el paràmetre que voliem modificar. 

===== Esborrar entrades a LDAP 

Aquesta acció és més senzilla ja que només requereix una sola comanda, similar a la de search però per a esborrar. Utilitzarem la comanda *_ldapdelete_*: 

----
ldapdelete -x -W -D 'cn=admin,dc=domini,dc=local' "uid=prova,ou=usuaris,dc=domini,dc=local“ 
----

Com s’aprecia, cal que li donem les dades de quin usuari realitza l’acció (admin) i de quina entrada volem esborrar (uid=ramon). El resultat és aquest: 

==== Importar usuaris i grups locals 

Hem vist el procés complert de creació, modificació i esborrat d’usuaris i grups, però tot això pot resultar farragós (sobretot el primer cop). 

Tenint en compte que és molt més senzill crear usuaris i grups locals amb Linux, podríem trobar alguna manera per traspassar aquestes dades i així estalviar-nos feina. 

Aquest procés el podem fer per mitjà d’scripts que recullin els paràmetres del arxiu _/etc/passwd_ del nostre servidor i els passin a un format comprensible per LDAP (format LDIF).

Teniu disponible aquest script al Moodle (script usuaris).

Mira de veure el codi utilitzat per entendre millor el què fa. A més, en algunes ocasions caldrà adaptar-lo al nostre SO (per exemple, l’script “importa” usuaris a partir del 1000, cal canviar el nom del domini...). 

Cal fer-hi un cop d’ull per veure per sobre com funciona. A més, caldrà adaptar alguns paràmetres als vostres interessos, com per exemple el nom de domini o el mail. 

Un cop fet, executa’l amb la comanda: 

----
sudo ./scriptusuaris.sh 
----

En cas que us doni algun error, cal revisar que: 

. Els usuaris locals disposin de totes les dades (common name, surname...), sinó donarà un error d’aquest tipus: 
((TODO))
. En cas de voler “debugar” possibles errors, el més senzill és posar les 3 últimes línies del script “scriptusuaris.sh” com a comentari, de manera que: 
.. No s’executarà la comanda ldapadd. 
.. No s’esborraran els arxius de text temporals. 
.. Al no esborrar-se els arxius de text temporals, podreu buscar possibles errors, com camps buits. A més, en cas que ens doni un error, podrem veure la línia que dóna problemes (en la captura anterior, la línia 21 contenia un camp buit). 
Si ha funcionat correctament, obtindrem un resultat com aquest: 

((TODO))

Ara comprovem que és correcte. Com veiem, s’han importat els usuaris: 

((TODO))

Per importar grups, el procediment és el mateix. Al Moodle hi ha un script que fa la funció d’importació de grups (scriptgrups.sh). 

El mètode a seguir és el mateix, i els errors que pot donar també. 

==== Equips clients de LDAP

Hem finalitzat la instal·lació i configuració del servidor Linux per a treballar amb OpenLDAP, però ara ens cal “adaptar” al client a la nova situació. Per fer-ho, ens caldrà modificar la forma en què s’autentifica, i canviar-ho de local a remot.

[IMPORTANT]
====
Abans de configurar el PC client (Debian), assegura’t que la seva xarxa està ben configurada. Concretament cal que tingui connexió amb el servidor Linux on hem instal·lat el LDAP i també amb internet (caldrà descarregar alguns programes). Modificar-ho a posteriori serà més complicat!!!
====

Amb Ubuntu, Lubuntu, Debian..., necessitem ajustar aquest comportament dels serveis NSS i PAM en cada client. Cal instal·lar aquests paquets:

libnss-ldap:: Permet que NSS obtingui la informació administrativa dels usuaris del LDAP (informació de comptes, grups, màquina, àlias…)

libpam-ldap:: Facilitarà l’autentificació amb LDAP als usuaris que utilitin PAM.

ldap-utils:: Facilita la interacció de LDAP des de qualsevol màquina de la xarxa.

===== Configuració d’equip client de LDAP

Durant la instal·lació ens demanaran algunes dades que cal canviar. 

Cal que feu algunes accions: 

*  Ens sortirà “ldapi://”, cal canviar-ho per “ldap://x.x.x.x”, on x.x.x.x és la IP del servidor (alerta!!! ldap sense la “i”!).
* Cal posar-hi els “dc” del nostre domini.
* Diem que si quan ens demani “make local root database admin”, i diem que no quan ens demani si la BBDD necessita 
login.
* Ens demanarà quin compte fem servir que tingui privilegis per realitzar canvis en les contrasenyes (cn). Durant la instal·lació vam utilitzar “admin” (per defecte), cal que ara també ho fem servir, juntament amb els dc del nostre domini.
* Per acabar, ens demanarà el password del compte anterior (admin), almenys un parell de vegades..
* En cas d’equivocar-nos en alguna cosa, podem reconfigurar aquesta part amb la comanda:

----
dpkg-reconfigure lib-nss-ldap libpam-ldap ldap-utils
----

Un cop instal·lat el software, caldrà fer alguns canvis a alguns arxius de Linux, bàsicament perquè fer que l’autentificació dels usuaris la faci el servidor Linux i no pas el propi PC (en local).

Cal doncs, canviar els paràmetres d’aquests arxius:

----
/etc/nsswitch.conf
/etc/pam.d/common-password
/etc/pam.d/common-session
----


/etc/nsswitch.conf::
En aquest arxiu s’inclouen les fonts des de les quals s’obté la informació del servei de noms de diferents categories i en quin ordre. Cada categoria és identificada per un nom.
+
L’arxiu és en format de text pla (com és habitual). Trobarem 3 columnes: la primera indica l’emmagatzematge i les restants l'origen on consultar i un conjunt limitat d’accions que es poden realitzar. En el nostre cas, cal identificar les línies amb el contingut “passwd, group i shadow”, cal afegir el text “ldap” per indicar el 
nou origen per autentificar els comptes de passwords, grups i usuaris.

((TODO)) Captura

/etc/pam.d/common-password::
En aquest arxiu només cal localitzar la línia número 26 i treure (esborrar) la opció “use_authok”, tal com s’apreciaa la captura:

((TODO)) Captura

/etc/pam.d/common-session::
Aquest arxiu ofereix un conjunt de regles de PAM per al inici de sessió. Aquí serà on indicarem que s’ha de crear un directori home durant el primer inici de sessió, també per als usuaris autentificats mitjant LDAP.
+
Cal afegir aquesta línia al final:
+
----
session optional pam_mkhomedir.so skel=/etc/skel umask=077
----

===== Habilitar daemon libnss-ldap

Ara caldrà reiniciar l’equip. Al inciar-lo, cal que ho feu en mode text.

Per arrancar en mode text, podeu fer-ho amb el Grub, i si no el teniu configurat, sempre podeu esperar a que us demani l’usuari (en mode gràfic) i clicar *alt+ctrl+f1*.

Un cop fet, entreu amb un usuari del servidor LDAP, especificant usuari i password.

El PC client intentarà buscar l’usuari a la seva base de dades local (etc/passwd). Si no el troba, ho buscarà per LDAP (així és com li hem dit que ho faci al arxiu de nsswitch.conf anteriorment). 

===== Entrar amb un usuari remot

Si per algun motiu no pots entrar, assegura’t que:

* Has configurat correctament els paquets (pàgina 3 d’aquest document). En cas de detectar una errada, sempre el pots reconfigurar. Si demano qualsevol cosa sobre encriptació, cal triar md5.
* Tens connexió amb el servidor (el ping retorna).
* No funciona? Prova a entrar com a usuari local (has de poder entrar sense problema). Un cop fet, revisa l’arxiu _/var/log/auth.log_. En aquest log se’ns mostren detalls de la connexió i l’autentificació, i ens pot donar alguna pista sobre què pot estar passant.

Algunes apreciacions...

Un cop fet això, pots pensar que “tant enrenou per res”. Aparentment el què hem fet sembla poc important, però en una empresa ésmolt rellevant.

Cal que aquests punts quedin clars:

* Qui ens ha autentificat l’usuari ha estat el servidor i no el nostre PC local.
* Des de un usuari remot no podem realitzar accions de root (de moment).
* La carpeta creada és en local i no al servidor. Tindria més sentit que fós al servidor (si, és així, i ja veurem més endavant com canviar-ho).
* La carpeta d’usuari creada a la /home, no està sincronitzada amb el servidor.

==== Servidor LDAP amb GUI 

Si, segurament tens ganes de matar al professor per obligar-te a treballar amb CLI fins ara. 
Era necessari per entendre millor com funciona el directori LDAP. En aquest apartat instal·larem i configurarem el LAM (LDAP Account Manager) per veure’n les seves característiques. 

Si estàs pensant en instal·lar un GUI al servidor, de moment no ho farem. El LDAP Account Manager requereix alguns components del LAMP (com el servidor Apache), que s’instal·len amb el propi paquet, de manera que podem accedir remotament (des del PC Lubuntu per exemple) a través del navegador web. 

Com bé saps, el paquet LAMP inclou un servidor web (Apache), una base de dades (MySQL) i el llenguatge de programació PHP. 

* Compatibilitat amb qualsevol servidor web que suporti PHP (mínim v4). 
* Compatible amb navegadors amb suport a CSS. 
* Compatible amb LDAP v2 i posterior. 
* Es pot xifrar la connexió amb SSL. 
* Es pot exportar la informació dels comptes en format PDF. 
* Es poden crear nous comptes a partir d’arxius de text. 
* Versió amb espanyol. 

El LAM ens permet a més: 

* Editar les entrades a partir de plantilles. 
* Copies d’entrades d’un lloc a un altre (entre servidors) 
* Capacitat de copiar arbres (DIT) de forma recursiva. 
* Esborrar entrades individuals o arbres complerts. 
* Cerques senzilles o avançades del directori. 

Per instal·lar-lo al servidor Linux: 

[source, bash]
----
sudo apt-get install ldap-account-manager 
----
 
Un cop fet només cal reiniciar, i llavors des de qualsevol PC client, podrem accedir via web, des de l’adreça http://IPdelservidor/lam: 

---- 
sudo apt-install lam
----

I ara? Després de reiniciar, has de poder accedir al LAM (http://IP_del_servidor/lam): 

===== Configurar LAM 

. Selecciona l’idioma (espanyol), i ves a “LAM Configuration” (a dalt a l’esquerre). 
. Clica a “edita la configuració general”. 
. Et demanarà un password. Per defecte és “lam”, però cal que el canviïs el més aviat possible!!! 
. Has de veure la configuració general. Hi ha d’haver 4 apartats: 
.. Preferències de seguretat (temps de connexions actives, filtres d’IP i certificats SSL). 
.. Política de contrasenyes (no afecta a la contrasenya mestra) 
.. Inici de sessió 
.. Canvi de contrasenya mestra 
. Canvia la contrasenya mestra. 

Ara caldrà editar els perfils del servidor. Torna a la pestanya “Configuración de LAM” i ves a “Editar perfiles del servidor” (si demana password, cal posar “lam” de nou, ja que el canvi de contrasenya mestra només té efecte després de reiniciar). 

Arribem a configuració de perfils, que ens permet: 

* Configuració general (informació global del servidor, característiques de seguretat...). * Tipus de comptes (d’administrador, d’usuaris, grups o equips). 
* Mòduls (comptes de Unix, Samba...) 
* Preferències del mòdul (conté informació del mòdul triat en l’apartat 
anterior). 

En el nostre cas ens centrarem en les dues primeres, que són les que explicarem 
a continuació. 


Apartat Preferències del servidor: 

Direcció del servidor:: Com que ja estem al servidor que ens toca, ho deixarem com està: ldap://localhost:389. 
Activar TLS:: Ho deixem com està (a “no”). TLS és una capa de seguretat pel transport 
d’informació. 
Sufix del arbre:: LAM inclou un navegador per LDAP. Si volem fer modificacions de forma directa, haurem d’incloure el sufix del nostre arbre. Seguint l’exemple que hem utilitzat fins ara, hauríem d’anotar dc=domini,dc=local (en la vostra pràctica evidentment poseu les vostres dades). 
Límit de cerca LDAP:: permet reduir els resultats de cerca si tenim un directori molt extens. En el nostre cas estarà desactivat. 

Apartat Preferències de lamdaemon: 

LAM només pot utilitzar carpetes personals (home) i quotes mitjançant scripts externs. Aquest apartat, ens permet indicar quin és el servidor per a les carpetes personals i on es troba l’script que administra les quotes. També permet establir permisos de les carpetes personals. De moment ho deixarem tal i com està. 

Apartat Configuració de les eines: 

Editor PDF:: per exportar informació dels comptes en PDF. 
Editor OU:: Ens permet editar les nostres unitats organitzatives (ou) del arbre LDAP. 
Informació del servidor:: mostra informació i estadístiques del servidor. 
Comprovacions:: verifica si l’esquema LDAP és compatible amb LAM. 
Explorador d’esquemes:: permet examinar l’esquema, obtenint tipus de classes, atributs, sintaxis i regles. 
Editor de perfils:: conté plantilles pels comptes. 
Multiedit:: modificació per lots de grans números d’entrades LDAP. 
Enviar arxius:: per crear comptes des del editor de textos, en format CVS. 

De moment ho deixarem tot en blanc. 

Apartat Preferències de seguretat: 

Hi trobem 2 mètodes per iniciar sessió: llista fixa o cerca LDAP. 

Llista fixa:: si ho triem, haurem d’especificar un (o varis) usuaris. Per a cadascun d’ells, triarem el nom global únic (DN). En el nostre cas, seria cn=admin,dc=domini,dc=local. 

Cerca LDAP:: si ho triem, fem que el LAM busqui un DN del directori a partir del nom d’usuari. 

En el nostre cas triarem la primera opció. Per acabar, caldrà que canviem el password d’aquest perfil del servidor (el que haguem triat de la “llista fixa” d’aquí sobre). 

===== Configurar LAM (account types) 

LAM admet diferents tipus d’entrades LDAP, i en aquest apartat tenim la oportunitat d’identificar les que volem administrar nosaltres en particular. A la pestanya “tipus de comptes” (“account types”) hi ha dues àrees: 

* Tipus de comptes disponibles 
* Tipus de comptes actius 

Apartat Tipus de comptes disponibles: 

Mostra una llista dels possibles tipus de comptes. Podem activar qualsevol d’ells fent clic en el signe “+” que hi ha al seu costat. 

Apartat Tipus de comptes actius: 

Conté els comptes que es troben vigents al nostre sistema. En cadascun d’ells podrem configurar diferents opcions, tot i que ens limitarem a les bàsiques: 

Sufix LDAP:: com sempre, han de contenir el sufix LDAP per a cada entrada d’aquest tipus 
Arbre del llistat:: Representa la llista d’atributs que es mostraran en el llistat de comptes. 

===== Configurar LAM (account types) 

Ara cal que escrivim el sufix per a cadascun dels tipus de comptes que utilitzarem. En el nostre cas serien els següents (el “ou” d’equips no l'havíem creat, al iniciar el LAM potser ens dirà si volem crear-lo de nou): 

* Per al tipus de compte Usuaris -> ou=usuaris,dc=domini,dc=local 
* Per al tipus de compte Grups -> ou=grups,dc=domini,dc=local 
* Per al tipus de compte Equip -> ou=equips,dc=domini,dc=local 


 (alerta, aquest no el vam crear en format text, i es crearà més endavant!) 

* Per al tipus de compte de Dominis de samba -> dc=domini,dc=local 

L’exemple anterior serviria per crear els OU de nou (en el nostre cas ja ha d’estar fet, ho vam fer en format text, a excepció del “equips” que l’hem creat ara). 

Un cop acabat guardem. En principi ja hem acabat la configuració inicial. A partir d’ara, quan entrem al LAM, veurem que el compte inicial ha canviat (ja no és manager, ara és admin), i podrem entrar a “remenar” usuaris, grups i altres. 

===== Configurar LAM (inici de sessió) 

Al iniciar per primer cop, si hem omplert les caselles de “account types”, ens demanaran si volem crear els “ou” que hi havia i que no troba. Li diem que si, ja que si no hi són s’hauran de crear: 
