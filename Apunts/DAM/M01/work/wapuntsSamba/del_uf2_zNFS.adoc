== NFS

Es tracta d'un protocol, implementat el 1984 per l'empresa Sun Microsystems, que s'utilitza en xarxes d'àrea local.

Sun Microsystems va desenvolupar NFS com un estàndard obert i el va incloure un RFC perquè qualsevol pogués implementar-lo. footnote[NFSv3 (RFC 1813), NFSv4 (RFC 3530)]

NFS (Network File System) és el sistema natiu utilitzat per Linux per compartir carpetes en una xarxa. Mitjançant NFS, un servidor pot compartir les seves carpetes a la xarxa. Des dels PCs dels usuaris es pot accedir a aquestes carpetes compartides i el resultat és el mateix que si estiguessin en el seu propi disc dur.

Bàsicament, NFS permet a PCs que utilitzen Linux, compartir i connectar-se a carpetes compartides entre si. 

Existeixen altres alternatives per compartir carpetes en una xarxa com **samba**, **ssh** o **ftp**, però el **sistema recomanat** per compartir carpetes entre sistemes Linux és NFS.

=== Instal·lació

La instal·lació de NFS es divideix en dues parts:

* Un equip que actua com a servidor i que emmagatzema els arxius compartits.
* Un o diversos equips que actuen com a clients, mitjançant els quals els usuaris accediran als arxius compartits pel servidor com si fossin locals.

Actualment, el protocol NFS està inclòs en la majoria de les distribucions Linux, i en les diferents versions de l’OS d'Apple. 

A Microsoft, és una mica més complicat: abans calia instal·lar el paquet Windows Services for UNIX. A partir de Windows 8 ja s'inclou de fàbrica, però només en la versió Enterprise edition. Una solució per salvar les incompatibilitats és recórrer a programari de tercers.

=== Característiques

* En facilitar l'accés centralitzat a la informació, s'evita la duplicitat en diferents punts de la xarxa,  forcem que totes les operacions d'escriptura relacionades amb una actualització concloguin abans de continuar (inclosa l'actualització de l'estructura de directoris). Així s'assegura la integritat de les dades.
* Permet emmagatzemar tot el perfil dels usuaris al servidor, de manera que qualsevol usuari podrà accedir a les seves dades des de qualsevol lloc de la xarxa.
* Permet compartir dispositius d'emmagatzematge sencers (unitats òptiques, discs externs, memòries flash, etc).
* Des de la versió 4, s'inclouen potents característiques de seguretat i Llistes de Control d'Accés (ACL - Access Control List), entre d'altres.

[NOTE]
====
Si tenim que interconectar entre sí per compartir fitxers màquines UNIX, tenim un sistema natiu per fer-ho com és NFS. Si hi ha màquines Windows implicades, poden fer servir SAMBA. De totes formes, no són sistemes excloents.
====
