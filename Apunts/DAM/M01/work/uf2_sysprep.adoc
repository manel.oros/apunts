Running sysprep to prepare and shut down your master
server
Now that our master server is prepped how we want, it is time to run the sysprep tool itself.
To do that, open up an administrative command prompt and browse to
C:\Windows\System32\Sysprep. Now you can make use of the Sysprep.exe utility inside that
folder in order to launch Sysprep itself.
As with many executables that you run from command prompt, there are a variety of optional
switches that you can tag onto the end of your command in order to make it do specific tasks.
From your command prompt window, if you simply run the sysprep.exe command, you will
see a graphical interface for sysprep, where you can choose between the available options:
Since I always use the same set of options for sysprep, I find it easier to simply include all of
my optional switches right from the command-line input, therefore bypassing the graphical
screen altogether. Here is some information on the different switches that are available to use
with Sysprep.exe:
/quiet: This tells sysprep to run without status messages on the screen.
/generalize: This specifies that sysprep is to remove all of the unique system
information (SID) from the Windows installation, making the final image usable on
multiple machines in your network, because each new one spun up from the image will
get a new, unique SID.
/audit: This restarts the machine into a special audit mode, where you have the option of
adding additional drivers into Windows before the final image gets taken.
/oobe: This tells the machine to launch the mini-setup wizard when Windows next boots.
/reboot: This restarts when sysprep is finished.
/shutdown: This shuts down the system (not a restart) when sysprep is finished. This is an
important one and is one that I typically use.
/quit: This closes sysprep after it finishes.
/unattend: There is a special answerfile that you can create that, when specified, will be
used in conjunction with the sysprep process to further configure your new servers as
they come online. For example, you can specify in this answerfile that a particular
installer or batch file is to be launched upon first Windows boot following sysprep. This
can be useful for any kind of cleanup tasks that you might want to perform, for example,
if you had a batch file on your system that you used to flush out the log files following
the first boot of new servers.
The two that are most important to our purposes of wanting to create a master image file that
we can use for quick server rollouts in the future are the /generalize switch and the
/shutdown switch. Generalize is very important because it replaces all of the unique
identification information, the SID info, in the new copies of Windows that come online. This
allows your new servers to co-exist on the network with your original server, and with other
new servers that you bring online. The shutdown switch is also very important, because we
want this master server to become sysprepped and then immediately shutdown so that we can
create our master image from it.
Tip
Make sure that your server does NOT boot into Windows again until after you have created
your master image, or taken your master copy of the .VHDX file. The first time that Windows
boots it will inject the new SID information, and you want that only to happen on new servers
that you have created based off your new image.
So rather than simply throwing all of the switches at you and letting you decide, let's take a
look at the ones that I typically use. I will make use of /generalize so that I make my new
servers unique, and I also like to use /oobe so that the mini-setup wizard launches during the
first boot of Windows on any of my new systems. Then, I will of course also use /shutdown,
because I need this server to be offline immediately following sysprep so that I can take a
copy of the hard drive to be used as my master image. So here is my fully groomed sysprep
command:
Sysprep.exe /generalize /oobe /shutdown
After launching this command, you will see sysprep moving through some processes within
Windows, and after a couple of minutes, your server will shut itself down:
You are now ready to create your master image from this hard disk.�


Creating your master image of the drive
Our master server has now shut down, and we are ready to create our master image from this
server. If it is a physical server, you can use any hard disk imaging utility in order to create an
image file from the drive. An imaging utility like those from the company Acronis will create
a single file from your drive, this file contains an image of the entire disk that you can use to
restore down onto fresh hard drives in new servers in the future. On the other hand, most of
you are probably dealing with virtual servers most often in your day to day work lives, and
prepping new servers in the virtual world is even easier. Once our master server has been
sysprepped and shutdown, you simply create a copy of the .VHDX file. Log in to your Hyper-V
server, copy and paste the hard disk file, and you're done.
This new file can be renamed WS2016_Master_withUpdates.VHDX or whatever you would like
it to be named in order to help you keep track of the current status on this image file. Save this
image file or copy of the .VHDX somewhere safe on your network, where you will be able to
quickly grab copies of it whenever you have the need to spin up a new Windows Server 2016.
Building new servers using copies of the master image
Now we get to the easy part. When you want to create new servers in the future, you simply
copy and paste your master file into a new location for the new server, rename the drive file
to be something appropriate for the server you are creating, and boot your new virtual
machine from it. Here is where you see the real benefit for the time that sysprep saves, as you
can now spin up many new servers all at the same time, by doing a quick copy-paste of the
master image file and booting all of your new servers from these new files.
As the new servers turn on for the first time and boot into Windows, they will run through the
out-of-the-box-experience, mini-setup wizard. Also, in the background, the operating system
gives itself a new random and unique hostname and SID information so that you can be sure
you do not have conflicts on your network with these new servers.
Tip
New servers created from a sysprepped image file always receive a new hostname when they
boot. This often confuses admins who might have named their master server something like
MASTER. After booting your new servers, you can expect to see randomized names on your
new servers and you will have to rename them according to their new duties in life.
For example, before running sysprep and creating my master image, the server that I was
working on was named DC1 because I had originally intended to use it as a Domain Controller
in my network. However, because I had not installed the role or configured anything domain
related on it, this server was a perfect candidate for displaying the sysprep process and so I
used it in our text today. You can now see inside the sysprep properties that I am back to
having a randomized hostname, and so if I still want to use this server as DC1, I will have to
rename it again now that it has finished booting through mini-setup:
Hopefully, this process is helpful information that can save you time when building out new
servers in your own environments. Get out there and give it a try the next time you have a new
server to build! You can further benefit yourself with the sysprep tool by keeping many
different master image files. Perhaps you have a handful of different kinds of servers that you
prep regularly, there is nothing stopping you from creating a number of different master
servers, and creating multiple master images from these servers.�

