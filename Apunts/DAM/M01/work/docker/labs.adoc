// https://www.katacoda.com/learn

==

. Identificar la imatge de docker configurada per executar Redis.
+
[source, bash]
----
docker search redis
----

. Executar un contenidor de redis en "background"
+
[source, bash]
----
docker container run -d redis
----

. Veure la llista de contenidors que estan corrent.
+
[source, bash]
----
docker ps
----
+
[source, bash]
----
docker container list
----

. Mirar més informació del container amb *_docker container inspect_*.
[source, bash]
----
docker container inspect <nom|id>
----

. Mirar els logs del contenidor creat.
+
[source, bash]
----
docker container logs <nom|id>
----

. Sabent que Redis s'executa al port *6379* per defecte, exposar el port en un nou contenidor de nom *_redistest_* amb la última versió de la imatge de redis.
+
[source, bash]
----
docker run -d --name redistest -p 6379:6379 redis:latest
----

. Executar més d'una instància de redis a diferewnts ports utilitzant ports dinàmics. Veure quins porrts s'estan utilitzant.

[source, bash]
----
docker run -d  p 6379 redis:latest
----

[source, bash]
----
docker container port <id|nom>
----

. La documentació de Redis indica que les dades de la imatge s'emmagatzemen al directori /data. Volem vincular una carpeta ~/redis del host amb la carpeta /data del contenidor perquè les dades emmagatzemnades siguin permanents.
+
[source, bash]
----
docker run -d --name redisvolume -v ~/redis:/data redis:latest
----

. Executa un contenidor de ubuntu i accedes a la shell d ebash dintre del contenidor.
+
[source, bash]
----
docker run -it ubuntu bash
----

==

[source, bash]
----

----

[source, bash]
----

----

[source, bash]
----

----

[source, bash]
----

----

