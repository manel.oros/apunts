Volcat MBR 

----
dd if=/dev/sda count=1 | hexdump -C
----




Hem vist com el MBR es divideix en dues parts ben diferenciades, el programa MBR que ocupa la major part del MBR i la taula de particions vista anteriorment.

Existeixen diversos programes que ens permeten gestionar i retocar aquests components del MBR.

En Windows existeix la comanda *((fixmbr))* que reinstal·la el programa del MBR, aquesta comanda nomes es pot utilitzar des de la consola de recuperació.

La taula de particions, pot ser gestionada per diversos programes que s'inclouen en els sistemes operatius. 

* En sistemes Windows 9x, la utilitat encarregada d'això és el *_((fdisk))_*. 
* Dins de la família Windows NT és la consola de l'administrador de discos *_((diskmgmt.msc))_*. Aquesta consola és la inclosa oficialment per la pròpia Microsoft, i existeixen multitud de programes de terceres companyies que permeten retocar aquesta taula de particions. 
+
[WARNING]
====
No és recomanable l'ús abusiu d'aquestes eines doncs poden espatllar la taula, i solen donar problemes a la llarga.
====
* Dins de la família Windows NT trobem també una eina de línia de comandes que permet gestionar les particions, *_((diskpart.exe))_*.
* Linux per la seva banda inclou diversos programes d'aquest tipus, com poden ser *_((fdisk))_*, *_((qtparted))_*, *_((parted))_*, *_((gparted))_* etc...

Cal tenir molta cura en treballar amb les particions. La taula MBR és una taula molt sensible a qualsevol tipus de canvis. Una mala elecció de qualsevol dels seus camps, pot portar a la inutilització total del disc dur. A més, donada la facilitat per “trastejar” amb la taula de particions, molts programes utilitzen configuracions estranyes que són desconegudes per a altres programes, la qual cosa pot portar a perdre particions o a canviar la seva grandària de manera incorrecta.


Per veure l'ajuda de totes les comandes possibles que podem executar en _diskpart_ podem executar el comando help. _diskpart_ no solament permet treballar amb discos durs bàsics, sinó també amb discos durs dinàmics (que utilitzen *LVM (gestor lògic de volums) que és un concepte que veurem en segon).






==== Arrencada de Windows 7/vista/8/10

((TODO))

////
ARRENCADA DE WINDOWS 7/VISTA/2008/8/10
La seqüència d'arrencada de Windows va canviar a partir de XP. La principal diferència estreba que s'ha canviat el gestor d'arrencada, ja no s'usa el *ntldr sinó que s'usa el Windows *Boot *Manager (*bootmgr).
Mentre que el gestor *ntldr usava un fitxer de text denominat *boot.*ini per configurar les seves opcions, *bootmgr utilitza una base de dades coneguda com *Boot *Configuration Data (*BCD) que no pot ser editada directament com l'era el *boot.*ini ja que no és un fitxer de text.
El *BCD és una base de dades amb dades sobre la configuració de l'arrencada que se sol emmagatzemar en \*Boot\*Bcd.
1. Es carrega i executa el *POST
2. Es carrega el *MBR del disc dur (si és l'opció triada en la *BIOS)
3. Es carrega el sector d'arrencada de la partició primària activa
4. Es carrega el programa *bootmgr.
5. *bootmgr ajusta el processador per treballar a 32 bits o 64 bits.
6. *bootmgr llegeix la base de dades *BCD i mostra un menú si és necessari
7. L'usuari selecciona un sistema operatiu del menú, o es carrega per defecte un d'ells
8. *bootmgr carrega *winload.*exe.
9. *Winload.*exe carrega *NTOSKRNL.*EXE (Nucli del sistema operatiu o *Kernel).
10. *NTOSKRNL.*EXE llegeix el registre de Windows, i procedeix a anar carregant el sistema complet.
Windows disposa d'un comando per configurar aquesta base de dades *BCD, el *bcedit.*exe, però és realment enutjós d'usar. És millor usar una utilitat *grafica d'una 3*rd *party (tercera companyia, una companyia diferent a la qual realitza el sistema) com pot ser *EasyBCD que permet configurar moltes més opcions que el *bcedit.*exe i de forma molt més fàcil.
////

===== Arrencada de Windows 8/10

((TODO))

////
Encara que l'arrencada de Windows actual és molt similar al de Windows 7 incorpora diverses novetats, moltes d'elles basades en l'ús de *UEFI en lloc de *BIOS. Una de les més importants és la del *Secure *Boot.
*SECURE *BOOT.
Els ordinadors quan trobaven el sector d'arrencada del SOTA que volien carregar, es limitaven a executar aquest codi, sense comprovar de cap manera què és el que s'està executant.
No obstant això, si expliquem en el sistema amb *UEFI en lloc de *BIOS i aquesta activada una característica coneguda com *Secure *Boot, el *firmware del sistema comprova la signatura digital del sector d'arrencada, per comprovar si és d'un sistema reconegut, i si s'ha produït algun tipus de modificació sobre el mateix. Per permetre l'arrencada del sistema operatiu, s'han de donar una de les següents situacions:
- El codi de càrrega va ser signat utilitzant un certificat “de confiança”. Per exemple un certificat de Microsoft.
- L'usuari ha aprovat la signatura digital del codi de càrrega. *UEFI hauria de permetre a l'usuari realitzar aquesta acció. (No sempre ocorre).
- L'usuari deshabilita *Secure *Boot en la configuració de *UEFI.
- L'usuari deshabilita totalment *UEFI, i en el seu lloc utilitza *BIOS.
*TRUSTED *BOOT.
Una vegada que *secure *boot ha acabat la seva comesa, el codi de càrrega (*bootloader) verifica el signat del *kernel de Windows 8 abans de carregar-ho. Al seu torn, el *kernel de W8 verifica tots els components de Windows que es van carregant, incloent els *drivers de dispositiu de la pròpia Microsoft que es carreguen en l'arrencada. Si un fitxer ha estat modificat, el *bootloader detecta el problema i es nega a seguir carregant el component. Windows 8 intenta reparar el component corrupte automàticament.
*FAST
*STARTUP Windows *Fast *Startup (Inici ràpid) és l'opció per defecte a utilitzar en Windows 8, Windows *Server 2016 i Windows 10 sempre que s'utilitzi *UEFI.
En un sistema Windows a cada moment es troben executant-se dues sessions en realitat, la de l'usuari actual i la del *kernel del sistema. Quan en Windows 7 s'apaga el sistema, es tanquen ambdues sessions i cal tornar a carregar-les des de zero quan el sistema s'inicia.

Windows 10 tanca totalment la sessió de l'usuari i la torna a carregar en cada inici, no obstant això, la sessió del *kernel la hiberna, llegint tot el seu estat en la RAM i gravant-ho directament en el disc dur. Això permet que quan el sistema s'iniciï, no es vegi obligat a tornar a llegir tots els arxius del *kernel, sinó que directament recupera l'estat des del disc dur fins a la RAM. Això permet que s'iniciï Windows 10 molt més ràpid que Windows 7.
Aquesta hibernació es realitza solament amb la sessió de *kernel perquè és petita i predictible, mentre que no es realitza amb la sessió d'usuari perquè sol ser molt més gran, i és impredictible (igual ocupa molt poc que moltíssim).
En el cas que comptem amb diversos sistemes operatius instal·lats en la mateixa màquina, si canviem d'un sistema operatiu a l'un altre es descarta la sessió de *kernel gravada en el disc dur i veurem com es “reinicia” la màquina dues vegades.
Aquest *fast *startup ens pot donar problemes en determinats escenaris:
Quan s'apaga un equip amb *Fast *Startup activat, Windows bloqueja el disc dur. No podrem accedir al mateix des d'un altre sistema operatiu. Si es dóna la circumstància que intentem usar aquest disc dur en una altra màquina ens podem trobar amb casos en els quals es corromp la informació emmagatzemada.
Depenent del nostre sistema, pot ser que no siguem capaços d'accedir a la configuració del nostre *BIOS/*UEFI *settings si tenim *Fast *Startup activat i hem hibernat la nostra sessió de *kernel. Quan hibernem un equip, aquest no entra en manera d'apagat complet i després no experimenta un encès complet (se sol parlar d'encès calent i encès *frio)
Si volem apagar totalment el nostre Windows 10 fent que no s'hiberni la sessió i que realitzi una arrencada completa la propera vegada que s'encengui l'equip, cal executar el següent comando en un interfície de línia de comandos amb poders d'administrador:
*shutdown /s /t 0
També podem desactivar totalment *Fast *Startup amb la instrucció
*powercfg /*hybernate *off
i tornar a activar-la quan vulguem amb
*powercfg /*hybernate *on
////

==== Arrencada de Linux. GRUB

((TODO))

////
Linux no compta amb un gestor d'arrencada pròpia, sinó que permet usar qualsevol gestor d'arrencada que desitgem. El que se sol incloure actualment en totes les versions de Linux és el *GRUB.
El Grand *Unified *Bootloader (*GRUB) és un gestor d'arrencada múltiple que s'usa comunament per iniciar dues o més sistemes operatius instal·lats en un mateix ordinador. Altres gestors d'arrencada usats anteriorment en Linux són el *syslinux i el *lilo.
En l'actualitat ens podem trobar amb *GRUB en les seves versions 1 i 2, que són alguna cosa diferents.
El procés d'inici de *GRUB 1 és el següent:
1. La *BIOS busca un dispositiu d'inici (com el disc dur) i passa el control al registre mestre d'inici (Màster *Boot *Record, *MBR, els primers 512 bytes del disc dur).
2. El *MBR conté la fase 1 de *GRUB. Com el *MBR és petit (512 bytes), la fase 1 només s'encarrega de buscar i carregar la següent fase del *GRUB (situat físicament en qualsevol part del disc dur). La fase 1 pot carregar ja sigui la fase 1.5 o directament la
2 3. *GRUB fase 1.5 està situada en els següents 30 kilobytes del disc dur. La fase 1.5 càrrega la fase 2. Aquesta fase és optativa i normalment no s'usa.
4. *GRUB fase 2 (carregada per les fases 1 o 1.5) rep el control, i presenta a l'usuari el menú d'inici de *GRUB. Aquest menú es configura mitjançant un fitxer de text amb nom *menu.*lst.
5. *GRUB carrega el *kernel (nucli) seleccionat per l'usuari en la memòria i li passa el control perquè carregui la resta del sistema operatiu.
*GRUB 2 substitueix el fitxer *menu.*lst (que editem manualment) per un procés modular, de manera que automàticament s'afegeixen els sistemes operatius i les opcions dels mateixos. Ja veurem en profunditat aquests gestors en els temes dedicats a GNU/Linux.
*GRUB no és en realitat un gestor d'arrencada per Linux, sinó un gestor d'arrencada per a qualsevol sistema operatiu. De fet, *GRUB és perfectament capaç d'arrencar qualsevol sistema operatiu de la família Windows sense cap tipus de problemes. Veiem aquí una llista cronològica indicant que moment apareix cada sistema operatiu.
////

==== Recuperació d'errors en l'arrencada

((TODO))

////
El procés d'arrencada és un concepte al que l'administrador de sistemes ha de prestar-li molta atenció, atès que el més mínim problema que s'origini en aquest procés, farà impossible que el sistema operatiu arrencada, i per tant deixés inservible el sistema informàtic.
Les zones que cal vigilar i conèixer com recuperar si és necessari, són el *MBR, el sector d'arrencada de la partició primària activa i el programa gestor d'arrencada que aquest situat en aquestes zones.
Però, que errors es poden produir en l'arrencada?
En primer lloc hem de parlar de les fallades de maquinari. En usar un disc dur sempre existeix la possibilitat que es corrompin clústers del mateix. Normalment aquests errors no solen tenir massa importància, però si es dóna la casualitat que es corromp el primer clúster del disc dur, que és on se situa el sector del *MBR i el primer sector d'arrencada de la primera partició, ens anem a trobar en seriosos problemes. Normalment en aquests casos el millor és canviar el disc dur complet, i intentar recuperar la informació que existia en el disc dur amb algun programa de recuperació de dades professional.
En segon lloc ens trobem l'acció del *malware (virus, cucs, *troyanos, etc.). Aquestes amenaces poden esborrar el *MBR i els sectors d'arrencada, i antigament existien bastants virus que es dedicaven a realitzar aquestes accions. Avui dia, i amb la “professionalització” dels desenvolupadors de *malware, aquestes pràctiques han quedat relegades a l'oblit.
La tercera causa, i la que sol ser culpable en el 99% dels casos, és que directament l'usuari espatlli l'arrencada d'un sistema operatiu, simplement instal·lant un segon operatiu. Vegem amb detall aquesta situació:
Hem vist com cada sistema operatiu compta amb el seu propi programa per instal·lar en el *MBR, el seu propi programa per instal·lar en el sector d'arrencada, i també compten amb el seu propi gestor d'arrencada.
És clar que si instal·lem en un mateix disc dur tres sistemes operatius diferents, cadascun d'ells haurà anat instal·lant el seu propi procés d'arrencada, però com solament pot existir un procés d'arrencada en un disc dur (només existeix un *MBR) el procés d'arrencada que es quedi al final serà el de l'ultimo sistema operatiu instal·lat, que picarà el procés d'arrencada del sistema operatiu anteriorment instal·lat, i així successivament.

Imaginem el cas següent: En un disc dur tenim instal·lat una partició amb Windows
En el *MBR tindrem instal·lat evidentment el gestor d'arrencada de Windows, i en la partició de Windows tindrem instal·lat els arxius que necessita el gestor d'arrencada de Windows per funcionar.
Decidim instal·lar en aquest disc dur una distribució de Linux com *Debian, per a això li vam crear una partició i procedim a instal·lar aquest sistema operatiu:
Durant aquest procés d'instal·lació, *Debian instal·larà (si no ho evitem) en el *MBR el gestor d'arrencada de *Debian (en aquest cas *grub), i per tant picarà al gestor d'arrencada de Windows que estava anteriorment instal·lat en el *MBR.
La propera vegada que iniciem la màquina, es carregarà el gestor d'arrencada de *grub, no l'anterior que teníem de Windows. Reconeixerà el gestor d'arrencada de *grub que en el disc dur existeix un Windows i ens permetrà arrencar des del, a part d'arrencar des de *Debian? Doncs en aquest cas sí. En el *mundillo dels gestors d'arrencada, és convenient recordar sempre aquestes petites regles:
1) *Grub és capaç d'arrencar qualsevol sistema operatiu, per la qual cosa respectarà sempre (o almenys ho intentarà) qualsevol sistema operatiu que hi hagués en disc dur abans que s'instal·lés aquest gestor d'arrencada.
2) Els gestors d'arrencada de Windows mai respectaran a Linux. De fet, el gestor d'arrencada de Windows solament és capaç d'arrencar automàticament a sistemes operatius Windows, sent molt complicat aconseguir arrencar altres sistemes operatius no de Microsoft.
WINDOWS WINDOWS WINDOWS
*MBR *MBR *MBR
WINDOWS *XPWINDOWS XP WINDOWS XP
*DEBIAN LINUX *DEBIAN LINUX *DEBIAN LINUX *DEBIAN LINUX
*MBR *MBR *MBR

3) Els gestors d'arrencada de Windows respecten als sistemes operatius Windows però solament als anteriors a dita Windows. És a dir, Windows 8 reconeix i respecta a Windows 7, però al contrari no, ja que quan es va crear el gestor d'arrencada de 7 el sistema operatiu Windows 8 no existia, i per tant aquest gestor d'arrencada no ho reconeixerà com un SOTA legítim, i per tant es negarà a arrencar-ho de forma automàtica.
Comproveu *cuales de les següents instal·lacions de sistemes operatius en un mateix disc dur, donarien problemes i *cuales no:
a) Instal·lem Windows 7, després Windows 8 i per ultimo Windows 10. Donaria problemes? Quins sistemes operatius apareixerien per escollir en el menú d'arrencada?
b) Instal·lem Linux, després Windows 10 i per ultimo Windows 7.
c) Instal·lem Windows 10, després Windows 8 i per ultimo Windows 7.
d) Instal·lem un Windows 10 i després un altre Windows 10.
Cada sistema operatiu compta amb eines que permeten reconstruir el programa gestor d'arrencada en el *MBR, i arreglar els sectors d'arrencada.
WINDOWS XP.
En el cas de Windows XP cal iniciar el sistema des del CD original d'instal·lació de Windows XP. En el procés d'instal·lació que s'executarà, cal arribar fins al punt en què ens permet executar la “consola de recuperació”. En aquesta consola podrem executar des de línies de comandos les següents ordres:
*FIXMBR Instal·la el gestor d'arrencada de XP en el *MBR.
*FIXBOOT Recupera el sector d'arrencada de Windows XP.
També existeixen ordres per recuperar la llista de sistemes operatius que apareixen en el menú, però això ho veurem en un tema posterior.
WINDOWS 7.
Igualment que en el punt anterior, hem d'iniciar el sistema des del CD original d'instal·lació de Windows 7. Arribarà un moment en què el propi programa d'instal·lació ens donarà l'opció de realitzar una reparació automàtica de l'inici de Windows. Escollim aquesta opció i comprovem si el sistema és capaç de reparar-se automàticament. Si comprovem que aquest automatisme falla (cosa bastant probable) tornem a iniciar el sistema des del CD, però aquesta vegada des del menú avançat escollim l'opció de consola de recuperació o línia de comandos. Des d'allí podem executar les següents ordres:
*Bootrec.*exe /*fixmbr Instal·la el gestor d'arrencada de Windows 7 en el *MBR.
*Bootrec.*exe /*fixboot Recupera el sector d'arrencada de Windows 7.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 28 I.S.O. Arrencada d'un sistema operatiu.
WINDOWS 8/10/2016
Les instruccions que hem vist anteriorment per Windows 7 funcionen exactament igual en Windows 8.
LINUX.
En aquest cas iniciem el sistema des d'un CD especial per a recuperació del *grub, com pot ser per exemple el “súper *grub *disk” o “súper *grub2 *disk”. També podem recuperar el sistema arrencant des d'un *cd d'una distribució “*live”. Aquest tema ho deixem para quan ens hàgim familiaritzat amb Linux.
PROBLEMES D'ARRENCADA AMB *UEFI I *BIOS.
*UEFI (*Unified Extensible *Firmware *Interface) és una interfície de *firmware estàndard per *PCs, dissenyada per reemplaçar *BIOS (sistema bàsic d'entrada i sortida). És un estàndard creat per més de 140 companyies tecnològiques que formen part del consorci *UEFI, en el qual s'inclou Microsoft. S'ha dissenyat per millorar la interoperabilitat del programari i solucionar les limitacions del *BIOS. Algunes dels avantatges que ofereix el *firmware *UEFI són:
• Ajudar a protegir el procés previ a l'inici enfront d'atacs de *bootkit.
• Temps d'inici i represa des de la hibernació més ràpids
• Compatibilitat amb unitats de disc dur amb particions de més de 2,2 *TB.
• Compatibilitat amb controladors de dispositius de 64 bits.
• Capacitat per usar *Secure *Boot.
*UEFI és el *firmware que ara mateix podem trobar en els PC comercials, és molt estrany trobar un equip que no ho suporti. En *BIOS de tipus *UEFI únicament podem instal·lar els sistemes de 64 bits. Els de 32 bits no es poden instal·lar en manera *UEFI. La *UEFI és un *BIOS molt més amigable que la clàssica *BIOS amb pantalla blava, suporta un entorn gràfic de major qualitat, *multilenguaje, *precarga d'aplicacions o gestió de *LAN, entre moltes altres opcions.
El gran problema que ens podem trobar en l'arrencada, és que actualment podem configurar el *BIOS bé en manera compatibilitat (*legacy) o en manera *UEFI puro. Bé, doncs un sistema operatiu que s'hagi instal·lat en manera *UEFI no podrà ser carregat per un sistema que es passi a *BIOS, i viceversa. Això fa que moltes vegades ens trobem amb sistemes que no arrenquen simplement perquè no s'ha escollit la versió correcta.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 29 I.S.O. Arrencada d'un sistema operatiu.
Un disc dur instal·lat nou sota *UEFI tindrà una estructura com la següent:
Veiem com es crea de forma predeterminada una partició de sistema Extensible *Firmware *Interface (partició de sistema *EFI), una partició reservada de Microsoft (*MSR, Microsoft *Reserved *Partition) i una partició principal de Windows que és on s'instal·larà el nostre SOTA.
En una instal·lació per defecte *BIOS el disc dur quedarà de la següent forma:
////

==== Arrencada a mode de fallades

((TODO))

////
En punt anterior hem vist com podem solucionar les fallades de l'arrencada més importants, que comporten sobreescriure el *MBR o bé el sector d'arrencada. No obstant això existeixen molts altres tipus d'errors que es poden produir en l'inici del sistema operatiu, i que no es poden solucionar amb aquestes tècniques. Errors típics d'aquest tipus poden ser la instal·lació d'un *driver corrupte, l'esborrat accidental d'un fitxer del sistema, etc.
Quan un sistema no pot iniciar-se a causa d'un error d'aquest tipus, sempre podem intentar iniciar el sistema operatiu en una manera especial coneguda com a manera a prova de fallades, on es carregaran les funcions bàsiques del sistema, intentant saltar-se les parts que poden estar provocant fallades. Per ingressar en la manera a prova de fallades en un Windows, n'hi ha prou amb prémer la tecla F8 just quan el sistema inicia la seva càrrega.
Aquesta pantalla que veiem aquí per exemple, és la que s'obté si iniciem Windows 7 després d'haver-nos sortit del mateix d'una forma *descontrolada (apagant l'ordinador sense tancar el sistema, per exemple).

Si premem F8 quan el sistema s'està iniciant, no obstant això, Windows ens presenta la següent pantalla:
Veiem com a més de les maneres segures, permet arrencar el sistema amb altres configuracions establertes, com poden ser amb gràfics de baixa resolució. Aquest menú apareix d'una forma o una altra en totes les versions de Windows, encara que en Windows 8/10 cal activar-ho abans de poder usar-ho des del panell de control del propi Windows.
Per fer això en Windows 10 cal anar al menú de configuració de Windows (Windows + I).
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 32 I.S.O. Arrencada d'un sistema operatiu.
En aquest panell de configuració escollim Actualització i seguretat. Després Recuperació i Inici Avançat – Reiniciar ara.
Amb la qual cosa arribarem a la pantalla de recuperació / inicio avançat de Windows 10.
L'opció apagar l'equip ens permet realitzar un apagat total de l'equip, perquè després puguem arrencar-ho en “*frio”, la qual cosa ens permet entrar en el *Setup del *BIOS, etc. Si no apaguem l'equip així, realment no s'apaga del tot i després realitza una arrencada en “calenta”. Aquest tipus d'arrencada no comprova les pulsacions del teclat amb la qual cosa no podrem entrar en *BIOS.
L'opció que ens interessa en aquest moment és la de solucionar problemes.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 33 I.S.O. Arrencada d'un sistema operatiu.
Les opcions restaurar i restablir permeten reinstal·lar completament el sistema operatiu, bé sense perdre arxius o ben realitzant un formato i per tant perdent-ho tot. A nosaltres ara mateix ens interessa Opcions avançades.
Des d'aquí podem realitzar moltes coses (una d'elles és intentar automàticament reparar l'inici, encara que normalment no funciona). Les opcions que ens interessen són ben Símbol del Sistema, per poder accedir a la consola de recuperació i executar els comandos *Bootrec com ja vam veure anteriorment, o bé Configuració d'inici, que ens mostrarà el menú de la manera d'arrencada a prova de fallades.
En Linux no tenim una manera segura com a tal, però podem passar-li paràmetres al *kernel indicant com volem llançar el nostre Linux, desactivant per exemple els gràfics en alta resolució, el *multiusuario, els ports USB, etc. Ho veurem en el tema de Linux.
IMPLANTACIÓ DE SISTEMES OPERATIUS. CURS 17/18
TEMA 2 Pàgina. 34 I.S.O. Arrencada d'un sistema operatiu.
EVOLUCIÓ HISTORIA DELS SISTEMES OPERATIUS.
Al llarg de la història, han existit centenars de famílies de sistemes operatius, cadascuna d'elles composta per desenes de sistemes operatius diferents. És una història que comença en els anys 1960 i que segueix fins al dia d'avui.
Podria semblar que existeixen pocs sistemes operatius al mercat, però si investiguem una mica ens adonem que existeixen centenars d'alternatives possibles. És més, avui dia s'ha obert el mercat dels sistemes operatius per a mòbils i *tablets, on existeixen desenes de sistemes operatius nous que es van presentat cada any.
////



== 
PUNTS DE MUNTATGE.
Normalment estem acostumats a la forma en què els sistemes operatius de Microsoft denominen als mitjans d'emmagatzematge secundaris, assignant una lletra a cada volum, de manera que la disquetera és l'A: la primera partició del primer disc dur és la C: la següent la D: etc. En Linux tot això canvia.
En primer lloc, vegem com Linux referencia als propis discos durs. Així, el primer disc dur de nostre maquina en Linux es coneix com /*dev/*hda (si és paral·lel) o /*dev/*sda (si és serial).
/ indica el *root o arrel de l'arbre de Linux (En Linux solament existeix un arbre)
*dev ens indica el directori on s'emmagatzemen tots els dispositius (*devices)
/*hda ens indica que ens referim al *Hard *Disk (*hd paral·lel) amb la lletra a, és a dir, el 1º.
/*dev/*hda – Dispositiu mestre en la *IDE 1.
/*dev/*hdb – Dispositiu esclau en la *IDE 1.
/*dev/*hdc – Dispositiu mestre en la *IDE 2.
/*dev/*hdd – Dispositiu esclau en la *IDE 2.
/*dev/*sda - Dispositiu sèrie en el bus serial 1.
/*dev/*sdd – Dispositiu sèrie en el bus serial 4.
Quan referenciem particions, s'utilitza un nombre a continuació del nom del disc dur. Aquest nombre representa la partició. Així, /*dev/*hda2 ens indica que ens referim a la segona partició del disc dur mestre de *IDE 1. Com en un disc dur no poden existir més de quatre particions primàries, aquestes reben nombres de l'1 al 4. Si vam crear una partició estesa, aquesta no rep cap nombre (igual que en Windows no se li assigna una lletra) i a les unitats lògiques que es creen dins d'aquesta partició estesa se li assignen nombres a partir del 5. Vegem alguns exemples.
/*dev/*hdb1 - Primera partició primària del disc dur 2 (esclau en el bus *IDE 1).
/*dev/*hda5 - Primera unitat lògica del disc dur 1 (mestre en el bus *IDE 1).
/*dev/*sdc7 - Tercera unitat lògica del disc dur 3 (*SATA en el bus 3).
Com vam comentar anteriorment, Linux no utilitza “lletres” per accedir a les particions que creiem, així que Com podem gravar un arxiu per exemple en /*dev/*hdb6? En Windows ens limitaríem a gravar l'arxiu en D: o I: o la lletra que assignem a aquesta partició, però això no es fa així en Linux.
IMPLANTACIÓ DE SISTEMES OPERATIUS.CURS 17/18
TEMA 5.1 Pàgina 21 I.S.O. GNU – Linux. Introducció.
En Linux, cada dispositiu d'emmagatzematge (partició, disquet, CD) que utilitzem ha de ser muntat en el nostre arbre mitjançant un punt de muntatge. En Linux solament existeix un espai d'emmagatzematge, un únic arbre que comença en l'arrel (*root) i que conté tot el que tenim en el nostre sistema. Això s'aconsegueix associant cada partició a un directori mitjançant un procés denominat muntatge.
Muntar una partició fa que el seu espai d'emmagatzematge es trobi disponible accedint al directori especificat (conegut com a punt de muntatge).
Per exemple, si muntem la partició /*dev/*hda5 en /*usr, significa que tots els fitxers i directoris a partir de /*usr resideixen físicament en /*dev/*hda5.
Per tant, el fitxer /*usr/*doc/*FAQ/txt/Linux-*FAQ estarà emmagatzemat en /*dev/*hda5, cosa que no ocorre amb el fitxer /etc/X11/*gdm/*sessions/*gnome.
És absolutament obligatori muntar almenys el *root o arrel (/) durant la instal·lació.
*Raiz (/) *Raiz (/)*Raiz (/) *Raiz (/)
*documentosdocumentos *documentosdocumentos *documentosdocumentos
música *músicamúsica
*fotosfotos fotos
apuntis *apuntesapuntesapuntesapuntes
*iso *iso
*parpar
/*DEV/*SDB /*DEV/*SDB /*DEV/*SDB1
/*DEV/*SDB5/*DEV/*SDB5 /*DEV/*SDB5/*DEV/*SDB5 /*DEV/*SDB5/*DEV/*SDB5
/*DEV/*SDA1 /*DEV/*SDA1 /*DEV/*SDA1
/*DEV/*SDA2 /*DEV/*SDA2 /*DEV/*SDA2
IMPLANTACIÓ DE SISTEMES OPERATIUS.CURS 17/18
TEMA 5.1 Pàgina 22 I.S.O. GNU – Linux. Introducció.
PARTICIÓ *SWAP.
Vam veure quan tractem els temes sobre conceptes de Sistemes Operatius, que existia una tècnica coneguda com a paginació de memòria, que ens permetia oferir als programes més memòria de la qual existeix físicament en la màquina, usant per a això una memòria virtual que en realitat existia en el disc dur.
En Windows aquesta tècnica utilitza un arxiu d'intercanvi que és gestionat directament per Windows, i se sol cridar *pagefile.*sys. Linux no crea cap arxiu d'intercanvi (tret que li obliguem), sinó que utilitza una partició sencera per a aquesta fi, coneguda com a partició *Swap. Simplement hem de crear-la en la instal·lació de Linux, i el sistema s'encarrega d'usar-la, sense tenir nosaltres que muntar-la ni gens per l'estil.
La grandària que se li sol donar a una partició *Swap, és el doble de la memòria RAM que tinguem instal·lat en el nostre sistema, sense excedir mai els 2 GB de *Swap. Aquesta és una regla general, encara que en cada cas particular pot ser que la grandària ideal de *Swap sigui diferent.
En *linux també tenim la possibilitat de no utilitzar una partició per a aquesta comesa i establir un fitxer tal com ho fa Windows. Ho veurem més endavant.


== Discs bàsics i dinàmics

Els discs bàsics i els discs dinàmics són dos tipus de configuracions de disc dur en Windows. La majoria
dels equips personals estan configurats com a discs bàsics, que són més fàcils d’administrar. Els usuaris
avançats i professionals informàtics poden fer servir discs dinàmics que fan servir diferents discs durs d’un
equip per administrar dades, normalment per obtenir un major rendiment o fiabilitat:
Disc bàsic: Un disc bàsic fa servir particions primàries, particions esteses i unitats lògiques per organitzar
les dades. Una partició formatejada també s’anomena volum (així volum i partició es poden fer servir
generalment de forma indistinta). Normalment els discs bàsics poden tenir 4 particions primàries o 3
primàries i 1 estesa. La partició estesa pot tenir diferents unitats lògiques (fins 128). Les particions d’un disc
bàsic no es poden compartir ni dividir dades amb altres particions. Cada partició d’un disc bàsic és una
entitat independent del disc.
Disc dinàmic: Els discs dinàmics poden contenir un gran nombre de volums dinàmics (fins 2000), que
funcionen com les particions primàries fetes servir en els discs bàsics. En algunes versions de Windows, es
possible combinar discs durs dinàmics independents en un únic volum dinàmic (anomenat expansió), dividir
dades entre diferents discs durs (anomenat secció) per tal d’obtenir un major rendiment o duplicar dades
entre diferents discs durs (que es pot anomenar mirall) per obtenir major confiabilitat.
