//DECRET (p33): https://xtec.gencat.cat/web/.content/alfresco/d/d/workspace/SpacesStore/0052/64a26049-c0c0-45cc-baa0-a4f831059839/DOGC_TS_desenvolupament_aplicacions_multiplataforma.pdf
= M03 - UF6: POO. Introducció a la persistència en les BD

== Introducció

(PENDENT)

.Model normalitzat. Imatge: 
[link=https://www.scientecheasy.com/2020/06/packages-in-java.html/]
image::[align="center"]

//1. Gestiona informació emmagatzemada en bases de dades relacionals mantenint la integritat i la consistència de les dades
//include::UF6_RA01.adoc[]


//3. Utilitza bases de dades orientades a objectes, analitzant-ne les característiques i aplicant tècniques per mantenir la persistència de la informació.
//include::UF6_RA03.adoc[]

=== Llicència

image:by-sa.png[]
Aquesta obra està subjecta a una llicència de http://creativecommons.org/licenses/by-sa/4.0/[Reconeixement-CompartirIgual 4.0 Internacional de Creative Commons]

----
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
----
