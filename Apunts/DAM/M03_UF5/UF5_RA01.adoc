== Col.leccions

Per parlar de col.leccions hem de parlar primerament d'arrays. Un array en l'àmbit de la programació de tipus primitius és una llista de valors d'un determinat tipus (int, char, long, float, etc...), identificada amb un determinat nom, on cadascun dels valors és accedit en funció de la posició que ocupa dintre de la llista, començant normalment per la posició zero.

Una col.lecció segons la RAE és:

----
Conjunto ordenado de cosas, por lo común de una misma clase y reunidas por su especial interés o valor.
----

Una col.lecció segons la definició de Java, és:

----
A collection represents a group of objects, known as its elements. Some collections allow duplicate elements and others do not. Some are ordered and others unordered (...)
----

(Pendent)

== Excepcions

=== Introducció

Abans de començar amb la definició d'excepció a Java, s'ha d'aclarir que és un error de programari. Segons la definició formal d'error (_RAE_):

----
ERROR: Concepto equivocado o juicio falso. Acción desacertada o equivocada. Cosa hecha erradamente. Diferencia entre el valor medido o calculado y el real."
----

La definició d'error de programari és també un concepte ampli. Per tant, obviant els errors d'anàlisi i/o disseny d’un determinat programari, ens centrarem únicament en els errors durant la fase de desenvolupament. Segons la _Viquipèdia_,

----
Error de programari: un error o defecte que causa un resultat incorrecte o inesperat en un programa o sistema, o que es comporti de forma no prevista. 
----

Llavors, concretant encara més, podem classificar els errors de programari segons la fase del desenvolupament en la que es produeixen:

*Errors de compilació*: Es produeixen quan la sintaxi del codi és incorrecte i el compilador o l’intèrpret no pot traduir-lo a codi executable. Normalment el procés queda interromput i no es genera el codi executable fins que la codificació és totalment correcta. Actualment la majoria dels IDE ja prevenen d’aquests errors abans que es produeixin.

*Errors d’execució*: Es produeixen quan el flux del programa queda interromput per una situació no prevista. Aquesta situació por estar provocada principalment per dos factors.

Factors intrínsecs:: Una combinació d’instruccions que porten a un estat indefinit del sistema. P.ex una divisió per zero, una crida al mètode d’un objecte null, un accès fora dels límits d’un array, etc...

Factors extrínsecs:: La impossibilitat de portar a terme una acció que depèn d’un sistema extern cambiant. P.ex. un recurs de xarxa no disponible, no tenir permisos per desar al disc, accedir a un directori que ja no existeix, etc...

=== La gestió d'errors "clàssica"
Quan es desenvolupa una determinada funcionalitat, aquesta acostuma a rebre entrades i a fer alguna cosa amb elles (les processa). Aquestes entrades poden ser dades proveïdes per l'usuari, per altres sistemes externs (una unitat de disc, una unitat de xarxa, una base de dades,...) o bé poden ser crides a d'altres funcions que proporcionen determinats valors. Aquestes situacions en les que el flux d'execució espera valors d'entrada que són indeterminats en temps (no sabem quan trigaràn en estar disponibles) i en forma (no sabem si seràn valors compatibles amb el que esperem o no) acostumen a ser un punt on el flux d'execució queda interromput i el processador emet un error de tipus _"error de entrada/sortida"_, _"divisió per zero"_ , _"error de conversió: s'esperava un enter"_, etc... Un cop el flux d'execució ha quedat interromput, l'aplcació queda interrompuda. PEr tant, la persona que desenvolupa ha de pensar previament en totes aquelles situacions que poden afectar al codi que desenvolupa.

P.ex., imaginem que estem desenvolupant una aplicació on l'usuari pot carregar d'un sensor de temperatura remot (de fet és una URL a Internet) que conté, de forma seqüencial, tota una sèrie de temperatures mitjanes d'una determinada localització, una per cada dia de l'any actual. Llavors, les processa emetent una tempertura mitjana per cada mes de l'any.


=== Java.Throwable
La classe _java.lang.Trowable_ és la superclasse de la qual depen tot el sistema de gestió d'errors i excepcions de Java. És la base d'un mecanisme de gestió de situacions anòmales

=== Java.lang.Error
Java classifica els errors d'execució en errors i excepcions.

La classe _java.lang.Error_ modela un tipus de situació no recuperable en temps d'execució. Acostumen a estar relacionats amb problemes fora de l'ambit del codi que s'executa. Tenen a veure  amb problemes de la màquina virtual de Java (JVM) on el programador no té pràcticament cap control. La seva definició és la següent:

----
An Error is a subclass of Throwable that indicates serious problems that a reasonable application should not try to catch. Most such errors are abnormal conditions
----

Normalment aquests errors no son recuperables i deixen l'execució en un estat indefinit o directament interromput. Per exemple, anomalies en l'assignació de memòria virtual de la JVM, problemes d'accès a les llibreries necessàries per a executar el codi, problemes amb el sistema operatiu sobre el qual s'executa la JVM, etc... 

=== Java.lang.exception

La classe _java.lang.exception_ modela un tipus d'excepció dintre de l'ambit del codi que s'està executant, de forma que el programador pot preveure quina serà la situació anómala i quina serà l'acció a emprendre en cas de que això passi.

== Expressions Regulars

(Pendent)


== Apendix

=== Fonts d'informació addicionals

* Llibreries Java: https://www.scientecheasy.com/2020/06/packages-in-java.html

* Excepcions a Java: https://www.tutorialspoint.com/java/java_exceptions.htm

* Classe Throwable: https://docs.oracle.com/javase/7/docs/api/java/lang/Throwable.html


