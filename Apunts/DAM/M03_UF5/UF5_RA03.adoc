== Interfícies gràfiques d’usuari

Una interfície gràfica d'usuari o GUI és un programa informàtic que actua com a interfície d'usuari (permetre la interacció entre màquina i humà) utilitzant un conjunt d'imatges i objectes gràfics per a representar la informació i per a captar totes les possibles interaccions amb l'usuari.

=== Abans de JavaFX
Fins a l'arribada cde JavaFX, les llibreries estàndard per a desenvolupar interfícies gràfiques d'usuari en Java es denominaven *JFC* (_Java Foundation Classes_). 

Aquest framework gràfic, implementat el 1997, permetia la creació d'interfícies gràfiques d'usuari (GUI) basat en el  conjunt *Abstract Window Toolkit* (AWT), *Swing* i *Java 2D* i proporcionava una GUI independent de la plataforma i portable a qualsevol sistema. L'inconvenient principal era que a l'estar basat en AWT, les crides als elements gràfics es realitzaven sobre la API nativa del sistema gràfic i per tant la visualització canviava d'un sistema a l'altre (Windows, Linux o MAC) en funció de l'estètica dels seus controls.
Paralel.lament, *Swing* (_javax.awt_), disponibles des de la versió 2.1 de Java, proporcionava una col.lecció de llibreries d'elements de control gràfic per a la construcció de GUI's (_Grafic User Interface_). 

Les principals característiques de *swing* són:

* Millores funcionals respecte a AWT.
* Programat totalment amb Java i sense elements externs, la qual cosa permet una portabilitat total.
* Unificació de l'aspecte gràfic, atès que no depèn dels controls específics de cada sistema amfitrió.
* Els elements s’identifiquen per una “J” al començament del nom del control: JLabel, JButton, JText, etc… i tots ells hereten de la classe *JComponent*.

=== Amb JavaFX

JavaFX (_openjfx.io_)és un projecte de codi obert format per un conjunt de llibreries per a la creació d’aplicacions d’escriptori i aplicacions mòbils.
A partir de la versió 11 de Java, aquestes llibreries ja no formen part del paquet estàndard de Java SE i es distribueixen com a llibreries  independents anomenades *OpenJFX* (https://openjfx.io/), amb sistema de versionat independent.

.JavaFX Roadmap i versió 16
image:JavaFX_Roadmap.png[]

Els seus avantatges enfront de les GUI anteriors són:

* Més riquesa i varietat de components GUI
* Consistent amb el patró MVC via fitxers FXML
* Gestió d’esdeveniments més consistent que amb Swing
* Capacitat per a mostrar pàgines web dintre d’aplicacions JavaFX via WebView
* Suporta dispositius tàctils i interacció avançada com rotació, zoom, gestos, etc…
* La seva integració amb Netbeans i Maven redueixen considerablement el temps d’aprenentatge d’aquesta tecnologia.

.Amb JavaFX es poden crear interfícies amb un gran rendiment visual
image:jfx_demo.gif[]

=== Integració de JavaFX amb Maven

Per integrar JavaFX als nostres projectes Maven, hem d'incloure, com a mínim, les següents dependències al *pom.xml*:

[source, xml]
----
<dependency>
    <groupId>org.openjfx</groupId>
    <artifactId>javafx-controls</artifactId>
    <version>16</version>
</dependency>
<dependency>
    <groupId>org.openjfx</groupId>
    <artifactId>javafx-fxml</artifactId>
    <version>16</version>
</dependency>
----

També hem d'instalar una eina anomenada *SceneBuilder* que ens ha de permetre editar les vistes en format _fxml_.

image::sceneBuilder.png[]

[TIP]
====
Les instruccions les teniu al document "Preparació de l'entorn de treball", apartat "Scene Builder"
====

=== Components d'una aplicació JavaFX

La infraestructura subjacent que permet la construcció d'un entorn gràfic amb JavaFX es compon de tres elements bàsics:

image:: jfx1.png[]

==== Escenari
Gestionat per la classe *javafx.stage.Stage*. 

És el contenidor de nivell superior d'una aplicació JavaFX i tota la resta de components estan agregats a una instància de Stage.

Hereta directament de la classe *javafx.stage.Window*, que alhora depèn directament de l'entorn gràfic on s'executa. 

En una mateixa aplicació es poden generar tants escenaris com es vulguin. Cadascun d'ells correspondrà a una finestra emergent o pantalla independent, en funció del sistema amfitrió.

[TIP]
====
Per exemple, al cas d'un telèfon mòbil, la classe *Stage* habilita tota la pantalla.
====

Al moment d'inicialitzar una aplicació _JavaFX_, sempre existeix un primer escenari generat pel mètode _launch()_ (veure apartat "Inicialització d'una aplicació JavaFX").

==== Escena

Gestionat per la classe *javafx.scene.Scene*.

Una escena pertany a un escenari i una escena conté els controls (components gràfics) que formen la GUI d'una determinada visualització anomenada *nodes*. 

Un escenari pot contenir més d'una escena, però solament una escena és visible en un moment donat a un escenari.

==== Element

Gestionat per la classe *javafx.scene.Node* és l'objecte base base per a organitzar tota l'estructura d'elements gràfics d'una escena. 

Una escena té zero o més nodes i un sol node _arrel_.

La composició dels elements en una escena s'anomena *Scene Graph*. És un arbre amb un sol node que actua com a _node arrel_ i amb n nodes _branca_, que alhora contenen altres elements o bé nodes _fulla_, que són els elements finals i que no contenen més subelements. Les principals classes que ho representen són:

* *javafx.scene.Parent*: Element pare, que defineix tres subclasses de node:
** *javafx.scene.Group*: Classe que conté una llista de controls que es renderitzen en ordre a mida que van apareixent.

** *javafx.scene.layout.Region*: Classe que gestiona elements de tipus _Layout_, que normalment són regions rectangulars que organitzen els elements en funció d'una lògica predefinida. A una regió també se li pot aplicar estils *CSS*, fons gràfic, etc... Alguns exemples:

*** *BorderPane*: divideix el contenidor en cinc parts (nord, sud, est, oest i centre)
HBox/VBox: permeten ubicar els elements en una fila/columna

*** **StackPane**: superposen els elements un a sobre de l'altre
GridPane: col·loca els components en files i columnes

*** **FlowPane**: els elements es van afegint d'esquerra a dreta. En arribar al límit de panell, els elements següents s'ubiquen en una fila inferior

*** **TilePane**: similar a FlowPane, però als elements se'ls assignen les mateixes adreces i s'ubiquen en una quadrícula.

*** *AnchorPane*: (per defecte a Netbeans): permet ancorar els elements a la part superior, inferior, esquerra, dreta o central. En redimensionar la finestra, els elements mantenen la posició relativa al punt d'ancoratge.

** *javafx.scene.control.Control*: És la classe base per a representar un control visual individual. Tots els elements visuals amb els que l'usuari pot interactuar són subclases que hereten d'aquesta classe, com per exemple *Button*, *Slider*, *ToolBar*. A un control també se li poden aplicar estils CSS de forma específica. 

Per exemple:

[source, java]
----
Label lbl = new Label();
lbl.setStyle("-fx-background-color: red");
----

.Estructura dels elements de JavaFX
image:jfx2.png[]

=== Inicialització d'una aplicació JavaFX

Cada cop que una aplicació JavaFX s'executa, s'efectuen tota una sèrie d'operacions d'inicialització prèvies a les línies de codi del nostre programa. També, un cop finalitzat el nostre programa i abans de descarregar-ho tot de la memòria, també es realitzen una sèrie d'operacions. Els passos són els següents:

* L'aplicació s'inicia per el punt d'entrada definit com a _public static void main(String[] args)_, però en aquest punt JavaFX no està inicialitzat i no és possible encara mostrar cap element gràfic per pantalla.

* Es crida a *Application.launch()* per a construïr una instància de la classe Application.

* Es crida al mètode *init()* i si està sobreescrit, s'executa tot el que està allà. En aquest punt no disposem de GUI.

* Es crida al mètode *start()* i si està sobreescrit, s'executa tot el que està allà. En aquest punt ja disposem de GUI.

* Espera a que finalitzi l'aplicació, la qual cosa succeeix si es compleix alguna de les següents situacions:

** Es crida a _Plattform.exit()_
** La darrera finestra s'ha tancat

* Es crida al mètode _stop()

.Cicle de vida d'una aplicació de JavaFX
image:jfx4.png[]


=== Generació d'un projecte JavaFX a NetBeans

NetBeans automatitza la creació de projectes JavaFX a través de diverses plantilles o arquetips. Per a crear un projecte de tipus Maven i JavaFXML, s'ha de seleccionar _File -> New Project -> Java With Maven -> Java FXML Archetype_:

image:jfx11.png[]

JavaFX disposa de dues formes per a gestionar la vista i el controlador (veure apartat patró MVP):

* **JavaFX Application**: Genera un projecte java estàndard amb les funcionalitats de JavaFX habilitades. L'estructura de mòduls i mètodes és la mateixa que per a qualsevol altre projecte Java:
Application.launch (). Mètode que llança laplicació.
Start (Stage). Mètode que executa tot el codi definit per l'usuari.

*  **JavaFXMLApplication**: Genera un projecte java amb les funcionalitats de JavaFX en format FXML. L'estructura bàsica de mòduls i mètodes és la següent:

** FXMLDocument.fxml implementa la vista en format XML. Conté tots els elements visuals i referències als controladors.

** FXMLDocumentController.java implementa el controlador, defineix els receptors d'esdeveniments i tota la lògica associada.
JavaFXApplication.java implementa les rutines d'inici de l'aplicació, igual que en el format d'únic mòdul.

=== Patro MVP

El comportament de JavaFX no s’ajusta totalment patró Model Vista Controlador (MVC), sinó al MVP (Model-Vista-Presentador):

* La vista comunica al presentador les accions de l’usuari. La vista queda representada per fitxers amb esquema XML i de tipus FXML.
* El presentador (controller) actualitza el model (dades) o la vista en funció dels canvis. El controlador queda representat per fitxers en codi java.
* El model, representat per a una classe que té accès a les dades, informa al presentador dels possibles canvis en aquestes.

==== Relació entre vistes i controladors
Un controlador pot gestionar una o més vistes, però una vista únicament pot ser  gestionada per un controlador.
La relació entre la vista (fxml) i la classe controlador (.java) queda definida per l’atribut fx:controller de l’element root del fixter fxml:

[source, xml]
----
<VBox alignment="CENTER" spacing="20.0" xmlns="http://javafx.com/javafx/17" xmlns:fx="http://javafx.com/fxml/1" fx:controller="com.mycompany.mavenproject3.PrimaryController">
----

.Patró Model-Vista-Presentador
image:jfx3.png[]

=== Esdeveniments

Un esdeveniment és el medi o mecanisme amb el qual una classe interactua amb la resta de classes o aquestes amb l’usuari. Habitualment té la forma d'una un senyal asíncrona rebuda per una o més classes, les quals reaccionen a l'esmentat esdeveniment.

Cada cop que es realitzen accions sobre un control es genera un esdeveniment. P.ex., sobre un botó podem passar el ratolí per sobre, prémer un botó o altre, arrossegar i deixar un objecte sobre aquest, seleccionar-lo, deixar-lo anar, deixar parat el ratolí sobre aquest, etc...

La classe *javafx.event.Event* és la classe base per a tota la resta d’esdeveniments. Algunes subclasses de Event:

* DragEvent: quan arrosseguem i deixem amb el ratolí
* Keyevent: accions amb el teclat
* MouseEvent: quan fem clic amb el ratolí
* WindowEvent: accions amb les finestres com mostrar, tancar, minimitzar, ...

Quan un determinat control (botó, slider, etc…) genera esdeveniments, podem “atrapar-los” de forma separada mitjançant mètodes que implementin una determinada reacció. Això s’aconsegueix mitjançant *EventHandler* i una implementació repartida entre la vista (fixter FXML) i el controlador (codi Java).
A la vista, definim quins esdeveniments són "consumits" i quins mètodes són els encarregats de fer-ho:


A la vista, definim que l'esdeveniment *onAction*, corresponent a fer clic al botó, serà gestionat pel mètode *handleButtonAction* del controlador:
[source, xml]
----
 <Button fx:id="btn1" mnemonicParsing="false" onAction="#handleButtonAction" prefHeight="56.0" prefWidth="260.0" text="Escena 2 &gt;&gt;" BorderPane.alignment="CENTER" />
----

Al controlador, el mètode *handleButtonAction* realitza les accions necessàries:
[source, java]
----
 @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        Stage stage;
        Parent root;
        Scene scene;
        
        // Amb el mètode getSource identifiquem el control que ha generat l'esdeveniment
        if(event.getSource() == btn1){
            
            // recuperem la instancia de l'objecte stage corresponent a la vista, mitjançant el control btn1
            stage = (Stage) btn1.getScene().getWindow();
            
            // creem un node arrel amb la vista que volem mostrar
            root = FXMLLoader.load(getClass().getResource("FXMLDocument2.fxml"));
        }
        else{
            stage = (Stage) btn2.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        }
        
        // creem una nova escena a partir del node arrel
        scene = new Scene(root);
        
        // li assignem l'escena a l'escenari recuperat anteriorment
        stage.setScene(scene);
        
        //mostrem
        stage.show();
    }
----

=== Les classes Media i MediaPlayer

JavaFX també pot reproduir continguts multimèdia com vídeo o so mitjançant les classes *Player*, *MediaView* i *MediaPlayer*. Per a poder accedir a les llibreries, s'ha d'incloure la següent depèndencia de Maven via *pom.xml*:

[source, xml]
----
<dependency>
    <groupId>org.openjfx</groupId>
    <artifactId>javafx-media</artifactId>
    <version>16</version>
</dependency>
----

Concretament, la classe MediaPlayer permet reproduir fitxers d'audio  amb la següent codificació: AAC, MP3, PCM. També suporta diferents formats de fitxer com MP3 i WAV i diferents protocols de comunicació com FILE, HTTP, HTTPS i JAR (_java.net.JarURLConnection_).

Per a reproduir un MP3, les classes principals són *Media* i *MediaPlayer*. La primera és aquella que connecta amb la font de dades, ja sigui fitxer, https, etc... Un cop s'obté una instància de _Media_ amb la connexió a la font, la classe _MediaPlayer_ permet controlar la reproducció.

[source, java]
----
    String uriString = new File("C:\\Users\\Mike\\workspace\\b\\src\\hero.mp3").toURI().toString()
    MediaPlayer player = new MediaPlayer( new Media(uriString));
    player.play();
----

=== Annex 

Exemples de codi JavaFX: https://docs.oracle.com/javase/8/javafx/sample-apps/index.html

JavaDOC OpenJFX versió 16: https://openjfx.io/javadoc/16/

Classe *javafx.scene.media*: https://openjfx.io/javadoc/16/javafx.media/javafx/scene/media/package-summary.html






















































