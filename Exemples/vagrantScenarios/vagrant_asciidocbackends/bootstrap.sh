#!/bin/sh

apt-get -y upgrade
apt-get -y update

apt-get -y install openjdk-11-jdk asciidoc asciidoctor graphviz dblatex 

gem install asciidoctor-latex --pre
gem install asciidoctor-epub3 --pre
gem install asciidoctor-pdf -v 1.5.0.beta.6 --pre
gem install asciidoctor-diagram

gem install pygments.rb
gem install rouge
gem install coderay

gem install prawn

cp /vagrant/comandes.txt /home/vagrant/comandes.txt

