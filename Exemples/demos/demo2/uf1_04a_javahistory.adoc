== Breu història de Java 

Java és un llenguatge de programació de propòsit general *basat en classes* footnote:[La herència s'aplica definint classes enlloc de directament sobre els propis objectes] i *orientat a objectes*.

Està dissenyat per ser multiplataforma, és a dir, el codi Java ha de poder executar-se en qualsevol plataforma que suporti Java sens necessitat de recompilar-lo.

Actualment segueix sent unt dels llenguatges principals tant per la quantitat de codi escrit com per la quantitat d'ofertes de treball existents.

Des de la seva creació al 1995 Java ha anat evolucionant, a continuació es mostra un resum de les funcionalitats principals de cada versió:

Java 1.0 (1996)::
Primera versió publica de Java. Implementava 212 classes organitzades dins
vuit paquets. La plataforma de Java sempre ha emfatitzat la retrocompatibilitat, de fet, la majoria de codi escrit per Java 1.0 segueix funcionant en Java 12.

Java 1.1 (1997)::
S'afegeixen les *"inner classes"* i la primera versió de la API *Reflection*. La mida de la plataforma es dobla.

Java 1.2 / Java 2 (1998)::
S'afegeix la API de *col·leccions*, (llistes, mapes, conjunts,...). La gran quantitat de millores de la versió porta a Sun renombrar  la plataforma amb el nom de *Java 2*. Es triplica la mida de la plataforma Java.

Java 1.3 (2000)::
Versió de manteniment basada en la resolució de "bugs" i en la millora de la estabilitat.


Java 1.4 (2002)::
Una altra de les versions importants. S'afegeix la API de *i/o*, el suport per *expressions regulars*, *XML* i *XMLST*, per *SSL*, la APiI de "*logging*" i el suport per a *criptografia*.

Java 5 (2004)::
Apareixen els *genèrics*, els *enums*, les *anotacions*, els mètodes amb arguments variables, *autoboxing* i el bucle *for each*. Aquesta versió mantenia 3562 classes i interfícies en 166 paquets.

Java 6 (2006)::
Versió de manteniment basada en la resolució de "bugs" i en la millora de la estabilitat. S'implementa la possibilitat que llenguatges de script interoperin amb Java.

Java 7 (2011)::
Primer llançament de Java sota la propietat de *Oracle*. S'introdueix la API *NIO*

Java 8 (2014) (LTS)::
Aquesta versió incorpora els canvis de llenguatge més importants des de Java 5. S'introdueixen les *expressions lambda* i s'adeqüen les classes existents, principalment les col·leccions, per l'ús d'aquestes. S'introdueix una nova versió de la API pel tractament de *dates* i *temps* i millores importants a les llibreries que gestionen la concurrència.

Java 9 (2017)::
S'introdueix el concepte de *modularitat* a la plataforma que permet empaquetar les aplicacions en *unitats de desplegament*. Es modifica l'algoritme de la màquina virtual per defecte i apareix una nova API per treballar amb *processos*.

Java 10 (Març 2018)::
És la primera versió en la nova política de llançaments per Java, una nova versió cada sis mesos amb vigència de només sis mesos i una versió LTS cada dos anys.

Java 11 (LTS) (Setembre 2018):: Primera versió LTS des de Java 8. Afegeix poques funcionalitats a Java, alguns canvis interns i una nova API per HTTP.

Java 12 (Març 2019):: Nou recol·lector de brossa amb duració constant independentment de la mida  de la memòria.
	