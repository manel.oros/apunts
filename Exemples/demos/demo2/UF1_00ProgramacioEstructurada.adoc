:!sectnums:
= UF1 Programació estructurada

[quote, Matthias Felleisen, professor i autor en les ciències de la computació.]
____
Bad programming is easy. Idiots can learn it in 21 days, even if they are dummies.
____

[quote, Martin Fowler, enginyer de software britànic especialitzat en anàlisi i disseny orientat a objectes.]
____
Any fool can write code that a computer can understand. Good programmers write code that humans can understand.
____

[quote, Eric S. Raymond, Autor del llibre "la catedral i el basar" famós assaig sobre el software de codi obert.]
____
Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.
____

[quote, George Carrette]
____
First learn computer science and all the theory. Next develop a programming style. Then forget all that and just hack.
____

<<<
include::uf1_01decret.adoc[]

:sectnums:

<<<
include::uf1_02introduccio.adoc[]

<<<

include::uf1_03paradigmes.adoc[]

<<< 
include::uf1_04a_javahistory.adoc[]

<<<
include::uf1_04b_partsDunProgramaJava.adoc[]

<<<
include::uf1_05MostrarCadenes.adoc[]

<<<
include::uf1_06comentarisILlegibilitat.adoc[]

<<<
include::uf1_07variablesjava.adoc[]

<<<
include::uf1_08operadors.adoc[]

<<<
include::uf1_09constants.adoc[]

<<<
include::uf1_10aFluxos.adoc[]

<<<
include::uf1_10bConsoleInputAndOutput.adoc[]


// <<<
// include::uf1_11a_IntroduccioProcesSoftware.adoc[]

<<<
include::uf1_11b_algoritmes.adoc[]

<<<
include::uf1_12estructuresDeSeleccio.adoc[]

////
<<<
include::uf1_13EstructuresDeRepeticio.adoc[]

<<<
include::uf1_14TipusDeDadesCompostes.adoc[]

<<<
include::uf1_15TractamentDeCadenes.adoc[]
////

<<<
include::uf1_99bibliography.adoc[]
