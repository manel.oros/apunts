== Tipus de dades Compostes

Un *tipus de dada compost* és aquell que permet emmagatzemar més d’un valor dins d’una única variable. 

=== ((Matrius))

Una matriu és una estructura de dades de *longitud fixa* que permet emmagatzemar més d'un valor del *mateix tipus de dades*.

Les matrius s'utilitzen per emmagatzemar una col·lecció de dades del mateix tipus. Enlloc de declarar variables individuals, com per exemple, _nom1_, _nom2_, _nom3_, ...., _nom99_ podríem declarar na matriu anomenada _noms_ i utilitzar _noms[0]_, _noms[1]_, _noms[2]_, ...., _noms[99]_ per representar les variables individuals.

==== Declaració de variables de tipus Matriu

Les matrius són un tipus de dades en Java i per tant es poden declarar variables de tipus matriu. 

* La sintaxis de declaració de les variables de tipus matriu és especifica d'aquesta estructura de dades.

Sintaxis per la declaració de matrius:

_tipus-de-dades-de-la-matriu_[] _nom-de-la-variable_;

Exemples:

[source, java]
----
int[] numeros; // Declara una matriu de ints
String[] noms; // Declara una matriu de Strings
----

[NOTE]
====
Alternativament, les matrius es poden declarar posant els claudàtors després del nom de la variable enlloc de després del tipus. En general *es prefereixen els claudàtors després del tipus*.

Per tant són correctes:

[source, java]
----
int[] m; // és equivalent a
int m[];
----
====

===== Creació de matrius

Una matriu és un tipus per referència i per tant:

* La declaració de la variable no reserva espai a la memòria per emmagatzemar la matriu, només es reserva un espai per guardar la posició de memòria dins la que s'emmagatzemarà la matriu.
* Una variable matriu sense inicialitzar contindrà un valor especial anomenat *null*, en el moment en que s'inicialitzi contindrà l'adreça de memòria on es troba la matriu.
* Necessitarà l'operador _new_ per a crear l'objecte.

[IMPORTANT]
====
Per crear un objecte matriu caldrà saber la seva longitud en el moment de la creació. 

Un cop creada la matriu, la seva longitud no es pot modificar.
====

Sintaxis per a la creació d'una matriu:

_nom-de-la-variable_ = _new_ _tipus-de-dades-de-la-matriu_[_longitud-de-la-matriu_];

Exemples:

[source, java]
----
int[] numeros; // Declaració de la matriu
numeros = new int[10];  // Creació d'una matriu de 10 elements de tipus int

String noms[] = new String[100]; // Declaració i creació d'una matriu de 100 Strings
----

==== Assignació i lectura de valors d' una matriu

[WARNING]
====
Els elements d'una matriu estan indexats a partir del zero.

Per tant una matriu _m_ de 10 elements s'indexa de 0 a 9. ( _m[0]_, _m[1]_, ...., _m[9]_ )

Accedir a un element fora del rang de la matriu donarà un error d'execució.
====

===== Assignació manual de valors a una matriu

Un cop disposem d'una matriu creada podem omplir-la de la següent manera:

[source, java]
----
int[] valors;
valors = new int[5];

valors[0] = 4;
valors[1] = 56;
valors[2] = 43;
valors[3] = 12;
valors[4] = 10;
----

===== Assignació manual de valors a una matriu amb l'ajuda d'un bucle

És fàcil omplir una matriu amb l'ajuda d'un bucle:

[source, java]
----
int[] valors;
valors = new int[5];

// Assignem a cada posició de la matriu la seva posició al quadrat.
// valor[0] = 0*0, valor[1] = 1*1, ....
for(int i = 0; i < 5; i++) {
    valors[i] = i*i;
}
----

===== Assignació de valors d'una matriu amb un inicialitzador

Els inicialitzadors permeten assignar valors concretats a les matrius en el moment de la seva declaració.

Per exemple:

[source, java]
----
char[] valors = {'a', 'b', 'c', 'd', 'e'};
----

equival a 

[source, java]
----
char[] valors = new valors[5];

valors[0] = 'a';
valors[1] = 'b';
valors[2] = 'c';
valors[3] = 'd';
valors[4] = 'e';
----

===== Lectura de valors d'una matriu

Podem llegir els valors d'una matriu de la següent manera.

[source, java]
----
int[] valors = {2, 3, 4, 5};

for(int i = 0; i < 4; i++) {
    System.out.print("valors[" + i + "] = " + valors[i];
}
----

==== Bucles ((for-each))

Java suporta una construcció especial de bucle anomenada bucle for-each que permet recórrer una col·lecció, en particular una matriu, sense utilitzar una variable index.

El següent codi mostra tots els elements de la matriu _matriu_.

[source, java]
----
// matriu és una matriu d'elements double
for (double element: matriu) {  
    System.out.println(element);
}
----

==== Longitud d'una matriu, propietat length

Accedir a un element d'una matriu fora de rang produeix un error de tipus *((ArrayIndexOutOfBoundsException))*.

La propietat *length* d'un objecte matriu permet saber la capacitat de la matriu.

Exemple:

[source, java]
----
int[] matriu = {1, 2, 3, 4, 5};

System.out.println("La matriu pot contenir " + matriu.length + " elements");

for (int i = 0; i < matriu.length - 1; i++) {
    System.out.println(matriu[i]);
}
----

=== Operacions habituals amb les matrius

Al treballar amb matrius apareixen una sèrie de necessitats de forma habitual. 

* Cerca d'elements dins d'una matriu
* Clonar una matriu
* Canviar la mida d'una matriu
* Ordenar els elements dins d'una matriu
* Determinar si dues matrius són iguals

==== Cerca d'un element dins d'una matriu

Existeixen diferents algoritmes per tractar el problema de la cerca de valors dins d'una matriu, alguns d'ells necessiten preparar la matriu d'alguna manera, per exemple ordenar-ne els elements abans de fer la cerca.

Veurem la implementació més simple d'una cerca en una matriu, anomenada cerca directa.

L'algoritme consisteix en desplaçar-se seqüencialment pels elements de la matriu i anar comprovant si hi ha una coincidència.

[source, java]
----
public class Cerca {

    public static void main(String[] args) {
        float[] valors = {2f, 5.5f, 9f, 10f, 4.9f, 8f, 8.5f, 7f, 6.6f, 5f, 9f, 7f};
        float valorCercat = 10f;
        boolean trobat = false; // true si s'ha trobat el valor
        int posicio = 0;  // Posició de la matriu on es fa la comprovació

        while ((posicio < valors.length) && (!trobat)) {
            if (valors[posicio] == 10) {
                trobat = true;
            }
            posicio = posicio + 1;
        }

        if (trobat) {
            System.out.println("El valor " + valorCercat + " s'ha trobat");
        } else {
            System.out.println(valorCercat + " no existeix a la matriu");
        }
    }
}
----

==== Clonar una matriu

Considerem el següent fragment de codi:

[source, java]
----
int m[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
int n[];
----

Volem clonar la matriu _m_ a la matriu _n_.

[WARNING]
====
Fer `n = m;` no clona la matriu.

Recordeu que les matrius són tipus per referència i per tant una variable de tipus matriu no emmagatzema al matriu sencera, sinó que conté l'adreça de memòria on es troba l'objecte matriu, en Java això es coneix com una *referència*.

Per tant, l'assignació anterior modifica el contingut de _n_ posant-hi el mateix que conté _m_.

_m_ conté l'adreça de memòria on s'ubica la matriu, per tant amb l'assignació anterior _n_ també conté l'adreça de memòria on es troba la matriu.

El punt és que de matriu en seguim tenint una, el que tenim són dues variables que en contenen la ubicació en memòria. Per tant modificar la matriu m o modificar la matriu n és exactament modificar la mateixa matriu.

Vegem-ho amb un exemple:

[source,, java]
----
public class TestReferencies {

    public static void main(String[] args) {
        int[] m = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] n = m;

        /* Modifiquem la matriu a través de n ,
           mirem la matriu a través de m 
           i veurem els canvis.
         */
        n[0] = n[0] * 100;

        for (int i = 0; i < m.length; i++) {
            System.out.print(m[i] + "  ");
        }
    }
}
----

----
100  2  3  4  5  6  7  8  9  10 
----
====

Per clonar una matriu cal fer-ho element a element.

Exemple:

[source, java]
----
int[] sourceArray = {2, 3, 1, 5, 10};
int[] targetArray = new int[sourceArray.length];
for (int i = 0; i < sourceArray.length; i++) {
    targetArray[i] = sourceArray[i];
}
----

[WARNING]
====
Si els elements de la matriu fossin elements per referència tornaríem a tenir problemes!!.
====

==== Canvi de mida

És impossible canviar de mida una matriu ja creada. 

Per modificar la mida d'una matriu cal fer-ne una de nova amb la nova longitud i copiar els elements de l'antiga manualment.

Per exmple, suposem que tenim una matriu de 5 elements i la volem fer créixer fina a 10.

[source, java]
----
int[] matriu = {2, 3, 1, 5, 10};
int[] novaMatriu = new int[10];  // Nova dimensió
for (int i = 0; i < matriu.length; i++) {
    novaMatriu[i] = matriu[i];
}  // Aquest bucle només omple els 5 primers elements de novaMatriu
----

==== Ordenació dels elements de la matriu

Per ordenar una seqüència de valors hi ha diversos algoritmes. Els més eficients són força complexos i es troben més enllà dels objectius d’aquest mòdul. 

A mode d'exemple analitzarem un dels algoritmes menys eficients dedicat a l'ordenació anomenat *((SelectionSort))* o algoritme de selecció.

Algoritme: (Suposem una ordenació ascendent)

. Buscar l'element més baix de la matriu
. Un cop trobat intercanviar-lo amb l'element situat en primera posició. +
En aquest punt tenim el menor element al principi de la matriu.
. Buscar l'element més baix de la matriu però començant per la segona posició.
. Un cop trobat intercanviar-lo amb l'element situat en segona posició. +
En aquest punt tenim les dues primeres posicions amb els elements més petits.
. Buscar l'element més baix de la matriu però començant per la tercera posició.
. ....
. Anem repetint el procés fins a arribar al penúltim element de la matriu.

Implementació:

[source, java]
----
public class SelectionSort {
  public static void main (String[] args) {
    float[] matriu = {5.5f, 9f, 2f, 10f, 4.9f};

    for (int i = 0; i < matriu.length - 1; i++) {
      for(int j = i + 1; j < matriu.length; j++) {
        //La posició tractada té un valor més alt que el de la cerca. Els intercanviem.
        if (matriu[i] > matriu[j]) {
          float aux = matriu[i];
          matriu[i] = matriu[j];
          matriu[j] = aux;
        }
      }   
    }

    System.out.print("L'array ordenat és: [");
    for (int i = 0; i < matriu.length;i++) {
      System.out.print(matriu[i] + " ");
    }
    System.out.println("]");
  }
}
----

==== Comprovar si dues matrius són iguals

Si tenim dues matrius _m1_ i _m2_ i es calcula _m1_ == _m2_ estarem coprovant si la referència _m1_ i la referència _m2_ apunten al mateix objecte matriu, no estarem coprovant si el contingut de les matrius _m1_  ni _m2_ és el mateix.

Pr exemple:

[source, java]
----
int[] m1 = {1, 2, 3};
int[] m2 = {1, 2, 3};
----

En aquest case _m1_ == _m2_  és false, ja que m1 i m2 representen dos objectes diferents a la memòria de l'ordinador i per tant contenen adreces de memòria diferents.

[source, java]
----
int[] m1 = {1, 2, 3};
int[] m2 = m1;
----

En aquest cas _m1_ == _m2_ és true ja que tant _m1_ com _m2_ són referències al mateix objecte matriu, en realitat de matriu només n'hi ha una pero accessible des de dues variables.

Si es vol comprovar si *el contingut de les matrius és el mateix* tenim dues possibilitats.

. Recórrer manualment les dues matrius i comparar els valors respectius.
. utilitzar el mètode *_equals_* de la classe *Arrays*.

=== Matrius multidimensionals

Donat que una matriu pot contenir elements d'un tipus de dades qualsevol res impedeix posar dins de cada element d'una matriu una altra matriu. De fet es pot seguir així indefinidament, podríem posar una matriu de matrius dins de cada element de la matriu, etc...

Ens centrarem en el cas bidimensional, és a dir matrius de matrius.

Estenent les restriccions que compleixen les matrius d'una dimensió al cas de les matrius multidimensionals:

* En una mateixa dimensió, totes la matrius tindran exactament la mateixa mida.
* El tipus de dada que es desa a les posicions de la darrera dimensió serà el mateix per a tots. 
 
==== Declarar matrius de dues dimensions

La sintàxis per declarar una matriu bidimensinal és:

_tipus-de-dades_[][] _nom-de-la-matriu_;

Exemple:

[source, java]
----
int[][] matriu;
----

==== Creació de matrius bidimensionals

La sintaxi per crear una nova matriu bidimensional és:

_nom-de-la-matriu_ = new _tipus-de-dades_[_longitud-de-la-matriu-contenedora_][_longitud-de-les matrius-contingudes_]

Per exemple:

.Exemple 1
[source, java]
----
int m[][] = new m[2][3];

m[0][0] = 1;
m[0][1] = 1;
m[0][2] = 1;

m[1][0] = 2;
m[1][1] = 2;
m[1][2] = 2;
----

Equivalentment,

.Exemple 2
[source, java]
----
int m[][] = {{1, 1, 1}, {2, 2, 2}};
----

Podem recórrer la matriu amb dos bucles i mostrar-ne el contingut:

[source, java]
----
for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 3; j++) {
        System.out.print(m[i][j]) + " ");
    }
    System.out.println(); // Forcem un salt de línia
}
----

----
1 1 1
2 2 2
----
