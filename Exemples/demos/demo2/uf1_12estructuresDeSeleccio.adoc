== Estructures de sel·lecció

=== Operadors relacionals

El operadors relacionals s'utilitzen per comprovar diferents condicions com ara si dos valors són iguals o un és més gran que l'altre. 

* El resultat obtingut amb un operador relacional és un valor de tipus *boolean*, és a dir, *true* o *false*.
* Els dos valors que relaciona un operador relacional han de ser compatibles.
* La majoria d'operadors relacionals *no funcionen amb Strings*. 
+
[WARNING]
====
Els operadors *==* i *!=* es poden utilitzar en Strings però *NO TENEN EL COMPORTAMENT ESPERAT !!!*. Per tant no els utilitzarem per a aquest tipus de dades.
====
* Per comprovar la igualtat entre cadenes s'utilitza el mètode *((equals))* de la classe String.
+
[source, java]
----
String fruita1 = "Poma";
String fruita2 = "Pera";
System.out.println(fruita1.equals(fruita2));
----

.Llista dels operadors relacionals en Java
[cols="1,1,2,1,1", options="autowidth"]
|====
| Operadors | Descripció | Tipus | Exemple | Resultat
| == | igual que | binari | 3 == 2 | false
| equals | igual que [per tipus String] | "hola".equals("hola") | true
| != | no igual que | binari | 3 != 2 | true
| > | major que | binari | 3 > 2 | true
| >= | major o igual que | binari |3 > 2 | true
| <  | menor que | binari | 3 < 2 | false
| <= | menor o igual que | binari | 3 <= 2 | false
|====

[WARNING]
====
Els 128 primers caràcters de la codificació unicode equivalen als de la codificació ASCII, això vol dir que el codi associat a cada caràcter creix alfabèticament. 

No obstant els caràcters especifica del català, accents, etc... no formen part dels primers 128 caràcters unicode.

Pe tant es poden comparar caràcters sempre i quan es tingui present que s'estan comparant les codificacions numèriques unicode i no els caràcters com a tals.
====

=== Estructura ((if))

La estructura *if* executa el següent bloc d'instruccions si i només si la condició proporcionada és vertadera.

.Estructura if
[plantuml,file="images/uml/if.png"]
....
start

if (condició) then (true)
  :cos del bloc if;
endif

stop
....

[source,java]
----
if (condició) {
    ....
} 
----

Exemples:

[source, java]
----
if (x>0) {
    System.out.println("x és positiu");
}
----

Si el bloc d'instruccions a executar conté una sola instrucció no cal posar parèntesis.

[source, java]
----
if (x>0) 
    System.out.println("x és positiu");
----

=== Estructura ((if-else))

La estructura *if-else* decideix quin bloc d'instruccions executar en funció de si la condició proporcionada és vertadera o falsa.

.Estructura if-else
[plantuml,file="images/uml/ifelse.png"]
....
start

if (condició) then (true)
  :cos del bloc if;
else (false)
  :cos del bloc else;
endif

stop
....

[source,java]
----
if (condició) {
    ....
} else {
    ....
}
----

Exemples:

[source, java]
----
if (x>0) {
    System.out.println("x és positiu");
} else {
    System.out.println("x és negatiu");
}
----



.Colocació dels parèntesis
****
Hi ha dues maners habituals de sangrar i posar els parèntesis en una estructura if-else.

[source, java]
----
if (nombre % 2 == 0) 
{
    System.out.println(nombre + " és parell");
}
else
{
    System.out.println(nombre + " és senar");
}
----

i

[source, java]
----
if (nombre % 2 == 0) {
    System.out.println(nombre + " és parell");
} else {
    System.out.println(nombre + " és senar");
}
----

****

=== Estructures if-else aniuades

Les estructures if-else es poden aniuar per representar condicions més complexes.

.Estructures if-else aniuades
[plantuml,file="images/uml/ifelseaniuades.png"]
....
start
if (condició A) then (true)
  :cos condició A;
else (false)
    if (condició B) then (true)
        :cos condició B;
    else (false)
        if (condició C) then (true)
            :cos condició C;
        else (false)
            if (condició D) then (true)
                :cos condició D;
            else (false)
                :cos else condició D;
            endif
        endif    
    endif
endif
stop
....

Per exemple:

[source, java]
----
if (x == 0) {
    System.out.println("x és zero");
} else {
    if (x > 0) {
        System.out.println("x és positiu");
    } else {
        System.out.println("x és negatiu");
    }
}
----

=== Estructures if-else encadenades

Les estructures if-else encadenades no representen un nou tipus d'estructura en Java, es tracta simplement d'un conjunt de sentències if-else niuades. Però representen una idea diferent i es sangren de manera diferent per representar aquest fet.

.Estructrues if-else encadenades
[plantuml,file="images/uml/ifelseencadenades.png"]
....
start
if (condició A) then (true)
  :cos condició A;
elseif (condició B) then (true)
  :cos condició B;
elseif (condició C) then (true)
  :cos condició C;
elseif (condició D) then (true)
  :cos condició D;
else (false)
  :cos else;
endif
stop
....

Vegem-ho amb un exemple, +
Volem fer un programa que en funció d'un valor entrat per l'usuari s'executi una opció del menú o una altra.

Una opció seria la següent:

[source, java]
----
if (valor == 1) {
    ....  
} else { 
    if (valor == 2) {
        ....
    } else {
        if (valor == 3) {
            ....
        } else {
            if (valor == 4) {
                ....
            }
        }
    }
}
----

Peró és complicada d'entendre, té moltes sangries i és fàcil no saber un comencen i on acaben els blocs.

El codi anterior s'hauria d'escriure com:

[source, java]
----
if (valor == 1) {
    ....  
} else if (valor == 2) {
        ....
} else if (valor == 3) {
            ....
} else if (valor == 4) {
                ....
}
----

En aquest cas la sangria deixa deixa molt clar la intenció del codi i és més fàcil de llegir.

=== Variables de tipus interruptor "flag variables"

Per emmagatzemar un valor _true_ o _false_ fa falta una variable de tipus *boolean*,es pot crear de la següent manera:

[source, java]
----
boolean resulatTest;
resultatTest = true;
----

Com que els operadors relacionals avaluen un valor booleà es pot guardar el resultat d'una comparació en na variable boolean.

[source, java]
----
boolean esPositiu = (x > 0); // Els parèntesis són innecessaris però faciliten la lectura del codi.

boolean esParell = (n % 2 == 0);
----

Es poden utilitzar variables de tipus boolean com a condició en una estructura condicional.

[source, java]
----
if (esPositiu) 
    System.out.println("El valor era positiu en el moment de la comprovació");
    
if (esParell) {
    System.out.println("El valor era parell en el moment de la comprovació");
}
----

=== Operadors lògics

Els operadors lògics es poden utilitzar per crear expressions booleanes compostes.

.Llista dels operadors lògics o booleans en Java
[options="autowidth"]
|====
| *Operadors* | *Descripció* | *Tipus* | *Exemple* | *Resultat*
| ! | negació | unari | !true | false
| && | and curtcircuit | binari | true & true | true
| & | and | binari | true & false | false
| &#124; &#124; | or curtcircuit | binari | true &#124; &#124; false | true
| &#124; | or | binari | true &#124; false | false
| ^ | xor [or exclusiu] | binari | true ^ true | false
|====

[NOTE]
====
La diferència entre & i && és que en el segon cas si la primera proposició és falsa ja no arriba a avaluar-se la segona.

La diferencia entre | i || és que si la primera proposició es certa ja no arriba a avaluar-se la segona.
====

Exemples:

.Exemple operador !
[source, java]
----
int i = 12;
boolean esNegatiu;
if (!esNegatiu) {
    System.out.println("i és postiu");
}
if (!(i < 10)) {
    System.out.println("i és major que 10");
}
----

.Exemple operador &&
[source, java]
----
int numerador = 10;
int denominador = 3;
if ((denominador != 0) && (numerador/denominador > 2)) {
    System.out.println("El quocient és positiu és major que 2");
}
----

En el cas anterior com que l'operador és && enlloc de & si el denominador fos 0 fallaria la primera condició de l'if i la segona ja no s'avaluaria. +
Si l'operador fos & s'intentaria avaluar la segona condició encara que la primera hagués fallat, produint en aquest cas una divisió entre 0 i per tant un error d'execució ja que un valor d'infinit només es pot donar entre nombres de coma flotant.

.Exemple operador ||
[source, java]
----
float temperatura = 25f;
if ((temperatura <= 18) || (temperatura >= 29)) {
    System.out.println("La temperatura no és la adequada!!");
}
----

=== Estructura ((switch))

Una expressió de tipus *switch* executa blocs de codi en base al valor d'una variable o expressió.

[source,java]
----
switch (expressió-del-switch) {
    case valor1:
        ....
    case valor2:
        ....
    ....
    case valorN:
        ....
    default:
        ....
}
----

* La expressió del switch s'ha d'avaluar com un tipus _byte_, _short_, _char_, _int_, _enum_ o _String_.
* L'etiqueta *default* és opcional.

Funcionament de l'estructura switch:

* S'avalua l'expressió del switch.
* Si el valor de l'expressió del switch coincideix amb algun dels valors d'un case, l'execució comença des d'aquest punt i *s'executen totes les instruccions fins al final del switch*.
* Si el valor de l'expressió del switch no coincideix amb cap valor dels case, l'execució continua a partir de l'etiqueta default fins al final del switch.

Exemples:

.Exemple 1
[source, java]
----
int i = 10;
switch (i) {
    case 10: // Es troba coincidència de valors
        System.out.println("Deu"); // L'execució comença aquí.
    case 20:
        System.out.println("Vint");
    default:
        System.out.println ("No hi ha coincidència");
}
----

----
Deu
Vint
No hi ha coincidència
----

.Exemple 2
[source, java]
----
int i = 30;
switch (i) {
    case 10: 
        System.out.println("Deu"); 
    case 20:
        System.out.println("Vint");
    default:
        System.out.println ("No hi ha coincidència"); // L'execució comença aquí.
}
----

----
No hi ha coincidència
----

.Exemple 3
[source, java]
----
int i = 30;
switch (i) {
    case 10: // Es troba coincidència de valors
        System.out.println("Deu"); 
    default:
        System.out.println ("No hi ha coincidència"); // L'execució comença aquí.
    case 20:
        System.out.println("Vint");

}
----

----
No hi ha coincidència
Vint
----

=== Instrucció ((break))

La instrucció *break* interromp l'execució d'una clàusula switch. L'execució del programa segueix a partir de la següent instrucció després del switch.

.Exemple 1
[source, java]
----
int i = 10;
switch (i) {
    case 10: // Es troba coincidència de valors
        System.out.println("Deu"); // L'execució comença aquí.
        break;  // Transfereix l'execució fora de l'estructura switch.
    case 20:
        System.out.println("Vint");
        break;
    default:
        System.out.println ("No hi ha coincidència");
        // No és necessari posar un break en aquest punt però és recomana fer-ho
        break;
}
----

----
Deu
----

=== A vegades no utilitzar el break és útil

Tot i que en general l'ús de la instrucció _break_ és necessari en algunes situacions és útil no utilitzar-lo. 

Per exemple:

[source, java]
----
import java.util.Scanner;

public class JavaApplication69 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Vols guardar el fitxer? [s/n]");

        String resposta = in.next(); // in.nextChar() no existeix!!

        switch (resposta) {
            case "s":
            case "S":
                // Guardem fitxer
                break;
            case "n":
            case "N":
                // NO guardem el fitxer
                break;
            default:
                // Tornem a mostrar la pregunta
                break;
        }
    }
}
----

=== Operador ternari ?:

L'operador ternari retorna un valor si determinada condició és vertadera i retorna un altre valor si la mateixa condició és falsa.

La sintaxi de l'operador és:

*_condició_ ? _valorSiFals_ : _valorSiVertader_*

Per exemple:

.Exemple 1
[source, java]
----
int i = 10;
int j = 20;
int nombreMenor = (i < j ? i : j);

System.out.println(nombreMenor); // Escriu 10
----