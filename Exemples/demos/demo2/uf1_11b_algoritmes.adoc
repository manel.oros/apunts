== Programació estructurada

La programació estructurada és un paradigma de programació sorgit a la dècada de 1960, particularment del treball de Böhm i Jacopini, i un famós escrit de 1968: «Go To Statement Considered Harmful», de Edsger Dijkstra.

Els seus postulats es veurien reforçats, a nivell teòric, pel *teorema de la programació estructurada* i, a nivell pràctic, per l'aparició de llenguatges dotats d'estructures de control consistents i ben formades.

=== Orígens de la programació estructurada

A la fi dels anys 1970 va sorgir una nova manera de programar que no solament permetia desenvolupar programes fiables i eficients, sinó que a més aquests estaven escrits de manera que es facilitava la seva comprensió en fases de millora posteriors.

El *((teorema de la programació estructurada))*, proposat per Böhm-Jacopini, demostra que *tot programa pot escriure's utilitzant únicament les tres instruccions de control següents*:

Sequència:: Les instruccions ordenades s'executen una darrera l'altre, és a dir, seqüencialment.
Sel·lecció:: Una o varies instruccions, un bloc d'instruccions, s'executen depenent de l'estat del programa.
Iteració:: Una instrucció o un conjunt d'instruccions, un bloc d'instruccions, s'executen repetidament fins que el programa arriba a un estat determinat.

.Representació gràfica de les tres estructures bàsiques de la programació estructurada.
[cols="^.^,^.^,^.^"]
|====

a| .Sequència
[plantuml, file="images/uml/structuredSequence.png"]
....
skinparam monochrome true
scale 2.0
start
:instrucció]

:instrucció]
end
.... a| .Sel·lecció
[plantuml, file="images/uml/structuredCondition.png"]
....
skinparam monochrome true
scale 2.0
start 
if (condició) then (sí)
:instrucció]
else (no)
endif 
end
.... a| .Repetició
[plantuml, file="images/uml/structuredRepetition.png"]
....
skinparam monochrome true
scale 2.0
start
while (condició) is (sí)
:instrucció]
endwhile (no)  
end
....
|====

*Solament amb aquestes tres estructures es poden escriure tots els programes i aplicacions possibles*. Si bé els llenguatges de programació tenen un major repertori d'estructures de control, aquestes poden ser construïdes mitjançant les tres bàsiques citades.

==== Estructures de control derivades

La majoria de llenguatges de programació que suporten el paradigma de programació estructurada accepten més estructures de control que les anteriors, no obstant, cal entendre que les noves estructures de control *sempre podran ser construïdes mitjançant les tres estructures bàsiques citades*

En general s'accepten les dues estructures següents:

[options="header", cols="^.^,^.^"]
|====
| Nova estructura | Que s'obtè de la combinació
a| 
[plantuml, file="images/uml/structuredCondition1.png"]
....
skinparam monochrome true
scale 2.0
start 

if (condició <U+03a9>) then (sí)
    :instruccions A]
else (no)
        :instruccions B]
endif 
end
.... a|
[plantuml, file="images/uml/structuredCondition2.png"]
....
skinparam monochrome true
scale 2.0
start

if (condició <U+03a9>) then (sí)
    :instruccions A]
else (no)
endif 
if (NOT condició <U+03a9>) then (sí)
    :instruccions B]
else (no)
endif 
end
....
|====

[options="header", cols="^.^,^.^"]
|====
| Nova estructura | Que s'obtè de la combinació
a| 
[plantuml, file="images/uml/structuredRepetition1.png"]
....
skinparam monochrome true
scale 2.0
start 

repeat
  :instruccions A]
repeat while (condició <U+03a9>) is (sí)
end
.... a|
[plantuml, file="images/uml/structuredRepetition2.png"]
....
skinparam monochrome true
scale 2.0
start

:instruccions A]
while (condició <U+03a9>) is (sí)
    :instruccions A]
endwhile (no) 
end
....
|====

=== Algorismes

.Algorisme
****
En matemàtiques, lògica, ciències de la computació i disciplines relacionades, un algorisme *és un conjunt d'instruccions* o regles *definides i no-ambigües*, *ordenades* i *finites* que permet, típicament, *solucionar un problema*, realitzar un còmput, processar dades i dur a terme altres tasques o activitats.

Donats un estat inicial i una entrada, seguint els passos successius s'arriba a un estat final i s'obté una solució. Els algorismes són l'objecte d'estudi de l'algorísmia.
****

En la vida quotidiana, s'empren algorismes sovint per a resoldre problemes. Alguns exemples són les receptes de cuina o les instruccions sueques per muntar un moble. Alguns exemples en matemàtiques són l'algorisme de multiplicació, per a calcular el producte, l'algorisme de la divisió per a calcular el quocient de dos números, l'algorisme d'Euclides per a obtenir el màxim comú divisor de dos enters positius, o el mètode de Gauss per a resoldre un sistema d'equacions lineals.

=== Mitjans d'expressió d'un algorisme

Els algorismes poden ser expressats de moltes maneres, incloent el *llenguatge natural*, *pseudocodi*, *diagrames de flux* i *llenguatges de programació* entre altres. Les descripcions en llenguatge natural tendeixen a ser ambigües i extenses, usar pseudocodi i diagrames de flux evita moltes ambigüitats del llenguatge natural. Aquestes expressions són formes més estructurades per a representar algorismes; no obstant això, es mantenen independents d'un llenguatge de programació específic.

La descripció d'un algorisme usualment es fa a tres nivells:

Descripció d'alt nivell::
S'estableix el problema, se selecciona un model matemàtic i s'explica l'algorisme de manera verbal, possiblement amb il·lustracions i ometent detalls.
Descripció formal::
S'usa pseudocodi per a descriure la seqüència de passos que troben la solució.
Implementació::
Es mostra l'algorisme expressat en un llenguatge de programació específic o algun objecte capaç de dur a terme instruccions.

També és possible incloure un teorema que *demostri que l'algorisme és correcte*, un *anàlisi de complexitat* o tots dos.

=== Representació d'algoritmes

Per descriure un algoritme concret es poden utilitzar les següents tècniques.

==== ((Pseudocodi))

El pseudocodi és un llenguatge informal d’alt nivell que usa les 
convencions i l’estructura d’un llenguatge de programació, però que està orientat a ser entès pels humans.

.Exemple de pseudocodi 1
----
algoritme Entrada
var
    i, k : enter
    x : real
    c: caràcter
fvar
inici
    i:= llegirEnter()
    x:= llegirReal()
    c:= llegirCaracter()
    k:= llegirEnter() + 7
falgoritme
----

.Exemple de pseudocodi 2
----
algoritme CondicionalSIDoble
    ...
    si condició llavors
        bloc_instruccions1
    altrament
        bloc_instruccions2
    fsi
    ...
falgoritme
----

.Exemple de pseudocodi 3
----
algoritme InstruccióMentre
    ...
    inicialitzacions
    mentre condició fer
        blocInstrucions1
        blocInstrucions2
        ...
        blocInstrucionsN
    fmentre
    blocInstrucionsN+1
    ...
falgoritme
----

=== ((Diagrama de control de flux))

Consisteix en una subdivisió de passos seqüencials, d’acord amb les sentències i estructures de control d’un programa, que mostra els  diferents camins que pot seguir un programa a l’hora d’executar les seves instruccions. Cada passa s’associa a una figura geomètrica  específica.

També es pot representar l'algoritme directament en codi font d'un llenguatge de programació l'inconvenient principal d'aquest enfoc és que a diferència del pseudocodi i dels diagrames de flux l'interpret de l'algoritme ha de tenir coneixements informàtics per entendre'l.

Moltes vegades s'utilitzen les representacions gràfiques dels algoritmes per a establir una comunicació amb el client del programa que no té coneixements tècnics informàtics.

==== Alguns dels símbols utilitzats

|====
^.^a| [plantuml,file="images/uml/simbolsFlux1.png"]
....
skinparam monochrome true
scale 2.0

:  inici  ;
.... ^.^a| [plantuml,file="images/uml/simbolsFlux2.png"]
....
skinparam monochrome true
scale 2.0

:Procés]
.... 
^.^a| [plantuml,file="images/uml/simbolsFlux3.png"]
....
skinparam monochrome true
scale 2.0

:Entrada/Sortida/
.... ^.^a| [plantuml,file="images/uml/simbolsFlux4.png"]
....
skinparam monochrome true
scale 2.0

:Crida subprograma \|
.... 
^.^a| [plantuml,file="images/uml/simbolsFlux5.png"]
....
skinparam monochrome true
scale 2.0

:    final    ;
.... |

|====

i un exemple:

.Exemple diagrama de control de flux
[plantuml,align="center",file="images/uml/fluxExemple1.png"]
....
skinparam monochrome true
scale 2.0
:  inici  ;
:comptador ← valorInicial]
if (comptador > valorFinal) then (sí)
else (no)
  :Bloc d'instruccions]
endif
:Escriure resultat/
:  final  ;
....

////
=== Diagrames d'activitat UML

.Diagrama d'activitat
****
És una tècnica que permet descriure lògica procedural, processos de negoci i fluxos de treball. Juga un rol similar al dels diagrames de flux però la principal diferència entre aquests és que els diagrames d'activitat suporten comportaments en paral·lel.
****

.Què és UML?
****
UML, Unified Modeling Language, consisteix en una família de notacions gràfiques, basades en un únic metamodel, que ajuden a descriure i a dissenyar sistemes de software, en particular sistemes construïts utilitzant la orientació a objectes.

Els llenguatges de modelat gràfics existeixen a la industria des de fa temps, la principal motivació que hi ha al darrera és que *els llenguatges de programació no tenen un nivell prou alt d'abstracció per facilitar les discussions sobre el disseny*.

Tot i que els llenguatges de modelat gràfics existeixen es de fa molt temps hi ha una gran disparitat d'opinions sobre el rol que ha de tenir dins de l'activitat de disseny de sistemes, aquesta disparitat d'opinions té un impacte directe sobre quina és la percepció del rol del propi UML.

UML és un estàndard relativament obert, controlat pel Object Management Group (OMG), un consorci obert de companyies. L'OMG es va formar per construir estàndards que suportessin la interoperabilitat, específicament dels sistemes orientats a objectes.
****

==== Alguns dels símbols utilitzats

===== Inici/Final

Els simbols d'inici i final denoten el principi i el final del diagrama.

[plantuml,align="center",file="images/uml/startend.png"]
....
start
:Hola món;
end
....

===== Condicionals

Alterna el flux d'execució en base a una condició.

[plantuml,align="center",file="images/uml/condicional.png"]
....
start

if (LibreOffice instal·lat?) then (si)
  :obrir tots els documents;
else (no)
  :instal·lar LibreOffice;
endif
end
....

===== Procés en paral·lel

Permet bifurcar el flux d'execució en dos processos independents i que s'executen simultàniament. 

Possiblement en algun moment el procés més ràpid haurà d'esperar al procés més lent i es podrà tornar a unificar el flux d'execució.

[plantuml,align="center",file="images/uml/fork.png"]
....
start

if (multiprocessador?) then (sí)
  fork
    :Activitat 1;
  fork again
    :Activitat 2;
  end fork
else (monoprocessador)
  :Activitat 1;
  :Activitat 2;
endif
end
....

===== Repeticions

Les fletxes transfereixen el control a altres localitzacions del codi delimitant blocs de codi que es repeteixen mentre es compleixi una condició.

[plantuml,align="center",file="images/uml/repeticio.png"]
....
start
floating note: Mostra els nombres de l'1 al 10
:int i ← 1;
while (i < 11) is (sí)
  :Mostra i;
  :i++;
endwhile (no)

end
....

===== Anotacions

Permeten afegir notes als diagrames o directament a les activitats.

[plantuml,align="center",file="images/plantuml/anotacions.png"]
....
start
:foo1;
floating note left: Això és una nota
:foo2;
note right
  Una altra nota.
end note
end
....

===== Exemple de diagrama d'activitat

.Exemple de diagrama d'activitat
[plantuml,align="center",file="images/plantuml/diagramaActivitat.png"]
....
start
:Rebre comanda;
    fork
        :Omplir comanda;
        if (comanda prioritària) then (sí)
            :Entrega 
            nocturna;
        else (no)
            :Entrega 
            estàndard;
        endif
    fork again
        :Enviar factura;
        :Rebre 
        pagament;
    end fork
    :Tancar comanda;
end
....
////

=== Alguns algoritmes d'exemple

==== Exemple 1

Escriure el diagrama de flux del següent programa:

* Generar un número aleatòriament.
* Demanar un número.
* Si els dos números coincideixen mostrar el missatge has guanyat, en cas contrari mostrar el missatge has perdut.
+
[plantuml,align="center",file="images/plantuml/exempleAlgoritme3.png"]
....
skinparam monochrome true
scale 2.0

start
    :Generar un número aleatori → numAleatori]
    :Entrar → numDemanat/
    if (numAleatori = numDemanat) then (sí)
        :Mostrar "Has guanyat!!"/
    else (no)
        :Mostrar "Has perdut!!"/  
    endif
stop
....

==== Exemple 2

Escriure el diagrama de flux del següent programa:

* Demanar un número enter, si el número entrat és senar tornar-lo a demanar, si el número és parell mostrar-lo i acabar el programa. 
+
La següent solució té en compte únicament les estructures bàsiques de la programació estructurada:
+
.Primera proposta de solució
[plantuml,align="center", file="images/uml/exempleAlgoritme21.png"]
....
skinparam monochrome true
scale 2.0
start
    :Entrar NUM/
    while (NUM % 2 és igual a 1) is (sí)
      :Entrar NUM/
    endwhile (no)
    :Mostrar num/
end
....
+
.Segona proposta de solució
[plantuml,align="center",file="images/uml/exempleAlgoritme22.png"]
....
skinparam monochrome true
scale 2.0
start
    repeat
        :Entrar NUM/
    repeat while (NUM % 2 = 1) is (sí)
    :Mostrar num/
end
....

==== Exemple 3

Escriure el diagrama de flux del següent programa:

* Demanar que es vol comprar, una barra de pa o una ampolla de vi. 
* Si es vol comprar una ampolla de vi demanar l'edat en anys del comprador.
* Si l'edat és menor que 18 indicar que no es pot realitzar la compra.
* En els altres casos mostrar el preu.
* Finalment demanar si es vol realitzar una altra compra i si es així tornar a començar el procés.
+
[plantuml,align="center",file="images/uml/exempleAlgoritme1.png"]
....
skinparam monochrome true
scale 2.0

start 
    repeat
        :Demanar que es vol comprar → compra/
        if (compra = vi) then (sí)
            :Demanar l'edat del comprador/
            if (Edat del comprador < 18) then (sí)
                :No es pot realitzar la compra/
            else (no)
                :Mostrar preu ampolla de vi/
            endif
        else (no)
            if (compra = pa) then (sí)
                :Mostrar preu pa/
            else (no)
            endif 
        endif
    repeat while (Seguir comprant) is (sí)         
end
....

