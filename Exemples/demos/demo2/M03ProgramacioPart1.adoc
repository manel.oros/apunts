//:teacher:

ifdef::teacher[]
= M03 Programació (Versió del professor)
endif::teacher[]
ifndef::teacher[]
= M03 Programació: Primera part
endif::teacher[]
:author: Josep Queralt
:email: jqueral8@xtec.cat
:revdate: Octubre 10, 2019
:revnumber: 1.22.1
:doctype: book
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:icons: font
:sectnums:

include::UF1_00ProgramacioEstructurada.adoc[]

[appendix]
include::apendixManualEstil.adoc[]

[appendix]
include::apendixBasesDeNumeracio.adoc[]

[appendix]
include::apendixRepresentacioDeDades.adoc[]

[appendix]
include::apendixNombresAleatoris.adoc[]

include::llicencia.adoc[]

include::index.adoc[]
